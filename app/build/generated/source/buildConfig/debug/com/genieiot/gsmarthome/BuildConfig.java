/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.genieiot.gsmarthome;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.genieiot.gsmarthome";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10;
  public static final String VERSION_NAME = "v1.4.5";
}
