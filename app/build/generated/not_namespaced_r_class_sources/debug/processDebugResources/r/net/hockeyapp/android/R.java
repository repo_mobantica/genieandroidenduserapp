/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package net.hockeyapp.android;

public final class R {
    private R() {}

    public static final class color {
        private color() {}

        public static final int hockeyapp_background_header = 0x7f06005f;
        public static final int hockeyapp_background_light = 0x7f060060;
        public static final int hockeyapp_background_white = 0x7f060061;
        public static final int hockeyapp_button_background = 0x7f060062;
        public static final int hockeyapp_button_background_pressed = 0x7f060063;
        public static final int hockeyapp_button_background_selected = 0x7f060064;
        public static final int hockeyapp_text_black = 0x7f060065;
        public static final int hockeyapp_text_light = 0x7f060066;
        public static final int hockeyapp_text_normal = 0x7f060067;
        public static final int hockeyapp_text_white = 0x7f060068;
    }
    public static final class drawable {
        private drawable() {}

        public static final int hockeyapp_btn_background = 0x7f0800d4;
    }
    public static final class id {
        private id() {}

        public static final int button_add_response = 0x7f090051;
        public static final int button_attachment = 0x7f090052;
        public static final int button_login = 0x7f090053;
        public static final int button_refresh = 0x7f090054;
        public static final int button_send = 0x7f090055;
        public static final int button_update = 0x7f090056;
        public static final int input_email = 0x7f0900ec;
        public static final int input_message = 0x7f0900ed;
        public static final int input_name = 0x7f0900ee;
        public static final int input_password = 0x7f0900ef;
        public static final int input_subject = 0x7f0900f0;
        public static final int label_author = 0x7f090100;
        public static final int label_date = 0x7f090101;
        public static final int label_last_updated = 0x7f090102;
        public static final int label_message = 0x7f090103;
        public static final int label_text = 0x7f090104;
        public static final int label_title = 0x7f090105;
        public static final int label_version = 0x7f090106;
        public static final int list_attachments = 0x7f090116;
        public static final int list_feedback_messages = 0x7f090117;
        public static final int text_headline = 0x7f0901bd;
        public static final int view_header = 0x7f09021c;
        public static final int web_update_details = 0x7f090221;
        public static final int wrapper_attachments = 0x7f090226;
        public static final int wrapper_feedback = 0x7f090227;
        public static final int wrapper_feedback_scroll = 0x7f090228;
        public static final int wrapper_messages = 0x7f090229;
        public static final int wrapper_messages_buttons = 0x7f09022a;
    }
    public static final class layout {
        private layout() {}

        public static final int hockeyapp_activity_expiry_info = 0x7f0b005c;
        public static final int hockeyapp_activity_feedback = 0x7f0b005d;
        public static final int hockeyapp_activity_login = 0x7f0b005e;
        public static final int hockeyapp_activity_update = 0x7f0b005f;
        public static final int hockeyapp_fragment_update = 0x7f0b0060;
        public static final int hockeyapp_view_feedback_message = 0x7f0b0061;
    }
    public static final class string {
        private string() {}

        public static final int hockeyapp_crash_dialog_app_name_fallback = 0x7f0e006a;
        public static final int hockeyapp_crash_dialog_message = 0x7f0e006b;
        public static final int hockeyapp_crash_dialog_negative_button = 0x7f0e006c;
        public static final int hockeyapp_crash_dialog_neutral_button = 0x7f0e006d;
        public static final int hockeyapp_crash_dialog_positive_button = 0x7f0e006e;
        public static final int hockeyapp_crash_dialog_title = 0x7f0e006f;
        public static final int hockeyapp_dialog_error_message = 0x7f0e0070;
        public static final int hockeyapp_dialog_error_title = 0x7f0e0071;
        public static final int hockeyapp_dialog_negative_button = 0x7f0e0072;
        public static final int hockeyapp_dialog_positive_button = 0x7f0e0073;
        public static final int hockeyapp_download_failed_dialog_message = 0x7f0e0074;
        public static final int hockeyapp_download_failed_dialog_negative_button = 0x7f0e0075;
        public static final int hockeyapp_download_failed_dialog_positive_button = 0x7f0e0076;
        public static final int hockeyapp_download_failed_dialog_title = 0x7f0e0077;
        public static final int hockeyapp_error_no_network_message = 0x7f0e0078;
        public static final int hockeyapp_expiry_info_text = 0x7f0e0079;
        public static final int hockeyapp_expiry_info_title = 0x7f0e007a;
        public static final int hockeyapp_feedback_attach_file = 0x7f0e007b;
        public static final int hockeyapp_feedback_attach_picture = 0x7f0e007c;
        public static final int hockeyapp_feedback_attachment_button_text = 0x7f0e007d;
        public static final int hockeyapp_feedback_attachment_error = 0x7f0e007e;
        public static final int hockeyapp_feedback_attachment_loading = 0x7f0e007f;
        public static final int hockeyapp_feedback_email_hint = 0x7f0e0080;
        public static final int hockeyapp_feedback_failed_text = 0x7f0e0081;
        public static final int hockeyapp_feedback_failed_title = 0x7f0e0082;
        public static final int hockeyapp_feedback_fetching_feedback_text = 0x7f0e0083;
        public static final int hockeyapp_feedback_generic_error = 0x7f0e0084;
        public static final int hockeyapp_feedback_last_updated_text = 0x7f0e0085;
        public static final int hockeyapp_feedback_max_attachments_allowed = 0x7f0e0086;
        public static final int hockeyapp_feedback_message_hint = 0x7f0e0087;
        public static final int hockeyapp_feedback_name_hint = 0x7f0e0088;
        public static final int hockeyapp_feedback_refresh_button_text = 0x7f0e0089;
        public static final int hockeyapp_feedback_response_button_text = 0x7f0e008a;
        public static final int hockeyapp_feedback_select_file = 0x7f0e008b;
        public static final int hockeyapp_feedback_select_picture = 0x7f0e008c;
        public static final int hockeyapp_feedback_send_button_text = 0x7f0e008d;
        public static final int hockeyapp_feedback_send_generic_error = 0x7f0e008e;
        public static final int hockeyapp_feedback_send_network_error = 0x7f0e008f;
        public static final int hockeyapp_feedback_sending_feedback_text = 0x7f0e0090;
        public static final int hockeyapp_feedback_subject_hint = 0x7f0e0091;
        public static final int hockeyapp_feedback_title = 0x7f0e0092;
        public static final int hockeyapp_feedback_validate_email_empty = 0x7f0e0093;
        public static final int hockeyapp_feedback_validate_email_error = 0x7f0e0094;
        public static final int hockeyapp_feedback_validate_name_error = 0x7f0e0095;
        public static final int hockeyapp_feedback_validate_subject_error = 0x7f0e0096;
        public static final int hockeyapp_feedback_validate_text_error = 0x7f0e0097;
        public static final int hockeyapp_login_email_hint = 0x7f0e0098;
        public static final int hockeyapp_login_headline_text = 0x7f0e0099;
        public static final int hockeyapp_login_headline_text_email_only = 0x7f0e009a;
        public static final int hockeyapp_login_login_button_text = 0x7f0e009b;
        public static final int hockeyapp_login_missing_credentials_toast = 0x7f0e009c;
        public static final int hockeyapp_login_password_hint = 0x7f0e009d;
        public static final int hockeyapp_paint_dialog_message = 0x7f0e009e;
        public static final int hockeyapp_paint_dialog_negative_button = 0x7f0e009f;
        public static final int hockeyapp_paint_dialog_neutral_button = 0x7f0e00a0;
        public static final int hockeyapp_paint_dialog_positive_button = 0x7f0e00a1;
        public static final int hockeyapp_paint_indicator_toast = 0x7f0e00a2;
        public static final int hockeyapp_paint_menu_clear = 0x7f0e00a3;
        public static final int hockeyapp_paint_menu_save = 0x7f0e00a4;
        public static final int hockeyapp_paint_menu_undo = 0x7f0e00a5;
        public static final int hockeyapp_permission_dialog_negative_button = 0x7f0e00a6;
        public static final int hockeyapp_permission_dialog_positive_button = 0x7f0e00a7;
        public static final int hockeyapp_permission_update_message = 0x7f0e00a8;
        public static final int hockeyapp_permission_update_title = 0x7f0e00a9;
        public static final int hockeyapp_update_button = 0x7f0e00aa;
        public static final int hockeyapp_update_dialog_message = 0x7f0e00ab;
        public static final int hockeyapp_update_dialog_negative_button = 0x7f0e00ac;
        public static final int hockeyapp_update_dialog_positive_button = 0x7f0e00ad;
        public static final int hockeyapp_update_dialog_title = 0x7f0e00ae;
        public static final int hockeyapp_update_mandatory_toast = 0x7f0e00af;
        public static final int hockeyapp_update_version_details_label = 0x7f0e00b0;
    }
    public static final class style {
        private style() {}

        public static final int HockeyApp_ButtonStyle = 0x7f0f00b6;
        public static final int HockeyApp_EditTextStyle = 0x7f0f00b7;
        public static final int HockeyApp_SingleLineInputStyle = 0x7f0f00b8;
    }
}
