package com.genieiot.gsmarthome;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

import Adapter.DeviceListAdapter;

/**
 * Created by root on 16/12/16.
 */

public class SetRouterPassword extends Activity implements View.OnClickListener{
    private RecyclerView recycler;
    private ArrayList<HashMap<String,String>> arrayList;
    DeviceListAdapter adapter;
    private TextView txtHeading;
    private WifiManager wifimanager;
    private ImageView imgRefresh,imgBack;

    private static String SSID="geniesmart";
    private static String PASSWORD="12341234";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setrouterpassword);

        initControl();

    }

    @Override
    protected void onResume() {
        super.onResume();

        wifimanager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d("App Version","Above Marshmellow");
            WifiAPController wifiAPController  = new WifiAPController();
            wifiAPController.wifiToggle(SSID,PASSWORD,wifimanager, this);

        }else {
            Log.d("App Version","Blow Marshmellow");
            setWifiApState(SetRouterPassword.this, true);
        }

        new AsyncDeviceList().execute();
    }

    public static boolean setWifiApState(Context context, boolean enabled) {
        //config = Preconditions.checkNotNull(config);
        try {
            WifiManager mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (enabled) {
                mWifiManager.setWifiEnabled(false);
            }
            WifiConfiguration conf = getWifiApConfiguration();
            mWifiManager.addNetwork(conf);

            return (Boolean) mWifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class).invoke(mWifiManager, conf, enabled);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static WifiConfiguration getWifiApConfiguration() {
        WifiConfiguration netConfig = new WifiConfiguration();
        netConfig.SSID = SSID ;
        netConfig.preSharedKey = PASSWORD;
        netConfig.hiddenSSID = true;
        netConfig.status = WifiConfiguration.Status.ENABLED;
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        netConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        netConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        return netConfig;
    }

    private void initControl() {

        recycler= (RecyclerView) findViewById(R.id.recycler);
        arrayList=new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(mLayoutManager);
        txtHeading= (TextView) findViewById(R.id.txtHeading);
        imgRefresh= (ImageView) findViewById(R.id.imgRefresh);
        imgBack= (ImageView) findViewById(R.id.imgBack);
        imgRefresh.setOnClickListener(this);
        imgBack.setOnClickListener(this);

        txtHeading.setText("Router Setting");

    }


    public void setAdapter(){
        adapter=new DeviceListAdapter(this,arrayList);
        recycler.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.imgRefresh:
                arrayList.clear();
                new AsyncDeviceList().execute();
                break;
            case R.id.imgBack:
                finish();
                break;
        }
    }

    public class AsyncDeviceList extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            BufferedReader br = null;
            boolean isFirstLine = true;

            try {
                br = new BufferedReader(new FileReader("/proc/net/arp"));
                String line;

                while ((line = br.readLine()) != null) {
                    if (isFirstLine) {
                        isFirstLine = false;
                        continue;
                    }

                    String[] splitted = line.split(" +");

                    if (splitted != null && splitted.length >= 4) {

                        String ipAddress = splitted[0];
                        String macAddress = splitted[3];

                        boolean isReachable = InetAddress.getByName(
                                splitted[0]).isReachable(500);  // this is network call so we cant do that on UI thread, so i take background thread.
                        if (isReachable) {
                            Log.d("Device Information", ipAddress + " : "
                                    + macAddress);
                            HashMap<String,String> mMap=new HashMap<>();
                            mMap.put("IP",ipAddress);
                            mMap.put("MAC",macAddress);
                            mMap.put("FLAG","1");

                            arrayList.add(mMap);
                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setAdapter();
        }
    }
}
