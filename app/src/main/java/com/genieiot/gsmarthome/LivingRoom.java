package com.genieiot.gsmarthome;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.icu.util.TimeUnit;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import net.hockeyapp.android.CrashManager;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import Adapter.AdapterCustomSwitch;
import Adapter.TryDemoSwitchAdpater;
import Database.DatabaseHandler;
import Fragments.Rooms;
import Session.Constants;
import Session.IOnClickOfSwitchChange;
import Session.MqttlConnection;
import Session.NetworkConnectionInfo;
import Session.SessionManager;
import app.AppController;

import static Database.DatabaseHandler.CREATED_BY;
import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.IMAGE_OFF;
import static Database.DatabaseHandler.IMAGE_ON;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.OPERATION;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_NUMBER;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MESSAGE_TRY_AGAIN;
import static Session.Constants.MY_PREFS_NAME;
import static Session.Constants.NOTINTERNET;
import static Session.Constants.TOPIC_INTERNET;
import static Session.Constants.TOPIC_LOCAL;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.SessionManager.X_AUTH_TOKEN;
import static com.genieiot.gsmarthome.MainActivity.myToast;


/**
 * Created by Genie IoT on 9/14/2016.
 */
public class LivingRoom extends FragmentActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener,
        android.support.v7.widget.PopupMenu.OnMenuItemClickListener, IOnClickOfSwitchChange {

    private TextView txtCount, txtHeading, txtUnhide;
    private ImageView imgBack, imgOption;
    public String mRoomId;
    private String mRoomName;
    RecyclerView lstSwitches;
    boolean backFlag;
    private Context context = null;
    private DatabaseHandler db;
    private ArrayList<HashMap<String, String>> switchDatas;
    private ArrayList<HashMap<String, String>> switchDatasTemp;
    private SessionManager sessionManager;
    private String strWebserviceName = "";
    private int methodType;
    private ProgressDialog pDialog;
    TryDemoSwitchAdpater adapterTry;
    AdapterCustomSwitch adapter;
    String type = null;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    MqttMessage message2;
    public String connectionValue;
    Constants constants;
    private EditText edtSwitchName;
    private String switchId;

    ArrayList<HashMap<String, Integer>> mSwitchONOFFMap = new ArrayList<>();
    String[] switchList = new String[]{"Bulb", "AC", "Chandelier", "Cooler", "Desk Lamp", "Desktop",
            "DISH", "Exhaust", "Fan", "Refrigerator",
            "Microwave", "Mixer", "Purifier", "Socket", "Sound System", "Stove",
            "Table Fan", "TV", "Tube", "Washing Machine", "Water Heater"};

    int[] mSwitchON = new int[]{R.drawable.on_light_bulb, R.drawable.on_ac, R.drawable.on_chandelier, R.drawable.on_cooler, R.drawable.on_desk_lamp,
            R.drawable.on_desktop, R.drawable.on_dish, R.drawable.on_exost, R.drawable.on_fan, R.drawable.on_refrigerator,
            R.drawable.on_microwave, R.drawable.on_mixer, R.drawable.on_purifier, R.drawable.on_socket, R.drawable.on_sound,
            R.drawable.on_stove, R.drawable.on_table_fan, R.drawable.on_television, R.drawable.on_tube, R.drawable.on_washing_machine,
            R.drawable.on_water_heater};


    int[] mSwitchOFF = new int[]{R.drawable.off_bulb, R.drawable.off_ac, R.drawable.off_chandelier, R.drawable.off_cooler, R.drawable.off_desk_lamp,
            R.drawable.off_desktop_computer, R.drawable.off_dish, R.drawable.off_exost, R.drawable.off_fan, R.drawable.off_fridge,
            R.drawable.off_microwave, R.drawable.off_mixer, R.drawable.off_purifier, R.drawable.off_socket, R.drawable.off_sound,
            R.drawable.off_stove, R.drawable.off_table_fan, R.drawable.off_television, R.drawable.off_tube, R.drawable.off_washing_machine,
            R.drawable.off_water_heater};

    private String IPADDRESS = "";
    String topic;
    String broker = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    private String messageType = "";
    public static LivingRoom thisInstance = null;
    MqttlConnection mqttlConnection;
    private NetworkChangeReceiver receiver;
    IntentFilter filter;
    private boolean isConnected = false;
    String status1 = null;
    String status = null;
    String subTopic = "";
    private String mNetWorkInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.living_room);

        mqttlConnection = new MqttlConnection();
        constants = new Constants();

        editor = getApplication().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        pref = getApplication().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);

        db = new DatabaseHandler(LivingRoom.this);
        sessionManager = new SessionManager(this);
        Constants.URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";

        getRoomData();
        initialiseControls();
        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();


        if (sessionManager.getUSERID().equals("")) {
            getSwitchesList();
            setSwitchAdapter();
            setMqttClient();
        } else {
            setSwitchAdapter();
        }

        for (int i = 0; i < mSwitchON.length; i++) {
            HashMap<String, Integer> mMap = new HashMap<>();
            mMap.put("ON", mSwitchON[i]);
            mMap.put("OFF", mSwitchOFF[i]);
            mSwitchONOFFMap.add(mMap);
        }


    }

    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.v("NetworkChangeReceiver", ".....................LivingRoom Receieved notification about network status");
            isNetworkAvailable(context);
            getConnectivityStatusString(context);

        }

        public String getConnectivityStatusString(Context context) {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    status1 = "1";
                    setInternetSataus(status1);
                    return status1;
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    status1 = "2";
                    setInternetSataus(status1);
                    return status1;
                }
            } else {
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("INTERNET_TEST_HUB").putExtra("INTERNET_TEST", "0"));
                status = "No internet is available";
                return status1;
            }


            return status;
        }


        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if (!isConnected) {
                                Log.v("Living room", "Now you are connected to Internet!");
                                // networkStatus.setText("Now you are connected to Internet!");
                                isConnected = true;

                            }
                            return true;
                        }
                    }
                }
            }
            Log.v("Living room", "You are not connected to Internet!");
            //    networkStatus.setText("You are not connected to Internet!");
            isConnected = false;
            return false;
        }
    }

    private void setInternetSataus(String internet_test) {
        if (getApplicationContext() != null) {
            if (internet_test.equals("1")) {

                chkStatus1();

            } else if (internet_test.equals("2")) {

                connectionValue = INTERNET;
                //  return connectionValue;
            } else {

                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();

                //  return connectionValue;
            }/*else if (internet_test.equals("2")) {
                Log.d("connection_val", "====" + "internet_all");
                subTopic = "updateStatus2_" + sessionManager.getHomeId();
                // setBrokerInternet("tcp://103.12.211.52:1883");
                Log.d("connection_val", "====" + "internet_all1");

            } else if (internet_test.equals("0")) {
                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
            }*/
        }
    }

    private class AsyncTaskCheckInternet extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {
            Constants request = new Constants();
            String mResponse = null;
            try {

                JSONObject jMain = new JSONObject();
                jMain.put("homeId", sessionManager.getHomeId());

                mResponse = request.doPostInternetRequest("http://192.168.0.119:8080/smart_home_local/home/verifygohub ", jMain + "");
                Log.d("Get_room_response", "response val =" + mResponse);


            } catch (Exception e) {

            }

            return mResponse;
        }

    }

    private void setMqttClient() {
        try {
            clientId = System.currentTimeMillis() + "";
            persistence = new MemoryPersistence();
            mqqtClient = new MqttClient(broker, clientId, persistence);

            Log.d("client_id1", "broker==" + broker);
            Log.d("client_id1", "clientId==" + clientId);
            Log.d("client_id1", "persistence==" + persistence);
            Log.d("client_id1", "topic==" + topic);

            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);


            Log.d("client_id1", "connecting...");
            mqqtClient.connect(connOpts);
            Log.d("client_id1", "connected");
            //  mqqtClient.setCallback(LivingRoom.this);

            mqqtClient.subscribe(topic);
            Log.d("client_id1", "published");

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Exception -->", e.toString());
            Log.d("client_id1", "mqtt connection error" + e.toString());
        }
    }

    private void getSwitchesList() {
        try {
            switchDatas.clear();
            switchDatas = db.getSwitches(mRoomId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getRoomData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            mRoomId = bundle.getString(ROOM_ID);
            mRoomName = bundle.getString(ROOM_NAME);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hiddenmenu, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_hide:
                //Toast.makeText(this, "add", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.schedule:
                //Toast.makeText(this, "add", Toast.LENGTH_SHORT).show();
                return true;
            default:

                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {

        HashMap mapList;

        if (sessionManager.getUSERID().equals("")) {
            mapList = (HashMap) adapterTry.getItem(adapterTry.getPosition());
        } else {
            mapList = (HashMap) adapter.getItem(adapter.getPosition());
        }


        switch (item.getItemId()) {
            case R.id.edit:
                if (sessionManager.getDemoUser().equals("DemoUser")) {
                    backFlag = false;
                    Intent intentEdit = new Intent(LivingRoom.this, EditOpetaion.class);
                    intentEdit.putExtra("SwitchInfo", mapList);
                    startActivity(intentEdit);
                } else {
                    if (sessionManager.getUserType().equals("2")) {
                        Toast.makeText(context, "Don't have authority to edit the switch.", Toast.LENGTH_LONG).show();

                    } else {
                        Intent intentEdit = new Intent(LivingRoom.this, EditOpetaion.class);
                        intentEdit.putExtra("SwitchInfo", mapList);
                        startActivity(intentEdit);
                    }

                }

                break;
            case R.id.hide:
//
                if (sessionManager.getDemoUser().equals("DemoUser")) {
                    hideOperation(mapList, "1");
                    setSwitchAdapter();
                } else {
                    if (!sessionManager.getUserType().equals("2")) {
                        if (item.getTitle().equals("Hide")) {
                            ALertDialogHideOperation();
                            // new AsyncHideOperation(mapList).execute("1");
                            Log.d("Living Room : ", "Hide");
                        }
                    } else {
                        Toast.makeText(context, "Don't have authority to Hide the Switch.", Toast.LENGTH_LONG).show();
                    }
                }

                break;

            case R.id.lock:

                if (sessionManager.getDemoUser().equals("DemoUser")) {
                    if (item.getTitle().equals("Lock")) {
                        lockOperation(mapList, "1");
                        mapList.put(LOCK, "1");
                        OnClickOfSwitchChange(mapList);
                    } else {
                        lockOperation(mapList, "0");
                        mapList.put(LOCK, "0");
                        OnClickOfSwitchChange(mapList);
                    }

                } else {
                    if (!sessionManager.getUserType().equals("2")) {

                        if (item.getTitle().equals("Lock")) {
                            new AsyncLockOpration(mapList).execute("1");
                            Log.d("Living Room : ", "Lock");

                        } else {
                            new AsyncLockOpration(mapList).execute("0");
                            Log.d("Living Room : ", "Unlock");
                        }
                    } else {
                        Toast.makeText(context, "Don't have authority to Lock the Switch.", Toast.LENGTH_LONG).show();
                    }
                }

                break;

            case R.id.schedule:

                if (sessionManager.getDemoUser().equals("DemoUser")) {
                    Intent intent = new Intent(LivingRoom.this, AddNewTask.class);
                    mapList.put(IP, getRoomIP());
                    intent.putExtra("SwitchInfo", mapList);
                    startActivity(intent);
                } else {
                    if (!sessionManager.getUserType().equals("2")) {
                        Intent intent = new Intent(LivingRoom.this, AddNewTask.class);
                        intent.putExtra("SwitchInfo", mapList);
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Don't have authority to Schedule the Switch.", Toast.LENGTH_LONG).show();
                    }
                }

                break;

        }

        return super.onContextItemSelected(item);
    }

    private void ALertDialogHideOperation() {

        final HashMap mapList;
        if (sessionManager.getUSERID().equals("")) {
            mapList = (HashMap) adapterTry.getItem(adapterTry.getPosition());
        } else {
            mapList = (HashMap) adapter.getItem(adapter.getPosition());
        }

        final Dialog dialog = new Dialog(LivingRoom.this);
        // Include dialog.xml file

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_hide_switch1);
        TextView tv = (TextView) dialog.findViewById(R.id.tvMessage);
        if (mapList.get(SWITCH_STATUS).equals("0")) {
            tv.setText("Are you sure you want hide switch ?");
        } else {
            tv.setText("In order to hide,we need to TURN OFF selected appliance.");
        }

        final EditText edtPasswordShareControl = (EditText) dialog.findViewById(R.id.edtPasswordShareControl);
        TextView btnok = (TextView) dialog.findViewById(R.id.txtOk);
        TextView btncancel = (TextView) dialog.findViewById(R.id.txtCancel);

        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mqttMessage = null;
                chkStatus();

                String switchStat = String.valueOf(mapList.get(SWITCH_STATUS));
                String switchNum = String.valueOf(mapList.get(SWITCH_NUMBER));
                String panelName = String.valueOf(mapList.get(PANEL_NAME));


                if (mapList.get(SWITCH_STATUS).equals("0")) {
                    dialog.dismiss();
                    new AsyncHideOperation(mapList).execute("1");
                } else {

                    if (connectionValue == LOCAL_HUB) {

                        mqttMessage = sessionManager.getUSERID() + "/" + mapList.get(SWITCH_NUMBER) + "/" + "0" + "/" + "0" + "/" + connectionValue + "/" + mapList.get(PANEL_NAME);

                        mqttMessage = "wish/Local_Switch_On_Off/" + mqttMessage;
                        Log.d("switch_detail ", "mqttMessage===" + mqttMessage);


                        message2 = new MqttMessage(mqttMessage.getBytes());
                        message2.setQos(0);
                        try {
                            mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL + sessionManager.getHomeId(), message2);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }


                    } else {

                        mqttMessage = sessionManager.getUSERID() + "/" + mapList.get(SWITCH_NUMBER) + "/" + "0" + "/" + "0" + "/" + connectionValue + "/" + mapList.get(PANEL_NAME) + "/" + sessionManager.getHomeId();

                        mqttMessage = "wish/Internet_Switch_On_Off/" + mqttMessage;
                        Log.d("switch_detail ", "mqttMessage===" + mqttMessage);


                        message2 = new MqttMessage(mqttMessage.getBytes());
                        message2.setQos(0);
                        try {
                            mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET, message2);
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }

                    }

                    dialog.dismiss();
                    new AsyncHideOperation(mapList).execute("1");
                }


            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void OnClickOfSwitchChange(HashMap<String, String> map) {
        Log.d("check_mqtt_ok", "Switch clicked "+connectionValue);
        switchONOFFOpt(map,connectionValue);
        try {
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void switchONOFFOpt(HashMap<String, String> map, String connectionValue) {
        Log.d("data_incoming", "==" + map);
        String mqttMessage = null;
        String dimmerValue = "";
        Constants conn = new Constants();
        //chkStatus();

        try {
            if (connectionValue == INTERNET) {
                if (map.get(DIMMER_STATUS).equals("1") && map.get(SWITCH_STATUS).equals("1")) {
                    if (Integer.parseInt(map.get(DIMMER_VALUE)) < 10) {
                        dimmerValue = "0" + map.get(DIMMER_VALUE);

                    } else {
                        dimmerValue = map.get(DIMMER_VALUE);
                        Log.d("switch_detail555 ", "DIMMER VALUE121===" + dimmerValue);

                    }
                    mqttMessage = sessionManager.getUSERID() + "/" + map.get(SWITCH_NUMBER) + "/" + map.get(SWITCH_STATUS) + "/" + dimmerValue + "/" + this.connectionValue + "/" + map.get(PANEL_NAME) + "/" + sessionManager.getHomeId();

                } else {

                    mqttMessage = sessionManager.getUSERID() + "/" + map.get(SWITCH_NUMBER) + "/" + map.get(SWITCH_STATUS) + "/" + "0" + "/" + this.connectionValue + "/" + map.get(PANEL_NAME) + "/" + sessionManager.getHomeId();
                }


                messageType = INTERNET;
                broker = "tcp://103.12.211.52:1883";
                topic = "refreshIntGenieHome";

                Constants.URL_GENIE = Constants.URL_GENIE_AWS;

                Log.d("check_mqtt", "On off page internet");
                topic = "refreshIntGenieHomeId_1";

                mqttMessage = "wish/Internet_Switch_On_Off/" + mqttMessage;
                Log.d("switch_detail ", "mqttMessage===" + mqttMessage);


                message2 = new MqttMessage(mqttMessage.getBytes());
                message2.setQos(0);
                try {
                    if (mqqtClient != null) {

                        mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET, message2);
                        Log.d("switch_detail ", "internet=PUBLISHED=topic=  " + TOPIC_INTERNET);
                        Log.d("switch_detail ", "internet==PUBLISHED=broker=  " + broker);
                        Log.d("switch_detail ", "internet==PUBLISHED=msg= " + message2);
                    } else {
                        mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET, message2);
                        Log.d("switch_detail ", "internet=PUBLISHED=topic=  " + TOPIC_INTERNET);
                        Log.d("switch_detail ", "internet==PUBLISHED=broker=  " + broker);
                        Log.d("switch_detail ", "internet==PUBLISHED=msg=  " + message2);

                        Log.d("switch_detail ", "PUBLISHED");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {

                if (map.get(DIMMER_STATUS).equals("1") && map.get(SWITCH_STATUS).equals("1")) {
                    if (Integer.parseInt(map.get(DIMMER_VALUE)) < 10) {
                        dimmerValue = "0" + map.get(DIMMER_VALUE);

                    } else {
                        dimmerValue = map.get(DIMMER_VALUE);
                        Log.d("switch_detail555 ", "DIMMER VALUE121===" + dimmerValue);
                    }
                    mqttMessage = sessionManager.getUSERID() + "/" + map.get(SWITCH_NUMBER) + "/" + map.get(SWITCH_STATUS) + "/" + dimmerValue + "/" + this.connectionValue + "/" + map.get(PANEL_NAME);

                } else {
                    mqttMessage = sessionManager.getUSERID() + "/" + map.get(SWITCH_NUMBER) + "/" + map.get(SWITCH_STATUS) + "/" + "0" + "/" + this.connectionValue + "/" + map.get(PANEL_NAME);

                }


                messageType = LOCAL_HUB;
                broker = "tcp://" + sessionManager.getRouterIP() + ":1883";
                topic = "refreshIntGenieHomeId_" + sessionManager.getHomeId();

                mqttMessage = "wish/Local_Switch_On_Off/" + mqttMessage;
                message2 = new MqttMessage(mqttMessage.getBytes());
                message2.setQos(0);

                Log.d("switch_detail ", "mqttMessage===" + mqttMessage);

                if (mqqtClient != null) {

                    mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL + sessionManager.getHomeId(), message2);
                    Log.d("switch_detail ", "local=PUBLISHED=topic=  " + topic);
                    Log.d("switch_detail ", "local=PUBLISHED=broker=  " + broker);
                    Log.d("switch_detail ", "local=PUBLISHED=msg=  " + mqttMessage);

                } else {

                    mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL + sessionManager.getHomeId(), message2);
                    Log.d("switch_detail ", "local=PUBLISHED1=topic=  " + topic);
                    Log.d("switch_detail ", "local=PUBLISHED1=broker=  " + broker);
                    Log.d("switch_detail ", "local=PUBLISHED1=msg=  " + mqttMessage);


                }
            }

            System.out.println("Message published");

            db.updateSwitchStatus(map.get(SWITCH_ID), map.get(SWITCH_STATUS), map.get(DIMMER_VALUE));

            Calendar calander;
            String time;
            calander = Calendar.getInstance();

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            time = simpleDateFormat.format(calander.getTime());

            HashMap<String, String> switcch = new HashMap<String, String>();
            switcch.put(SWITCH_ID, map.get(SWITCH_ID));
            switcch.put(SWITCH_NAME, map.get(SWITCH_NAME));
            switcch.put(SWITCH_TYPE_ID, map.get(SWITCH_TYPE_ID));
            switcch.put(SWITCH_STATUS, map.get(SWITCH_STATUS));
            switcch.put(SWITCH_NUMBER, map.get(SWITCH_NUMBER));
            switcch.put(ROOM_ID, map.get(ROOM_ID));
            switcch.put(DIMMER_STATUS, map.get(DIMMER_STATUS));
            switcch.put(DIMMER_VALUE, map.get(DIMMER_VALUE));
            switcch.put(ROOM_NAME, map.get(ROOM_NAME));
            switcch.put(SWITCH_IMAGE_ID, map.get(SWITCH_IMAGE_ID));
            switcch.put(TIME, time);
            switcch.put(PANEL_NAME, map.get(PANEL_NAME));

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date = new Date();
            String formattedDate = dateFormat.format(date);
            switcch.put(CREATED_BY, formattedDate);

            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));

        } catch (MqttException e) {
            e.printStackTrace();
            Log.d("MqttConnect Publish-->", e.toString());
        }
        settextCount(0);
        try {
            adapter.notifyDataSetChanged();  //m
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void chkStatus1() {
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            String result = null;
            try {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    result = new AsyncTaskCheckInternet().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get(2000, java.util.concurrent.TimeUnit.MILLISECONDS);

                } else {
                    result = new AsyncTaskCheckInternet().execute().get(2000, java.util.concurrent.TimeUnit.MILLISECONDS);
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }catch (TimeoutException e) {
                e.printStackTrace();
            }
            if (result != null) {
                Log.d("Get_room_response : ", result + "");
                try {
                    JSONObject response = new JSONObject(result);
                    String status = response.getString("status");
                    String results = response.getString("response");
                    String statusCode = response.getString("statusCode");

                    if (status.equals("SUCCESS")) {
                        if (statusCode.equalsIgnoreCase("0")){
                            connectionValue = INTERNET;
                        }else {
                            connectionValue = LOCAL_HUB;
                        }
                    } else {
                        connectionValue = INTERNET;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                connectionValue = INTERNET;
            }
        } else if (mobile.isConnectedOrConnecting()) {

            connectionValue = INTERNET;
            //  return connectionValue;
        } else {
            connectionValue = NOTINTERNET;

            //  return connectionValue;
        }
    }

    public String chkStatus() {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            try {
                new AsyncTaskCheckRouter().execute().get();
            } catch (InterruptedException e) {
                Log.d("connection_checking", "checked" + e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("connection_checking", "checked" + e.getMessage());
                e.printStackTrace();
            }

            return connectionValue;
        } else if (mobile.isConnectedOrConnecting()) {

            connectionValue = INTERNET;
            return connectionValue;
        } else {
            connectionValue = NOTINTERNET;

            return connectionValue;
        }

    }

    @Override
    public void OnProgressChangeListener(HashMap<String, String> map) {
        String mqttMessage = null;
        String mqttMessage1 = null;
        String dimmerValue;
        try {
            if (Integer.parseInt(map.get(DIMMER_VALUE)) < 10) {
                dimmerValue = "0" + map.get(DIMMER_VALUE);
            } else {
                dimmerValue = map.get(DIMMER_VALUE);
            }

            mqttMessage1 = "$[" + map.get(SWITCH_TYPE_ID) + "00" + map.get(LOCK) + "]";
            mqttMessage = "$[" + map.get(SWITCH_TYPE_ID) + dimmerValue + "0]";

            if (mNetWorkInfo.equals("INTERNET")) {

                mqttMessage = "wish," + getRoomIP() + "/out," + mqttMessage;
                mqttMessage1 = "wish," + getRoomIP() + "/out," + mqttMessage1;

                MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
                MqttMessage message1 = new MqttMessage(mqttMessage1.getBytes());

                message2.setQos(0);
                mqqtClient.publish(topic, message1);
                mqqtClient.publish(topic, message2);
            } else {
                Log.d("Living Room ", mqttMessage);
                MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());
                MqttMessage message1 = new MqttMessage(mqttMessage1.getBytes());

                message2.setQos(0);
                mqqtClient.publish(topic, message1);
                mqqtClient.publish(topic, message2);
            }


            System.out.println("Message published");
            db.updateSwitchStatus(map.get(SWITCH_ID), map.get(SWITCH_STATUS), map.get(DIMMER_VALUE));
        } catch (MqttException e) {

            e.printStackTrace();
        }


        settextCount(0);

    }


    private void hideOperation(HashMap<String, String> mapList, String hideStatus) {
        DatabaseHandler database = new DatabaseHandler(this);
        database.UpdateHideStatus(mRoomId, mapList.get(SWITCH_ID), hideStatus);
        switchDatas = db.getSwitches(mRoomId);
        setSwitchAdapter();

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend"));
    }

    private void lockOperation(HashMap<String, String> mapList, String mLockStatus) {

        DatabaseHandler database = new DatabaseHandler(this);
        database.UpdateLockFlag(mRoomId, mapList.get(SWITCH_ID), mLockStatus);
        switchDatas = db.getSwitches(mRoomId);
        setSwitchAdapter();
    }

    private BroadcastReceiver messageActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            ArrayList<HashMap<String, String>> switchDatas;
            if (adapter != null) {

                switchDatas = db.getSwitches(mRoomId);
                adapter.updateData(switchDatas);
                adapter.notifyDataSetChanged();
                settextCount(0);
            }
        }
    };

    private BroadcastReceiver messageActivity1 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            switchDatas = db.getSwitches(mRoomId);
            adapter.updateData(switchDatas);
            adapter.notifyDataSetChanged();
            settextCount(0);
        }
    };


    private BroadcastReceiver hideStatus = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            switchDatas = db.getSwitches(mRoomId);
            adapter.updateData(switchDatas);
            adapter.notifyDataSetChanged();
            settextCount(0);
        }
    };

    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(messageActivity, new IntentFilter("NotificationSend"));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageActivity1, new IntentFilter("NotificationSend1"));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageMqtt, new IntentFilter("MqttCallBack"));

        LocalBroadcastManager.getInstance(this).registerReceiver(hideStatus, new IntentFilter("HiddenStatus"));

        Animation bottomUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_up);
        bottomUp.setDuration(1000);
        lstSwitches.startAnimation(bottomUp);
        backFlag = true;
        CrashManager.register(this);

        setToolbar();


        settextCount(0);
    }

    private void getSwitchListServer() {

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("roomId", mRoomId);
        strWebserviceName = "/switch/getswitchbyroom";
        methodType = Request.Method.POST;

        callWebService(methodType, jsonParams, strWebserviceName);

    }

    private void setSwitchAdapter() {

        switchDatas = db.getSwitches(mRoomId);
        Log.d("switch_dta_hide", "111==" + switchDatas);


        IPADDRESS = getRoomIP();
        SessionManager session = new SessionManager(LivingRoom.this);

        if (session.getDemoUser().equals("DemoUser")) {
            adapterTry = new TryDemoSwitchAdpater(LivingRoom.this, switchDatas, mSwitchONOFFMap, IPADDRESS);
            lstSwitches.setLayoutManager(new LinearLayoutManager(this));
            lstSwitches.setAdapter(adapterTry);
            adapterTry.notifyDataSetChanged();
        } else {
            adapter = new AdapterCustomSwitch(LivingRoom.this, switchDatas, mSwitchONOFFMap);
            lstSwitches.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        settextCount(0);
    }

    private String getRoomIP() {
        if (mRoomId.equals("1")) {
            IPADDRESS = sessionManager.getFirstString();
        } else if (mRoomId.equals("2")) {
            IPADDRESS = sessionManager.getSecondString();
        } else if (mRoomId.equals("3")) {
            IPADDRESS = sessionManager.getThirdString();
        }

        return IPADDRESS;
    }

    private void callWebService(int methodType, Map<String, String> jsonParams, final String strWebserviceName) {
        showProgressDialog();

        Log.d("param", jsonParams.toString());
        JsonObjectRequest myRequest = new JsonObjectRequest(
                methodType, URL_GENIE + "" + strWebserviceName,
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();

                        if (response != null) {
                            try {
                                String status = response.getString("status");
                                String results = response.getString("result");
                                if (strWebserviceName.equals("/switch/getswitchbyroom")) {

                                    JSONArray resultArray = new JSONArray(results);
                                    if (status.equals("SUCCESS")) {
                                        createListForRoom(resultArray);
                                        setSwitchAdapter();

                                    }

                                } else if (strWebserviceName.equals("/switch/delete")) {

                                    status = response.getString("status");

                                    if (status.equals("SUCCESS")) {
                                        getSwitchListServer();
                                        myToast(LivingRoom.this, "Deleted Susccessfully");
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        error.printStackTrace();
                        myToast(LivingRoom.this, MESSAGE_TRY_AGAIN);
                        setSwitchAdapter();

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put(X_AUTH_TOKEN, sessionManager.getSecurityToken());
                return headers;
            }
        };

        myRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

//        myRequest.setRetryPolicy(new DefaultRetryPolicy(0,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(myRequest);
    }

    private void createListForRoom(JSONArray resultArray) {

        switchDatas.clear();
        switchDatasTemp.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            HashMap<String, String> roomType = new HashMap<String, String>();
            JSONObject jsonObject = null;
            String mDimmer;
            try {
                jsonObject = new JSONObject(resultArray.get(i).toString());

                roomType.put(SWITCH_ID, jsonObject.getString("id"));
                roomType.put(SWITCH_NAME, jsonObject.getString("switchName"));
                roomType.put(SWITCH_TYPE_ID, jsonObject.getString("switchType"));
                roomType.put(DIMMER_STATUS, jsonObject.getString("dimmerStatus"));

                if (jsonObject.getString("dimmerValue") == null) {
                    mDimmer = "0";
                } else {
                    mDimmer = jsonObject.getString("dimmerValue");
                }

                roomType.put(DIMMER_VALUE, mDimmer);
                roomType.put(SWITCH_STATUS, jsonObject.getString("switchStatus"));
                roomType.put(IMAGE_ON, jsonObject.getString("onImage"));
                roomType.put(IMAGE_OFF, jsonObject.getString("offImage"));
                roomType.put(LOCK, jsonObject.getString("lockStatus"));
                roomType.put(HIDE, jsonObject.getString("hideStatus"));
                roomType.put(SWITCH_IMAGE_ID, "1");        //switchImageId //jsonObject.getString("switchIdentifer")
                roomType.put(ROOM_ID, mRoomId);
                roomType.put(ROOM_NAME, mRoomName);

                db.insertSwitchNew(roomType);
                switchDatas.add(roomType);
                switchDatasTemp.add(roomType);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void settextCount(int count) {
        int count1 = db.getSwitchRoomStatusCount(mRoomId);
        Log.d("Room ID", mRoomId);
        Log.d("Room Appliance ON", count1 + "");
        txtCount.setText(count1 + "");
    }

    private void optionDialog() {

        PopupMenu popupMenu = new PopupMenu(this, imgOption);
        popupMenu.setOnMenuItemClickListener(LivingRoom.this);
        if (sessionManager.getDemoUser().equals("DemoUser")) {
            popupMenu.inflate(R.menu.popup_memu_try);
        } else {
            popupMenu.inflate(R.menu.popup_menu);
        }
        popupMenu.show();

    }

    public boolean onMenuItemClick(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.item_hidden:

                onClickOfHideOption();
                return true;
            case R.id.item_turn:
                //chkStatus();
                Log.d("LivingRoom","turnOffAll: "+connectionValue);
                int a = db.getSwitchHideStatusCount(mRoomId);
                Log.d("hide_swich_count", "===" + a);

                if (a <= 0) {
                    Toast.makeText(context, "All switches are hide", Toast.LENGTH_SHORT).show();
                } else {
                    if (connectionValue == LOCAL_HUB) {
                        onClickOfTurnOFFAppliance("0", mRoomId, LOCAL_HUB);

                    } else {
                        onClickOfTurnOFFAppliance("0", mRoomId, INTERNET);

                    }
                }


                return true;
            case R.id.item_turn_on:
                int a1 = db.getSwitchHideStatusCount(mRoomId);
                Log.d("hide_swich_count", "===" + a1);
               // chkStatus();
                Log.d("LivingRoom","turnONAll: "+connectionValue);
                if (a1 <= 0) {
                    Toast.makeText(context, "All switches are hide", Toast.LENGTH_SHORT).show();
                } else {
                    if (connectionValue == LOCAL_HUB) {
                        onClickOfTurnOFFAppliance("1", mRoomId, LOCAL_HUB);

                    } else {
                        onClickOfTurnOFFAppliance("1", mRoomId, INTERNET);

                    }
                }


        }
        return false;
    }

    private void onClickOfTurnOFFAppliance(String OprationStatus, String mRoomId, String connectionValue) {
        if (sessionManager.getUserType().equals("2")) {
            Toast.makeText(context, "Don't have authority to perform operation.", Toast.LENGTH_SHORT).show();

        } else {
            if (db.isAlreadyONOFF(mRoomId, OprationStatus)) {

                //topic="refreshIntGenieHomeId_"+sessionManager.getHomeId();
                new AsyncTurnONAllAppliancesMqtt().execute(OprationStatus, connectionValue);

                //     new TurnOffAllSwitchAsyncTask().execute(OprationStatus,mRoomId);
            } else {
                String messge = OprationStatus.equals("0") ? "Switches are already OFF" : "Switches are already ON";
                Toast.makeText(context, messge, Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void setToolbar() {

        txtHeading.setText(mRoomName);
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgBack:
                LivingRoom.this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                break;
            case R.id.imgOption:
                optionDialog();
                break;

        }
    }

    private void onClickOfHideOption() {


        if (sessionManager.getUserType().equals("2")) {
            Toast.makeText(context, "Don't have authority to perform operation.", Toast.LENGTH_SHORT).show();
        } else {


            backFlag = false;
            Intent i = new Intent(this, HideList.class);
            i.putExtra(ROOM_ID, mRoomId);
            i.putExtra(ROOM_NAME, mRoomName);
            startActivity(i);
        }
    }


    private void addSwitchDialog1(String operation, String index) {

        Intent intent = new Intent(LivingRoom.this, EditOpetaion.class);
        intent.putExtra(OPERATION, operation);
        intent.putExtra(ROOM_ID, mRoomId);
        if (operation.equals("Edit")) {
            intent.putExtra(SWITCH_ID, switchId);
            if (!sessionManager.getUSERID().equals("")) {
                intent.putExtra(SWITCH_TYPE_ID, switchDatas.get(Integer.parseInt(index)).get(SWITCH_TYPE_ID));
                intent.putExtra(SWITCH_NAME, switchDatas.get(Integer.parseInt(index)).get(SWITCH_NAME));

            }
        }
        startActivity(intent);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedText = parent.getSelectedItem().toString();
        if (position != 0) {
            edtSwitchName.setText(selectedText);
            return;
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void initialiseControls() {
        context = LivingRoom.this;
        switchDatas = new ArrayList<HashMap<String, String>>();
        switchDatasTemp = new ArrayList<HashMap<String, String>>();
        sessionManager = new SessionManager(LivingRoom.this);
        txtCount = (TextView) findViewById(R.id.txtCount);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgOption = (ImageView) findViewById(R.id.imgOption);
        // txtUnhide = (TextView) findViewById(R.id.txtUnhide);
        lstSwitches = (RecyclerView) findViewById(R.id.lstSwitches);
        lstSwitches.setLayoutManager(new LinearLayoutManager(this));

        imgBack.setOnClickListener(this);
        imgOption.setOnClickListener(this);

        pDialog = new ProgressDialog(LivingRoom.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("LivingRoom Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        registerReceiver(receiver, filter);

    }


    @Override
    public void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    class AsyncHideOperation extends AsyncTask<String, Void, String> {
        HashMap<String, String> mapList;

        public AsyncHideOperation(HashMap mapList) {
            this.mapList = mapList;
        }

        @Override
        protected String doInBackground(String... params) {
            Constants request = new Constants();
            String response = null;
            try {

                if (Constants.isInternetAvailable(LivingRoom.this)) {
                    SessionManager session = new SessionManager(LivingRoom.this);
                    JSONObject jMain = new JSONObject();

                    jMain.put("switchId", mapList.get(SWITCH_ID));
                    jMain.put("hideStatus", params[0]);
                    jMain.put("userId", session.getUSERID());
                    jMain.put("messageFrom", messageType);
                    Log.d("hide_operation", "hide status===" + params[0]);
                    response = request.doPostRequest(URL_GENIE_AWS + "/switch/changehidestatus", jMain + "", session.getSecurityToken());
                    Log.d("hide_operation", "url===" + URL_GENIE_AWS + "/switch/changehidestatus");
                    Log.d("hide_operation", "json body===" + jMain);
                    Log.d("hide_operation", "response===" + response);
                    return response;
                } else {
                    Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
            }


            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                Log.d("Hide service status :", result);

                try {
                    JSONObject jObj = new JSONObject(result);
                    if (jObj.getString("status").equals("SUCCESS")) {
                        JSONObject jResult = new JSONObject(jObj.getString("result"));
                        hideOperation(mapList, jResult.getString("hideStatus"));
                        Toast.makeText(context, "Switch hide successfully", Toast.LENGTH_LONG).show();
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend3"));
                    } else {
                        Toast.makeText(context, "Fail hide the operation", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                }
            } else {

                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class AsyncLockOpration extends AsyncTask<String, Void, String> {

        HashMap<String, String> mapList;

        public AsyncLockOpration(HashMap mapList) {
            this.mapList = mapList;
        }

        @Override
        protected String doInBackground(String... params) {
            Constants request = new Constants();
            SessionManager manager = new SessionManager(LivingRoom.this);
            String response = null;
            try {

                if (Constants.isInternetAvailable(LivingRoom.this)) {

                    JSONObject jMain = new JSONObject();
                    jMain.put("switchId", mapList.get(SWITCH_ID));
                    jMain.put("lockStatus", params[0]);
                    jMain.put("userId", sessionManager.getUSERID());
                    jMain.put("messageFrom", messageType);

                    response = request.doPostRequest(URL_GENIE_AWS + "/switch/changelockstatus", jMain + "", manager.getSecurityToken());
                    Log.d("Lock_Body", "url===" + URL_GENIE_AWS + "/switch/changelockstatus");
                    Log.d("Lock_Body", "json object===" + jMain);
                    Log.d("Lock_Body", "response===" + response);
                    return response;

                } else {
                    Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                Log.d("Lock Service result:", result);
                try {
                    JSONObject jObj = new JSONObject(result);
                    if (jObj.getString("status").equals("SUCCESS")) {
                        JSONObject jResult = new JSONObject(jObj.getString("result"));
                        //hideOperation(mapList,jResult.getString("lockStatus"));
                        lockOperation(mapList, jResult.getString("lockStatus"));
                    } else {
                        Toast.makeText(context, "Fail to lock the operation", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                }
            } else {
                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        thisInstance = null;

        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageMqtt);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageActivity);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageActivity1);

        if (sessionManager.getDemoUser().equals("DemoUser") && backFlag) {
            this.finish();
            if (mqqtClient != null) {
                new AsyncDisconnect().execute();
            }
        }


    }

    class AsyncDisconnect extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                mqqtClient.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private BroadcastReceiver messageMqtt = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                if (thisInstance != null) {
                    ArrayList<HashMap<String, String>> switchDatas = db.getSwitches(mRoomId);
                    if (adapter != null) {
                        adapter.refreshSwitchView(switchDatas);
                    }
                    settextCount(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //setSwitchAdapter()
        }
    };

    private class AsyncTurnONAllAppliances extends AsyncTask<String, Void, String> {
        private String status;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LivingRoom.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants con = new Constants();
            String mResponse = null;
            try {
                SessionManager session = new SessionManager(LivingRoom.this);
                status = params[0];
                String mRequest = createJSONBody(params[0]);
                Log.d("Turn All Request ", mRequest);
                mResponse = con.doPostRequest(URL_GENIE + "/switch/changeStatusByRoom", mRequest, session.getSecurityToken());
            } catch (Exception e) {
            }

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }

            if (result != null && !result.isEmpty()) {

                Log.d("Living Turn All Room ", result);

                try {
                    JSONObject jResult = new JSONObject(result);
                    if (jResult.getString("status").equals("SUCCESS")) {
                        JSONArray jArrResult = new JSONArray(jResult.getString("result"));
                        for (int i = 0; i < jArrResult.length(); i++) {
                            JSONObject jSwitch = jArrResult.getJSONObject(i);
                            if (db.isDimmerSwitch(jSwitch.getString("siwtchid"))) {
                                db.updateAllDimmerSwitchStatus(jSwitch.getString("siwtchid"), status);
                            } else {
                                db.updateAllSwitchStatus(jSwitch.getString("siwtchid"), status);
                            }
                        }
                        // Toast.makeText(context, "Action is done successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, jResult.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                }
            } else {
                Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
            }
            setSwitchAdapter();
            settextCount(0);
        }


    }


    private String createJSONBody(String switchStatus) {
        JSONObject jMain = new JSONObject();
        try {
            jMain.put("roomId", mRoomId);
            jMain.put("onoffstatus", switchStatus);
            jMain.put("userId", sessionManager.getUSERID());
            jMain.put("messageFrom", messageType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jMain + "";
    }

    protected MqttClient getMqttConnection() throws MqttException {
        if (mqqtClient != null) {
            System.out.println("reusing connection...");
            return mqqtClient;
        } else {
            Log.d("check_mqtt", "Switch clicked inside1.2");
            setMqttClient();
            return mqqtClient;
        }
    }


    private class AsynOperation extends AsyncTask<Void, Void, MqttClient> {
        @Override
        protected MqttClient doInBackground(Void... params) {
            try {
                clientId = System.currentTimeMillis() + "";
                persistence = new MemoryPersistence();
                mqqtClient = new MqttClient(broker, clientId, persistence);

                Log.d("client_id1", "broker==" + broker);
                Log.d("client_id1", "clientId==" + clientId);
                Log.d("client_id1", "persistence==" + persistence);
                Log.d("client_id1", "topic==" + topic);

                connOpts = new MqttConnectOptions();
                connOpts.setConnectionTimeout(300);
                connOpts.setCleanSession(true);


                Log.d("client_id1", "connecting...");
                mqqtClient.connect(connOpts);
                Log.d("client_id1", "connected");
                //  mqqtClient.setCallback(LivingRoom.this);

                mqqtClient.subscribe(topic);
                Log.d("client_id1", "published");

            } catch (MqttException e) {
                e.printStackTrace();
                Log.d("SubScribe_Exception", e.toString());
            }
            return mqqtClient;
        }
    }

    private class AsyncTaskCheckRouter extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {

            InetAddress routername = null;

            Log.d("mqtt_checking", "checked");
            try {
                routername = InetAddress.getByName("192.168.0.119");
                if (routername.isReachable(300)) //5000
                {
                    Log.d("network_checking12", "Router is   reachable");
                    connectionValue = LOCAL_HUB;
                } else {
                    Log.d("network_checking12", "Router is not  reachable");
                    connectionValue = INTERNET;
                }

            } catch (java.io.IOException e) {
                Log.d("network_checking12", "Router EXCEPTION" + e.getMessage());
                e.printStackTrace();
            }

            return connectionValue;

        }


    }

    private class AsyncTurnONAllAppliancesMqtt extends AsyncTask<String, Void, String> {
        HashMap<String, String> map = null;
        String dimmerValue = "";
        String response = null;
        String mqttMessage = null;
        String onoffstatus;
        private ArrayList<HashMap<String, String>> switchDatas;

        @Override
        protected String doInBackground(String... params) {
            // setMqttClient();
            switchDatas = db.getSwitches(mRoomId);
            onoffstatus = params[0];
            connectionValue = params[1];

            setMqttClient();

            Log.d("switch_ON_OFF_ALL=", "ALL DATA" + switchDatas);
            int temp = switchDatas.size() - 1;

            for (int i = 0; i <= temp; i++) {

                if (connectionValue == LOCAL_HUB) {
                    if (switchDatas.get(i).get(DIMMER_STATUS).equals("1")) {
                        if (Integer.parseInt(switchDatas.get(i).get(DIMMER_VALUE)) < 1) {
                            dimmerValue = "50";
                            Log.d("testing_turn_on_all", "if dimmer value <10=" + dimmerValue);
                        } else {
                            dimmerValue = switchDatas.get(i).get(DIMMER_VALUE);
                            Log.d("testing_turn_on_all", "if dimmer value=" + dimmerValue);
                        }
                        mqttMessage = sessionManager.getUSERID() + "/" + switchDatas.get(i).get(SWITCH_NUMBER) + "/" + onoffstatus + "/" + dimmerValue + "/" + connectionValue + "/" + switchDatas.get(i).get(PANEL_NAME);
                        Log.d("testing_turn_on_all", "dimmer status=" + mqttMessage);
                    } else {
                        mqttMessage = sessionManager.getUSERID() + "/" + switchDatas.get(i).get(SWITCH_NUMBER) + "/" + onoffstatus + "/" + "0" + "/" + connectionValue + "/" + switchDatas.get(i).get(PANEL_NAME);
                        Log.d("testing_turn_on_all", "outside status=" + mqttMessage);
                    }
                } else {


                    if (switchDatas.get(i).get(DIMMER_STATUS).equals("1")) {
                        if (Integer.parseInt(switchDatas.get(i).get(DIMMER_VALUE)) < 1) {
                            dimmerValue = "50";
                            Log.d("testing_turn_on_all", "if dimmer value <10=" + dimmerValue);
                        } else {
                            dimmerValue = switchDatas.get(i).get(DIMMER_VALUE);
                            Log.d("testing_turn_on_all", "if dimmer value=" + dimmerValue);
                        }
                        mqttMessage = sessionManager.getUSERID() + "/" + switchDatas.get(i).get(SWITCH_NUMBER) + "/" + onoffstatus + "/" + dimmerValue + "/" + connectionValue + "/" + switchDatas.get(i).get(PANEL_NAME) + "/" + sessionManager.getHomeId();
                        Log.d("testing_turn_on_all", "dimmer status=" + mqttMessage);
                    } else {
                        mqttMessage = sessionManager.getUSERID() + "/" + switchDatas.get(i).get(SWITCH_NUMBER) + "/" + onoffstatus + "/" + "0" + "/" + connectionValue + "/" + switchDatas.get(i).get(PANEL_NAME) + "/" + sessionManager.getHomeId();
                        Log.d("testing_turn_on_all", "outside status=" + mqttMessage);
                    }


                }


                if (connectionValue == LOCAL_HUB) {
                    mqttMessage = "wish/Local_Switch_On_Off/" + mqttMessage;
                    Log.d("switch_ON_OFF_ALL=", "on off msg====" + mqttMessage);

                    MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());

                    message2.setQos(0);
                    try {
                        if (mqqtClient != null) {


                            //  getMqttConnection().publish(topic, message2);
                            mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL + sessionManager.getHomeId(), message2);
                            Log.d("publish_switch_all12", "on off msg====" + message2);

                        } else {

                            // setMqttClient();
                            //   getMqttConnection().publish(topic, message2);
                            mqttlConnection.getMqttConnectionLocal().publish(TOPIC_LOCAL + sessionManager.getHomeId(), message2);
                            Log.d("publish_switch_all12", "on off msg====" + message2);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    mqttMessage = "wish/Internet_Switch_On_Off/" + mqttMessage;
                    Log.d("switch_ON_OFF_ALL=", "on off msg====" + mqttMessage);

                    MqttMessage message2 = new MqttMessage(mqttMessage.getBytes());

                    message2.setQos(0);
                    try {
                        if (mqqtClient != null) {


                            mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET, message2);
                            Log.d("publish_switch_all12", "on off msg====" + message2);
                        } else {


                            mqttlConnection.getMqttConnectionInternet().publish(TOPIC_INTERNET, message2);
                            Log.d("publish_switch_all12", "on off msg====" + message2);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                db.updateSwitchStatus(switchDatas.get(i).get(SWITCH_ID), onoffstatus, switchDatas.get(i).get(DIMMER_VALUE));


            }
            return null;
        }

        protected void onPostExecute(String result) {

            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend1"));

        }
    }


}