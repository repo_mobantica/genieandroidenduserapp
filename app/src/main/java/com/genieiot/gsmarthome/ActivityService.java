package com.genieiot.gsmarthome;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 21/1/17.
 */

public class ActivityService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ActiveService", ".............onStartCommand");
        Log.d("ActiveService", "isNetworkAvailable: " + isNetworkAvailable(getApplicationContext()));
        if (isNetworkAvailable()) {
            new GetActivityMessagesTask().execute();
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            Log.d("ActivityService", "................No Internet Connection");
        }

        return START_STICKY;
    }

    class GetActivityMessagesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Constants constants = new Constants();
            SessionManager session = new SessionManager(getApplicationContext());
            String response = "";
            try {

                JSONObject json = new JSONObject();
                json.put("homeId", session.getHomeId());
                // response=constants.doGetRequest("http://"+session.getRouterIP()+":8080/GSmart_final_9jan/home/getactivitylist",session.getSecurityToken());
                // response=constants.doPostRequest("http://"+session.getRouterIP()+":8080/smart_home_local/home/getactivitylist",json+"",session.getSecurityToken());

                response = constants.doPostRequest(URL_GENIE_AWS + "/home/getactivitylist", json + "", session.getSecurityToken());

                Log.d("resonse_of_activity", "url===" + URL_GENIE_AWS + "/home/getactivitylist");
                Log.d("resonse_of_activity", "rjobjet===" + json);
                Log.d("resonse_of_activity", "response===" + response);

                if (response != null) {
                    parseActivityResult(response);
                }
            } catch (Exception e) {

                stopSelf();
                e.printStackTrace();
                return null;

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            stopSelf();
        }
    }

    private void parseActivityResult(String response) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date = new Date();
            String formattedDate = dateFormat.format(date);

            JSONObject jResult = new JSONObject(response);
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            if (jResult.getString("status").equals("SUCCESS")) {
                JSONArray jArrResult = new JSONArray(jResult.getString("result"));
                Log.d("Activity", "parseActivityResult: " + response);
                for (int i = 0; i < jArrResult.length(); i++) {
                    JSONObject jData = jArrResult.getJSONObject(i);
                    db.insertNotificationMessage(jData.getString("id"), jData.getString("message"), formattedDate, jData.getString("created_date"), jData.getString("roomname"), jData.getString("image"));
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend3"));
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;

        return connected;
    }

    public class RetrieveFeedTask extends AsyncTask<String, Void, Boolean> {


        @Override
        protected Boolean doInBackground(String... strings) {
            boolean success = false;
            try {
                URL url = new URL("https://google.com");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                // connection.setConnectTimeout(10000);
                connection.connect();
                success = connection.getResponseCode() == 200;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return success;

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d("ActivityService", ".............post execute result: " + aBoolean);
            if (aBoolean) {
                new GetActivityMessagesTask().execute();
            }

        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
