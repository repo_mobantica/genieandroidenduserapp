package com.genieiot.gsmarthome;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

import Database.DatabaseHandler;

import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_STATUS;

/**
 * Created by root on 10/10/16.
 */

public class AlarmReceiver extends BroadcastReceiver {

    Context mContext;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.mContext=context;

        Log.d("BroadcastReceiver ..","AlarmReceiver()") ;
        DatabaseHandler databaseHandler=new DatabaseHandler(mContext);
        HashMap<String,String> mMap=databaseHandler.getScheduleData(intent.getStringExtra(SWITCH_ID));
        String switchType=databaseHandler.getSwitchType(intent.getStringExtra(SWITCH_ID));

        Log.d("Schedule Data ",""+mMap);
          // Toast.makeText(mContext, intent.getStringExtra(SWITCH_ID),Toast.LENGTH_LONG).show();
        String  strConnection = ":!200:!245:!" + switchType + ":!" +mMap.get(SWITCH_STATUS)+":!0:!168";
        Log.d("Schedule Instruction  ",""+strConnection);
        new LongOperation().execute(strConnection,mMap.get(IP));
//        new AlarmReceiver.LongOperation().execute(arg1.getStringExtra("Command"));
//        //new AlarmReceiver.LongOperation().execute("R2_0\r\n");
//
//        int alarmId = arg1.getExtras().getInt("alarmId");
//        PendingIntent alarmIntent;
//        alarmIntent = PendingIntent.getBroadcast(arg0, alarmId,
//                new Intent(arg0, AlarmReceiver.class),
//                PendingIntent.FLAG_CANCEL_CURRENT);
////        cancelAlarm(alarmIntent);
//        AlarmManager alarmManager
//                = (AlarmManager) arg0.getSystemService(ALARM_SERVICE);
//        alarmManager.cancel(alarmIntent);

    }



    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                final String strConnection = params[0];
                String ip=params[1];
                setSwitchOn(strConnection,ip);

            } catch (final Exception e) {

                e.printStackTrace();

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
                    }
                });


            }
            return null;
        }
    }
    private void setSwitchOn(final String connection,String ip) throws Exception {

        Socket pingSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        try {
            pingSocket = new Socket(ip, 23);
            out = new PrintWriter(pingSocket.getOutputStream(), true);

        } catch (final IOException e) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mContext, "Check hotspot", Toast.LENGTH_SHORT).show();
                }
            });
            return;

        }

        out.println(connection);
        out.close();
        pingSocket.close();


    }

}
