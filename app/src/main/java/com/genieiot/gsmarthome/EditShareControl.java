package com.genieiot.gsmarthome;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;

import org.json.JSONObject;

import java.util.HashMap;

import Session.Constants;
import Session.SessionManager;

import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 11/14/16.
 */

public class EditShareControl extends AppCompatActivity implements View.OnClickListener{
    EditText edtEmail;
    RadioButton radioAdmin, radioNormal,radioModerateuser;
    Button btnShare;
    ImageView imgBack;
    private TextView txtHeading;
    private RadioGroup rgShareName;
    private String mSelectedUser;
    private ProgressDialog pDialog;
    String mEmail,mUserID;
    HashMap<String,String> mMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_shared_control);
        mMap=new HashMap<>();
        mMap= (HashMap<String, String>) getIntent().getSerializableExtra("UserInfo");
        initControl();
        setUserData();
    }

    private void setUserData() {
        try {
            edtEmail.setText(mMap.get("email"));
            edtEmail.setEnabled(false);
            String mUserType=mMap.get("userType");
            if(mUserType.equals("Admin")){
                mSelectedUser="0";
                radioAdmin.setChecked(true);
            }else if(mUserType.equals("Moderate User")){
                mSelectedUser="1";
                radioModerateuser.setChecked(true);
            }else if(mUserType.equals("Normal User")){
                mSelectedUser="2";
                radioNormal.setChecked(true);
            }

            mUserID=mMap.get("id");

        }catch(Exception e){}
    }

    private void initControl() {
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        radioAdmin = (RadioButton) findViewById(R.id.radioadmin);
        radioModerateuser = (RadioButton) findViewById(R.id.radioModerateuser);
        radioNormal = (RadioButton) findViewById(R.id.radionormaluser);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        btnShare = (Button) findViewById(R.id.btnSave);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        rgShareName= (RadioGroup) findViewById(R.id.rgShareName);
        txtHeading.setText("Edit ShareControl");

        txtHeading.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        rgShareName.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioadmin:
                        mSelectedUser=0+"";
                        break;
                    case R.id.radioModerateuser:
                        mSelectedUser=1+"";
                        break;

                    case R.id.radionormaluser:
                        mSelectedUser=2+"";
                        break;
                }

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSave:
                    onClickOfUpadate();
                break;
            case R.id.imgBack:
                this.finish();

        }

    }

    private void onClickOfUpadate() {
        SessionManager sessionManager=new SessionManager(this);

        if(!sessionManager.getUSERID().equals("")) {

        if(sessionManager.getUserType().equals("0")) {

            if (Constants.isInternetAvailable(EditShareControl.this)) {
                new AsyncUpdateUser().execute();

            } else {
                Toast.makeText(this, "Please check internet connection..!", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "You don't have any authority to edit the control.", Toast.LENGTH_SHORT).show();
        }

        }
        else{
            Toast.makeText(this,"You are Demo User.", Toast.LENGTH_SHORT).show();
        }
    }

    class AsyncUpdateUser extends AsyncTask<Void,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditShareControl.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants con=new Constants();
            String mResponse=null;
            try
            {
                SessionManager manager=new SessionManager(EditShareControl.this);
                String mRequest=createJSONBody();

                mResponse=con.doPostRequest(URL_GENIE_AWS + "/user/updatetype",mRequest,manager.getSecurityToken());
            }
            catch(Exception e){}

            return mResponse;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null && pDialog.isShowing() ) {
                pDialog.cancel();
            }
            if(result!=null) {
                parseResult(result);

            }
            else{
                Toast.makeText(EditShareControl.this, "Please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void parseResult(String result) {
        try
        {
            JSONObject response=new JSONObject(result);
            if(response.getString("status").equals("SUCCESS"))
            {
                Toast.makeText(this,"Share Control updated successfully.", Toast.LENGTH_SHORT).show();
             //   this.finish();
                Intent i =new Intent(this,ShareControlUserList.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }else{
                Toast.makeText(this,"Please try again.", Toast.LENGTH_SHORT).show();
            }
        }catch(Exception e){}
    }

    private String createJSONBody() {
        JSONObject jMain=new JSONObject();
        SessionManager manager=new SessionManager(EditShareControl.this);
        try{
            jMain.put("userId",mUserID);
            jMain.put("newUserType",mSelectedUser);
            jMain.put("adminUserId",manager.getUSERID());

        }catch(Exception e){}
        return jMain+"";
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }
}
