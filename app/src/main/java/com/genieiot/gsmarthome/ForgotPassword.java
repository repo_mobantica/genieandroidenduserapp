package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;

import org.json.JSONException;
import org.json.JSONObject;

import Session.Constants;

import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 10/20/16.
 */

public class ForgotPassword extends Activity implements View.OnClickListener {

    private EditText mEdtMail;
    private Button btnSave;
    private ProgressDialog pDialog;;
    private String tag_json_obj = "jobj_req";
    private String mEmailID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword);

        initControl();
    }

    private void initControl() {

        btnSave = (Button) findViewById(R.id.btnSave);
        mEdtMail = (EditText) findViewById(R.id.edtMail);
        btnSave.setOnClickListener(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
    }

    @Override
    public void onClick(View v)
    {
        if (isFieldSubmtted()) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            if (MainActivity.isInternetAvailable(ForgotPassword.this)) {

                mEmailID=mEdtMail.getText().toString();
                new ForgotPasswordAsync().execute();

            } else
            {
                myToast( MESSAGE_INTERNET_CONNECTION);
            }
        }
    }


    class ForgotPasswordAsync extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ForgotPassword.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String res="";
            //email
            try {
                JSONObject jObj = new JSONObject();
                jObj.put("email",mEmailID);
                res=net.doPostRequest(URL_GENIE_AWS + "/user/sendPasswordOTP", jObj.toString());
              //  res=net.doPostRequest(URL_GENIE_AWS + "/user/verifyEmailOTP", jObj.toString());
            }catch(Exception e){}
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result!=null){
                if(pDialog!=null){
                    pDialog.cancel();
                }
               try {
                   JSONObject response=new JSONObject(result);
                   if (response != null) {

                           if (response.getString("status").equals("SUCCESS")) {

                               String mResult = response.getString("result");
                               JSONObject resultObject = new JSONObject(mResult);

                               Intent intent = new Intent(ForgotPassword.this, OTP.class);
                               intent.putExtra("EmailId", mEdtMail.getText().toString());
                               intent.putExtra("USERID", resultObject.getString("userId"));
                               startActivity(intent);

                           }
                           else{
                               Toast.makeText(ForgotPassword.this, response.getString("msg"),Toast.LENGTH_SHORT).show();
                           }

                   }
               }catch(Exception e){}

            }else{
                Toast.makeText(ForgotPassword.this, getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }
    }


    public boolean validateEmail() {
        String emailInput, emailPattern;

        emailInput = mEdtMail.getText().toString().trim();

        emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (!emailInput.matches(emailPattern)) {
            return false;
        }

        return true;
    }
    private void myToast(String message) {
        Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();
    }


    private boolean isFieldSubmtted() {

        if (mEdtMail.getText().toString().length() < 1)
        {
            myToast(getResources().getString(R.string.MSG_VALID_MAIL));
            return false;
        }
        else if (!validateEmail())
        {
            myToast(getResources().getString(R.string.MSG_VALID_MAIL));
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }
}