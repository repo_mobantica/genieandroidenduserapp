package com.genieiot.gsmarthome;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.provider.SyncStateContract;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Database.DatabaseHandler;
import Fragments.Rooms;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.IMAGE_OFF;
import static Database.DatabaseHandler.IMAGE_ON;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_NUMBER;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.Constants.URL_GENIE_LOCAL;

/**
 * Created by root on 1/2/17.
 */

public class GetSwitchByRoomService extends Service {
    DatabaseHandler db;
    String connectionValue = "";
    public static final String LOCAL_HUB = "Localhub";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("onStartCommand() Switch", "start()");
        //new AsyncGetSwitchByRoomTask().execute();
        connectionValue = intent.getStringExtra("connectionValue");
        Log.d("GetSwitchByRoomService","connectionValue : "+connectionValue);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new AsyncGetSwitchByRoomTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new AsyncGetSwitchByRoomTask().execute();
        }



        return START_STICKY;
    }

    private class AsyncGetSwitchByRoomTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Constants request = new Constants();
            db = new DatabaseHandler(getApplicationContext());
            SessionManager session = new SessionManager(getApplicationContext());
            ArrayList<HashMap<String, String>> roomList = db.getRooms();

            for (int i = 0; i < roomList.size(); i++) {
                try {
                    JSONObject jObj = new JSONObject();
                    jObj.put("roomId", roomList.get(i).get(ROOM_ID));
                    Log.d("check_mqtt", "1");
                    //  String response=request.doPostRequest(URL_GENIE + "/switch/getswitchbyroom",jObj+"",session.getSecurityToken());  //ORIGINAL
                    String response = request.doPostRequest(URL_GENIE_AWS + "/switch/getswitchbyroom", jObj + "", session.getSecurityToken());
                    Log.d("Get_switch_response", "response ==" + URL_GENIE_AWS + "/switch/getswitchbyroom");
                    Log.d("Get_switch_response", "json obj===" + jObj);
                    Log.d("Get_switch_response12", "response===" + response);
                    if (response != null) {
                        parseSwitchResponse(response, roomList.get(i).get(ROOM_ID), roomList.get(i).get(ROOM_NAME));
                    }
                } catch (Exception e) {
                    if (!connectionValue.equalsIgnoreCase(LOCAL_HUB)) {
                         stopSelf();
                    }
                }


            }
            if (connectionValue != null) {
                if (connectionValue.equalsIgnoreCase(LOCAL_HUB)) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new AsyncUpdateSwitchStatusTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new AsyncUpdateSwitchStatusTask().execute();
                    }
                }
            }


            // LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!connectionValue.equalsIgnoreCase(LOCAL_HUB)) {
                stopSelf();
            }

        }
    }

    private void parseSwitchResponse(String response, String roomId, String roomName) {
        try {
            JSONObject jResult = new JSONObject(response);
            if (jResult.getString("status").equals("SUCCESS")) {
                JSONArray jArrResult = new JSONArray(jResult.getString("result"));

                for (int i = 0; i < jArrResult.length(); i++) {

                    HashMap<String, String> roomType = new HashMap<String, String>();
                    JSONObject jsonObject = null;
                    String mDimmer;
                    try {
                        jsonObject = new JSONObject(jArrResult.get(i).toString());

                        roomType.put(SWITCH_ID, jsonObject.getString("id"));
                        roomType.put(SWITCH_NAME, jsonObject.getString("switchName"));
                        roomType.put(SWITCH_TYPE_ID, jsonObject.getString("switchType"));
                        roomType.put(DIMMER_STATUS, jsonObject.getString("dimmerStatus"));

                        if (jsonObject.getString("dimmerValue") == null) {
                            mDimmer = "0";
                        } else {
                            mDimmer = jsonObject.getString("dimmerValue");
                        }

                        roomType.put(DIMMER_VALUE, mDimmer);
                        roomType.put(SWITCH_STATUS, jsonObject.getString("switchStatus"));
                        roomType.put(IMAGE_ON, jsonObject.getString("onImage"));
                        roomType.put(IMAGE_OFF, jsonObject.getString("offImage"));
                        roomType.put(LOCK, jsonObject.getString("lockStatus"));
                        roomType.put(HIDE, jsonObject.getString("hideStatus"));
                        roomType.put(SWITCH_NUMBER, jsonObject.getString("switchNumber"));
                        roomType.put(PANEL_NAME, jsonObject.getString("panelname"));


                        roomType.put(SWITCH_IMAGE_ID, "0");//jsonObject.getString("switchImageId")
                        roomType.put(ROOM_ID, roomId);
                        roomType.put(ROOM_NAME, roomName);

                        Log.d("Get_switch_response1", "===" + jsonObject.getString("hideStatus"));

                        //  db.getSwitches(roomId);


                        db.insertSwitchNew(roomType);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend1"));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (!connectionValue.equalsIgnoreCase(LOCAL_HUB)) {
                            stopSelf();
                        }
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            if (!connectionValue.equalsIgnoreCase(LOCAL_HUB)) {
                stopSelf();
            }
        }
    }

    private class AsyncUpdateSwitchStatusTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Constants request = new Constants();
            db = new DatabaseHandler(getApplicationContext());
            SessionManager session = new SessionManager(getApplicationContext());
            ArrayList<HashMap<String, String>> roomList = db.getRooms();

            for (int i = 0; i < roomList.size(); i++) {
                try {
                    JSONObject jObj = new JSONObject();
                    jObj.put("roomId", roomList.get(i).get(ROOM_ID));
                    Log.d("check_mqtt", "1");
                    //  String response=request.doPostRequest(URL_GENIE + "/switch/getswitchbyroom",jObj+"",session.getSecurityToken());  //ORIGINAL
                    String response = request.doPostRequest(URL_GENIE_LOCAL + "/switch/getswitchbyroom", jObj + "", session.getSecurityToken());
                    Log.d("GetSwitchByRoomService", "response ==" + URL_GENIE_LOCAL + "/switch/getswitchbyroom");
                    Log.d("GetSwitchByRoomService", "json obj===" + jObj);
                    Log.d("GetSwitchByRoomService", "response===" + response);
                    if (response != null) {
                        parseSwitchStatusResponse(response, roomList.get(i).get(ROOM_ID), roomList.get(i).get(ROOM_NAME));
                    }
                } catch (Exception e) {
                    // stopSelf();
                }
            }

            // LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //  stopSelf();
        }
    }

    private void parseSwitchStatusResponse(String response, String roomId, String roomName) {
        try {
            JSONObject jResult = new JSONObject(response);
            if (jResult.getString("status").equals("SUCCESS")) {
                JSONArray jArrResult = new JSONArray(jResult.getString("result"));

                for (int i = 0; i < jArrResult.length(); i++) {

                    HashMap<String, String> roomType = new HashMap<String, String>();
                    JSONObject jsonObject = null;
                    String mDimmer;
                    try {
                        jsonObject = new JSONObject(jArrResult.get(i).toString());

                        roomType.put(SWITCH_ID, jsonObject.getString("id"));
                        roomType.put(SWITCH_STATUS, jsonObject.getString("switchStatus"));
                        roomType.put(DIMMER_STATUS, jsonObject.getString("dimmerStatus"));

                        if (jsonObject.getString("dimmerValue") == null) {
                            mDimmer = "0";
                        } else {
                            mDimmer = jsonObject.getString("dimmerValue");
                        }

                        roomType.put(DIMMER_VALUE, mDimmer);
                        roomType.put(ROOM_ID, roomId);

                        Log.d("Get_switch_response1", "===" + jsonObject.getString("hideStatus"));

                        db.updateSwitchStatus(roomType);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend1"));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        stopSelf();
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            stopSelf();
        }
    }
}
