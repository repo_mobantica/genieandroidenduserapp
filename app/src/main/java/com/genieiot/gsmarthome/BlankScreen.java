package com.genieiot.gsmarthome;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import Session.Constants;
import Session.SessionManager;

import static Session.Constants.LOCAL_HUB;

/**
 * Created by root on 28/12/16.
 */

public class BlankScreen extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rrError;
    private Button btnRetry;
    private ProgressDialog processsDailog1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blank_activity);
        rrError= (RelativeLayout) findViewById(R.id.rrError);
        btnRetry= (Button)findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(this);
       // new AsyncHubIP().execute();
      //  new NetworkSniffTask(BlankScreen.this).execute();



     //   new AsyncUserDataTask().execute();


        //code by me
        Intent intent = new Intent(BlankScreen.this, MainActivity.class);
        BlankScreen.this.startActivity(intent);
        //end
    }

    @Override
    public void onClick(View v) {
     switch (v.getId()){
         case R.id.btnRetry:
             //new AsyncHubIP().execute();
             new AsyncUserDataTask().execute();
             break;
     }
    }

    public class AsyncHubIP extends AsyncTask<Void,Void,Void> {
        ProgressDialog processsDailog=new ProgressDialog(BlankScreen.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processsDailog.setMessage("Searching Genie Hub...");
            processsDailog.setCancelable(false);
            processsDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            BufferedReader br = null;
            boolean isFirstLine = true;

            try {
                br = new BufferedReader(new FileReader("/proc/net/arp"));
                String line;

                while ((line = br.readLine()) != null) {
                    if (isFirstLine) {
                        isFirstLine = false;
                        continue;
                    }

                    String[] splitted = line.split(" +");

                    if (splitted != null && splitted.length >= 4) {

                        String ipAddress = splitted[0];
                        String macAddress = splitted[3];

                        boolean isReachable = InetAddress.getByName(
                                splitted[0]).isReachable(500);  // this is network call so we cant do that on UI thread, so i take background thread.
                        if (isReachable) {
                            Log.d("Device Information", ipAddress + " : "
                                    + macAddress);
                            // HashMap<String,String> mMap=new HashMap<>();
                            // mMap.put("IP",ipAddress);
                            // mMap.put("MAC",macAddress);
                            Constants constant=new Constants();
                            String response=constant.doGetRequest("http://www.macvendorlookup.com/api/v2/"+macAddress);
                            // String response=constant.doPostRequest("http://www.macvendorlookup.com/api/v2/"+mac,"");
                            // Log.select_menu("Response : ",""+response);
                            if(response!=null && !response.isEmpty()){

                                JSONArray jArr=new JSONArray(response);
                                String strCompanyName=jArr.getJSONObject(0).getString("company");

                                SessionManager session=new SessionManager(BlankScreen.this);
                                if(strCompanyName!=null &&!strCompanyName.isEmpty()){
                                    if(strCompanyName.equals("Raspberry Pi Foundation")){
                                        session.setAppUrl(ipAddress);
                                        session.setAppUrlFlag(true);
                                        Log.d("Hub Info : ",ipAddress);
                                        break;
                                    }
                                }


                            }

                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(processsDailog!=null && processsDailog.isShowing()){
                processsDailog.cancel();
            }

            SessionManager sessionManager=new SessionManager(BlankScreen.this);
            if(sessionManager.getAPPURL().equals("")){
                Toast.makeText(BlankScreen.this,"Hub is not found.", Toast.LENGTH_SHORT).show();
                rrError.setVisibility(View.VISIBLE);
            }


            if(sessionManager.getUserSync()) {
                     new AsyncUserDataTask().execute();
            }

        }
    }

    class AsyncUserDataTask extends AsyncTask<Void,Void,String>{

        SessionManager sessionManager=new SessionManager(BlankScreen.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processsDailog1=new ProgressDialog(BlankScreen.this);
            processsDailog1.setMessage("Finding hub data...");
            processsDailog1.setCancelable(false);
            processsDailog1.show();
        }

        @Override
        protected String doInBackground(Void... params) {

            String mResponse=null;
            Constants req=new Constants();
            try{
                JSONObject jBody=new JSONObject();
                jBody.put("id",sessionManager.getUSERID());
                jBody.put("firstName",sessionManager.getFirstName());
                jBody.put("lastName",sessionManager.getLastName());
                jBody.put("email",sessionManager.getEmail());
                jBody.put("password",sessionManager.getPassword());
                jBody.put("phoneNumber",sessionManager.getPhone());
                jBody.put("userType",sessionManager.getUserType());
                jBody.put("image",sessionManager.getUserImage());
                jBody.put("deviceId",sessionManager.getDeviceToken());
                jBody.put("deviceType","ANROID");
                jBody.put("isEmailVerified",1);
                jBody.put("isFirstLogin",0);
                String homeId=sessionManager.getHomeId();
                jBody.put("homeid",homeId);

                Log.d("syncShareControlData ",jBody+"");


                String url ="http://"+sessionManager.getAPPURL()+":8080/smart_home_local/user/syncShareControlData";
                Log.d("syncsharecontrol_url","url->"+url);
                mResponse=req.doPostRequest("http://"+sessionManager.getAPPURL()+":8080/smart_home_local/user/syncShareControlData",jBody+"",sessionManager.getSecurityToken());
                Log.d("token","token: "+sessionManager.getSecurityToken());

                Log.d("syncShareControlData_ok","mResponse"+mResponse);
            }
            catch(Exception e){
                Log.d("error",e.getMessage());
            };

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(processsDailog1!=null && processsDailog1.isShowing()){
                processsDailog1.dismiss();
            }
            try{
                if(result!=null){
                Log.d("syncUserControl Result ",result);

                    JSONObject jMain=new JSONObject(result);
                    if(jMain.getString("status").equals("SUCCESS")){
                        sessionManager.setUserSync(true);
                    }
                    else{
                        rrError.setVisibility(View.VISIBLE);
                        Toast.makeText(BlankScreen.this,jMain.getString("msg"),Toast.LENGTH_SHORT).show();
                    }
            }else{

                rrError.setVisibility(View.VISIBLE);
                //Toast.makeText(BlankScreen.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
            }catch(Exception e){e.printStackTrace();};

            Intent intent = new Intent(BlankScreen.this, MainActivity.class);
            BlankScreen.this.startActivity(intent);
            finish();
        }
    }


    public class NetworkSniffTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress;
        private static final String TAG = "nstask";
        Context mContext;

        ArrayList<HashMap<String,String>> DeviceInfo=new ArrayList<>();
        private WeakReference<Context> mContextRef;


        public NetworkSniffTask(Context context) {
            mContextRef = new WeakReference<Context>(context);
            mContext=context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress=new ProgressDialog(mContext);
            progress.setMessage("Searching Genie Hub...");
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "Let's sniff the network");

            try {
                Context context = mContextRef.get();

                if (context != null) {

                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                    WifiInfo connectionInfo = wm.getConnectionInfo();
                    int ipAddress = connectionInfo.getIpAddress();
                    String ipString = Formatter.formatIpAddress(ipAddress);


                    Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
                    Log.d(TAG, "ipString: " + String.valueOf(ipString));

                    String prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1);
                    Log.d(TAG, "prefix: " + prefix);

                    SessionManager session=new SessionManager(mContext);
                    for (int i = 0; i < 255; i++) {

                        String testIp = prefix + String.valueOf(i);
                        InetAddress address = InetAddress.getByName(testIp);
                        boolean reachable = address.isReachable(500);
                        String hostName = address.getCanonicalHostName();

                        if (reachable) {
                            HashMap<String,String> map=new HashMap<>();
                            String companyName=getMacFromArpCache(hostName);
                            map.put("IP",hostName);
                            map.put("CompanyName",companyName);

                            DeviceInfo.add(map);
                            Log.i("Device Info ","Company Name "+companyName+" Host Ip : "+hostName);
                            Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");

                            if(companyName!=null &&!companyName.isEmpty()){
                                if(companyName.equals("Raspberry Pi Foundation")){
                                    session.setAppUrl(hostName);
                                    session.setAppUrlFlag(true);
                                    break;
                                }
                            }

                        }
                    }

                }
            } catch (Throwable t) {
                Log.e(TAG, "Well that's not good.", t);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(progress!=null && progress.isShowing()){
                progress.cancel();
            }

            SessionManager sessionManager=new SessionManager(BlankScreen.this);
            if(sessionManager.getAPPURL().equals("")){
                Toast.makeText(BlankScreen.this,"Hub is not found.", Toast.LENGTH_SHORT).show();
                rrError.setVisibility(View.VISIBLE);
            }

            if(sessionManager.getAppUrlFlag())
            {
                new AsyncUserDataTask().execute();
            }


        }
    }

    public static String getMacFromArpCache(String ip) {
        if (ip == null)
            return null;
        String strCompanyName="";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && ip.equals(splitted[0])) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {

                        Constants constant=new Constants();
                        String response=constant.doGetRequest("http://www.macvendorlookup.com/api/v2/"+mac);
                        // String response=constant.doPostRequest("http://www.macvendorlookup.com/api/v2/"+mac,"");
                        // Log.select_menu("Response : ",""+response);
                        if(response!=null && !response.isEmpty()){
                            JSONArray jArr=new JSONArray(response);
                            strCompanyName=jArr.getJSONObject(0).getString("company");
                        }
                        return strCompanyName;
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
