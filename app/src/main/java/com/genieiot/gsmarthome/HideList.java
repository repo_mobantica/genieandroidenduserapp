package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Adapter.HideListAdapter;
import Adapter.RoomHideListAdapter;
import Database.DatabaseHandler;
import Session.Constants;
import Session.IOnClickListner;
import Session.SessionManager;

import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 23/11/16.
 */

public class HideList extends Activity implements View.OnClickListener,IOnClickListner {

    TextView mTxtHeading, mTxtUnhide;
    ImageView mImgBack;
    RecyclerView recyclerView;
    HideListAdapter adapter;
    private RoomHideListAdapter adapterRoom;
    ArrayList<HashMap<String, String>> arrayList;
    private ProgressDialog pDialog;
    public static String mRoomId, mRoomName;
    DatabaseHandler db;
    SessionManager sessionManager;
    private String messageType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hidelist);
        initialiseControls();
        db = new DatabaseHandler(this);
        sessionManager=new SessionManager(this);

        Intent intent = getIntent();
        mRoomId = intent.getStringExtra(ROOM_ID);
        mRoomName = intent.getStringExtra(ROOM_NAME);

        if(URL_GENIE.equals(URL_GENIE_AWS)){
            messageType=INTERNET;
        }
        else{
            messageType=LOCAL_HUB;
        }

        setAdapter();

    }

    private void initialiseControls() {

        mTxtHeading = (TextView) findViewById(R.id.txtHeading);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        arrayList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        mImgBack.setOnClickListener(this);

    }
    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(refreshAdapter, new IntentFilter("NotificationSend"));

        if(mRoomId.equals("0"))
        {
            mTxtHeading.setText("Hidden Rooms");
        }
        else
        {
            mTxtHeading.setText("Hidden Switches");
        }
    }
    private void setAdapter() {

        if(mRoomId.equals("0")) {

            arrayList = db.getHideRooms();
            if (arrayList.size() == 0) {
                Toast.makeText(getApplicationContext(), "No record found", Toast.LENGTH_SHORT).show();
            }
            adapterRoom = new RoomHideListAdapter(HideList.this, arrayList);
            recyclerView.setAdapter(adapterRoom);
            adapterRoom.notifyDataSetChanged();

        }else{

            arrayList = db.getHideSwitches(mRoomId);
            Log.d("hidden_list","===="+arrayList);
            if (arrayList.size() == 0) {
                Toast.makeText(getApplicationContext(), "No record found", Toast.LENGTH_SHORT).show();
            }
            adapter = new HideListAdapter(HideList.this, arrayList);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgBack:
                HideList.this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                break;
        }

    }

    private void UpdateHideStatus(HashMap<String, String> mapHide, String hideStatus) {
        DatabaseHandler database=new DatabaseHandler(this);
        if(mRoomId.equals("0")){
            database.updateRoomHidestatus(mapHide.get(ROOM_ID),"0");
        }else {
            database.UpdateHideStatus(mRoomId, mapHide.get(SWITCH_ID), hideStatus);
        }

    }



    @Override
    public void onClickListner(HashMap<String, String> map) {

        if(sessionManager.getDemoUser().equals("DemoUser")){
            UpdateHideStatus(map, "0");
            setAdapter();
        }
        else
        {
            if(sessionManager.getUserType().equals("2")) {
                Toast.makeText(HideList.this, "Don't have authority to Hide the Switch.", Toast.LENGTH_LONG).show();
            }
            else {
                // this method is  use for Room fragment;s hidden option
                if (mRoomName.equals("0"))
                {
                    UpdateHideStatus(map, "0");
                    setAdapter();
                } else {
                    //This is for living room's hidden option
                    new HideSwitchesAsyncTask(map).execute("0");
                }
            }
        }
    }
    private class HideSwitchesAsyncTask extends AsyncTask<String, Void, String> {
        HashMap<String, String> mapHide;

        public HideSwitchesAsyncTask(HashMap mapList) {
            this.mapHide = mapList;
        }

        @Override
        protected String doInBackground(String... params) {
            Constants request = new Constants();
            SessionManager session=new SessionManager(HideList.this);
            String response = null;
            try {
                Log.d("hide_param","=="+params[0]);

                if (Constants.isInternetAvailable(HideList.this)) {
                    JSONObject jMain = new JSONObject();
                    jMain.put("switchId", mapHide.get(SWITCH_ID));
                    jMain.put("hideStatus", params[0]);
                    jMain.put("userId",session.getUSERID());
                    jMain.put("messageFrom",messageType);

                    Log.d("hidden_list1",""+jMain);
                    response = request.doPostRequest(URL_GENIE_AWS + "/switch/changehidestatus", jMain + "",session.getSecurityToken());
                    Log.d("hidden_list1","===="+response);

                    return response;
                } else {
                    Toast.makeText(HideList.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {


                try {
                    JSONObject jObj = new JSONObject(result);
                    if (jObj.getString("status").equals("SUCCESS")) {

                        JSONObject jResult = new JSONObject(jObj.getString("result"));
                        UpdateHideStatus(mapHide, jResult.getString("hideStatus"));
                        setAdapter();


                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend1"));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("refreshRoom"));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend2"));

                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("HiddenStatus"));


                        finish();


                    }
                    else
                    {
                        Toast.makeText(HideList.this, "Fail hide operation", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(HideList.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }

    }



    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(refreshAdapter);
    }



    private BroadcastReceiver refreshAdapter=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setAdapter();
        }
    };


}
