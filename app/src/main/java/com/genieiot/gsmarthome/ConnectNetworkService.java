package com.genieiot.gsmarthome;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.CREATED_BY;
import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_NUMBER;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.NOTINTERNET;


public class ConnectNetworkService extends IntentService implements MqttCallback {

    String INTERNET_TEST = "INTERNET_TEST";
    SessionManager sessionManager;
    String connectionValue;
    String broker = "", subTopic = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    public static ArrayList<HashMap<String, String>> switchData;
    private String messageType = "";
    private String BROADCAST_ACTION = "mqqtconnectionreceiver";

    public ConnectNetworkService() {
        super("ConnectNetworkService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            INTERNET_TEST = intent.getStringExtra("INTERNET_TEST");
        }
        sessionManager = new SessionManager(getApplicationContext());
        setInternetSataus(INTERNET_TEST);


    }

    private void setInternetSataus(String internet_test) {
        if (getApplicationContext() != null) {
            if (internet_test.equals("1")) {

                chkStatus();
                Intent broadCastIntent = new Intent();
                broadCastIntent.setAction(BROADCAST_ACTION);
                broadCastIntent.putExtra("connectionValue", connectionValue);
                sendBroadcast(broadCastIntent);



               /* if (connectionValue == LOCAL_HUB) {

                    Log.d("connection_val", "====" + "wifi1");
                    subTopic = "updateStatus1_" + sessionManager.getHomeId();
                    Log.d("connection_val", "====" + "wifi2");
                    Log.d("connection_val", "topic" + subTopic);
                    //   mqttlConnection.getMqttConnectionLocal();
                    setBrokerLocal("tcp://192.168.0.119:1883");
                    Log.d("connection_val", "====" + "wifi3");
                } else {

                    subTopic = "updateStatus2_" + sessionManager.getHomeId();
                    Log.d("connection_val", "====" + "internet1");
                    //  mqttlConnection.getMqttConnectionInternet().subscribe(subTopic);
                    setBrokerInternet("tcp://103.12.211.52:1883");
                    Log.d("connection_val", "====" + "internet2");
                }*/


            } else if (internet_test.equals("2")) {
                Log.d("connection_val", "====" + "internet_all");
                subTopic = "updateStatus2_" + sessionManager.getHomeId();
                // mqttlConnection.getMqttConnectionInternet().subscribe(subTopic);
                setBrokerInternet("tcp://103.12.211.52:1883");
                Log.d("connection_val", "====" + "internet_all1");

            } else if (internet_test.equals("0")) {
                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
                //   Toast.makeText(getApplicationContext(), "ok3", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public String chkStatus() {
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            try {
                new AsyncTaskCheckRouter1().execute().get();
               /* String result = new AsyncTaskCheckInternet().execute().get();
                if (result != null) {
                    Log.d("Get_room_response : ", result + "");
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String results = response.getString("response");
                        String statusCode = response.getString("statusCode");

                        if (status.equals("SUCCESS")) {
                            connectionValue = LOCAL_HUB;
                        } else {
                            connectionValue = INTERNET;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    connectionValue = INTERNET;
                }*/

            } catch (InterruptedException e) {
                Log.d("connection_val", "checked" + e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("connection_val", "checked" + e.getMessage());
                e.printStackTrace();
            }

            return connectionValue;
        } else if (mobile.isConnectedOrConnecting()) {
            connectionValue = INTERNET;
            return connectionValue;
        } else {
            connectionValue = NOTINTERNET;
            return connectionValue;
        }

    }
    private class AsyncTaskCheckInternet extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            Constants request=new Constants();
            String mResponse=null;
            try {

                JSONObject jMain=new JSONObject();
                jMain.put("homeId",sessionManager.getHomeId());

                mResponse=request.doPostInternetRequest("http://192.168.0.119:8080/smart_home_local/home/verifygohub",jMain+"");
                Log.d("Get_room_response","response val ="+mResponse);


            }catch(Exception e){

            }

            return mResponse;
        }
    }
    private class AsyncTaskCheckRouter1 extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... strings) {

            InetAddress routername = null;

            Log.d("mqtt_checking", "checked");
            try {
                routername = InetAddress.getByName("192.168.0.119");
                if (routername.isReachable(300)) //5000
                {
                    Log.d("connection_val", "Router is   reachable");
                    connectionValue = LOCAL_HUB;
                } else {
                    Log.d("connection_val", "Router is not  reachable");
                    connectionValue = INTERNET;
                }

            } catch (java.io.IOException e) {
                Log.d("network_checking12", "Router EXCEPTION" + e.getMessage());
                e.printStackTrace();
            }

            return connectionValue;

        }


    }

    private void setBrokerLocal(String mBroker) {
        try {


            broker = "tcp://192.168.0.119:1883";

            subTopic = "updateStatus1_" + sessionManager.getHomeId();
            broker = mBroker;
            clientId = System.currentTimeMillis() + "";
            persistence = new MemoryPersistence();


            mqqtClient = new MqttClient(broker, clientId, persistence);
            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);

            Log.d("client_id151", "broker==" + broker);
            Log.d("client_id151", "clientId==" + clientId);
            Log.d("client_id151", "persistence==" + persistence);
            Log.d("client_id151", "topic==" + subTopic);


            mqqtClient.connect(connOpts);
            Log.d("client_id151", "connected");
            mqqtClient.setCallback(this);

            mqqtClient.subscribe(subTopic);
            Log.d("client_id151", "subscribe");
            Log.d("Set_Broker_MainActivity", broker + " " + subTopic);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.d("SubScribe Exception", e.toString());
        }
    }


    private void setBrokerInternet(String mBroker) {
        try {


            broker = "tcp://103.12.211.52:1883";

            subTopic = "updateStatus2_" + sessionManager.getHomeId();
            broker = mBroker;
            clientId = System.currentTimeMillis() + "";
            persistence = new MemoryPersistence();


            mqqtClient = new MqttClient(broker, clientId, persistence);
            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);

            mqqtClient.connect(connOpts);
            mqqtClient.setCallback(this);
            Log.d("connection_val", "====" + "internet_all12");
            mqqtClient.subscribe(subTopic);
            Log.d("Set_Broker_MainActivity", broker + " " + subTopic);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.d("SubScribe Exception", e.toString());
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
        try {
            Log.d("MainActivity -->", "Connection Lost");
            Thread.sleep(4000);
            getMqttConnection(broker);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        // Log.select_menu("MainActivity ","MessArrived "+s);
        //   parseMqttCallBack(mqttMessage.toString());

        Log.d("service Update_status_log15", "msg== " + mqttMessage);
        Log.d("service Update_status_log15", "topic== " + s);

        try {
            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
            String temp = mqttMessage.toString();

            if (temp.charAt(0) == '$') {

                String switchNumber = String.valueOf(temp.charAt(2));
                String swtchStatus = String.valueOf(temp.charAt(3));
                String dimmerval = String.valueOf(temp.charAt(4)) + String.valueOf(temp.charAt(5));
                String panel = temp.charAt(7) + "" + temp.charAt(8) + "" + temp.charAt(9) + "" + temp.charAt(10) + "" + temp.charAt(11) + "" + temp.charAt(12);

                Log.d("switch_stat12", "msg=" + temp);
                Log.d("switch_stat12", "switchNumber=" + switchNumber);
                Log.d("switch_stat12", "swtchStatus=" + swtchStatus);
                Log.d("switch_stat12", "dimmerval=" + dimmerval);
                Log.d("switch_stat12", "panel=" + panel);


                db.updateSwitchStatusMqtt(switchNumber, swtchStatus, dimmerval, panel);

                switchData = db.getSwitchLog(switchNumber, panel);
                Log.d("seitch_data_after_log", "===" + switchData);


                Calendar calander;
                String time;
                calander = Calendar.getInstance();

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                time = simpleDateFormat.format(calander.getTime());

                HashMap<String, String> switcch = new HashMap<String, String>();

                switcch.put(DIMMER_STATUS, switchData.get(0).get(DIMMER_STATUS));
                switcch.put(PANEL_NAME, switchData.get(0).get(PANEL_NAME));
                switcch.put(SWITCH_TYPE_ID, switchData.get(0).get(SWITCH_TYPE_ID));
                switcch.put(SWITCH_NAME, switchData.get(0).get(SWITCH_NAME));
                switcch.put(SWITCH_NUMBER, switchData.get(0).get(SWITCH_NUMBER));
                switcch.put(DIMMER_VALUE, switchData.get(0).get(DIMMER_VALUE));

                switcch.put(ROOM_ID, switchData.get(0).get(ROOM_ID));
                switcch.put(SWITCH_STATUS, switchData.get(0).get(SWITCH_STATUS));

                switcch.put(SWITCH_ID, switchData.get(0).get(SWITCH_ID));
                switcch.put(ROOM_NAME, switchData.get(0).get(ROOM_NAME));
                switcch.put(SWITCH_IMAGE_ID, switchData.get(0).get(SWITCH_IMAGE_ID));
                switcch.put(TIME, time);


                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Date date = new Date();
                String formattedDate = dateFormat.format(date);
                switcch.put(CREATED_BY, formattedDate);

                db.insertSwitchRecentlog(switcch);


                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend1"));
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend2"));

            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    private MqttClient getMqttConnection(String broker) throws MqttException {
        if (mqqtClient != null) {
            System.out.println("MainActivity reusing connection...");
            return mqqtClient;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new AsynMqttCallbackTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, broker);
            } else {
                new AsynMqttCallbackTask().execute(broker);
            }
            return mqqtClient;
        }
    }

    private class AsynMqttCallbackTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            // setBroker(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (messageType.equals(INTERNET)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    //   new AsyncInternetRoomListTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    //  new AsyncInternetRoomListTask().execute();
                }
            }
        }
    }
}
