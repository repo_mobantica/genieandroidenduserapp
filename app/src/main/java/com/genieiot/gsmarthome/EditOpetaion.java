package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.MainActivity.isInternetAvailable;
import static com.genieiot.gsmarthome.MainActivity.myToast;

/**
 * Created by root on 11/24/16.
 */

public class EditOpetaion extends Activity implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    private EditText edtSwitchName;
    private Button btnUpdate;
    private String mSwitchID;
    private ProgressDialog pDialog;
    private ImageView imgBack;
    private TextView txtHeading;
    private Spinner spnRoomType;
    private String switchImageID;
    ArrayAdapter<String> dataAdapter;
    SessionManager sessionManager;
    private boolean selectionFlag=false;
   // private String TAG=getLocalClassName().toString();

    String[] switchList=new String[]{"Bulb","AC","Chandelier","Cooler","Desk Lamp","Desktop","DISH","Exhaust","Fan","Refrigerator",
            "Microwave","Mixer","Purifier","Socket","Sound System","Stove",
            "Table Fan","TV","Tube","Washing Machine","Water Heater"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_opration_activity);
        initComponent();
        HashMap<String,String> mapList= (HashMap<String, String>) getIntent().getSerializableExtra("SwitchInfo");
        Log.d("edit_list","switchinfo==="+mapList);

        mSwitchID=mapList.get(SWITCH_ID);
        edtSwitchName.setText(mapList.get(SWITCH_NAME));
        edtSwitchName.setSelection(edtSwitchName.getText().length());

        List switchListL = Arrays.asList(switchList);
        Log.d("edit_list","switchlist==="+switchListL);

        dataAdapter = new ArrayAdapter<String>(EditOpetaion.this,R.layout.textview_black_background,switchListL);
        spnRoomType.setAdapter(dataAdapter);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        try {
            spnRoomType.setSelection(Integer.parseInt(mapList.get(SWITCH_IMAGE_ID)));
        }catch(Exception e){}

    }

    private void initComponent() {

        edtSwitchName= (EditText) findViewById(R.id.edtSwitchName);
        btnUpdate= (Button) findViewById(R.id.btnUpdate);
        imgBack= (ImageView) findViewById(R.id.imgBack);
        txtHeading= (TextView)findViewById(R.id.txtHeading);
        spnRoomType= (Spinner) findViewById(R.id.spnRoomType);

        btnUpdate.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        spnRoomType.setOnItemSelectedListener(this);
        sessionManager=new SessionManager(this);

        txtHeading.setText("Edit Switch");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnUpdate:
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                if (isInternetAvailable(getApplicationContext()))
                {
                    OnclickOfEdit();
                }else
                {
                    myToast(this, MESSAGE_INTERNET_CONNECTION);
                }


                break;

            case R.id.imgBack:
                EditOpetaion.this.finish();
                break;
        }
    }

    private void OnclickOfEdit() {
        String mSwitchName=edtSwitchName.getText().toString();
        if(mSwitchName.equals("")){
            Toast.makeText(this, "Please enter switch name.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(Constants.isInternetAvailable(this)){
            if(sessionManager.getDemoUser().equals("DemoUser")) {
                DatabaseHandler db=new DatabaseHandler(this);
                db.updateSwitchRoomName(mSwitchID,mSwitchName);
                db.updateSwitchImageId(mSwitchID,switchImageID);
                db.updateRecntSwitchImageId(mSwitchID,switchImageID,mSwitchName);
                Toast.makeText(EditOpetaion.this,"Edit successfully.", Toast.LENGTH_SHORT).show();
                finish();
            }
            else {
                new AsyncEditSwitchTask().execute(mSwitchName);
            }
        }else{
            Toast.makeText(this,"Please check internet connection.", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switchImageID=position+"";
        if(selectionFlag) {
            edtSwitchName.setText(spnRoomType.getSelectedItem().toString());
        }else{
            selectionFlag=true;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class AsyncEditSwitchTask extends AsyncTask<String,Void,String> {
        String switchName;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditOpetaion.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants constant=new Constants();
            String mResponse=null,messageType;
            switchName=params[0];

            try{
                if(URL_GENIE.equals(URL_GENIE_AWS)){
                    messageType=INTERNET;
                }
                else{
                    messageType=LOCAL_HUB;
                }

                JSONObject jReqBody=new JSONObject();
                jReqBody.put("switchId",mSwitchID);
                jReqBody.put("switchName",params[0]);
                jReqBody.put("userId",sessionManager.getUSERID());
                jReqBody.put("messageFrom",messageType);
                jReqBody.put("switchImageId",switchImageID);
                Log.d("Edit_Operation","Edit Body "+jReqBody+"");
                mResponse=constant.doPostRequest(URL_GENIE_AWS+"/switch/edit",jReqBody+"",sessionManager.getSecurityToken());
                Log.d("Edit_Operation","Edit url == "+URL_GENIE_AWS+"/switch/edit");
                Log.d("Edit_Operation","Edit response "+mResponse);
                return mResponse;
            }catch(Exception e){}

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null){
                pDialog.cancel();
            }

            if(result!=null){
                Log.d("Edit Operation","Edit Result "+result+"");
                try{
                    JSONObject jObj=new JSONObject(result);
                    if(jObj.getString("status").equals("SUCCESS")){
                        Toast.makeText(EditOpetaion.this,"Switch updated successfully", Toast.LENGTH_SHORT).show();
                        DatabaseHandler db=new DatabaseHandler(EditOpetaion.this);
                     //   db.updateSwitchImageId(mSwitchID,switchImageID);
                      //  db.updateRecntSwitchImageId(mSwitchID,switchImageID,switchName);
                        db.updateSwitchRoomName(mSwitchID,switchName);
                        db.updateSwitchImageId(mSwitchID,switchImageID);
                        db.updateRecntSwitchImageId(mSwitchID,switchImageID,switchName);
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend3"));

                        EditOpetaion.this.onBackPressed();

                    }else{
                        Toast.makeText(EditOpetaion.this,jObj.getString("msg"),Toast.LENGTH_SHORT).show();
                    }

                }catch(Exception e){}
            }else{
                Toast.makeText(EditOpetaion.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
