package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;
import app.AppController;

import static Database.DatabaseHandler.OPERATION;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.ROOM_TYPE;
import static Database.DatabaseHandler.ROOM_TYPE_ID;
import static Database.DatabaseHandler.SELECTED_SWITCH;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.MESSAGE_TRY_AGAIN;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.MainActivity.isInternetAvailable;
import static com.genieiot.gsmarthome.MainActivity.myToast;

/**
 * Created by root on 9/26/16.
 */

public class AddRooms extends Activity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private EditText edtRoomName;
    private Button btnSave;
    private Spinner spnRoomType;
    private ArrayList<String> categories;
    private DatabaseHandler db;
    private ArrayList<HashMap<String, String>> listRoomType;
    private String roomTypeId;
    private String txtType = "";

    private String operation = "";
    private TextView txtHeading;
    private String roomId = "";
    ImageView imgBack;
    private String strHeader = "",roomImageID;
    private String strRoomName = "";
    private ProgressDialog pDialog;
    private SessionManager sessionManager;
    private String strWebserviceName = "";
    private int methodType;

    String[] roomList=new String[]{"Bedroom","Hall","Kitchen","Bathroom","Balcony","Living Room","Terrace","Study Room","Kid Room"};
    private boolean selectionFlag=false;
    private String mRoomName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_edit_rooms);
        initialiseControls();

        Intent intent=getIntent();

        String roomname= intent.getStringExtra(ROOM_NAME);
        edtRoomName.setText(roomname);
        edtRoomName.setSelection(edtRoomName.getText().length());

        int roomImage=Integer.parseInt(intent.getStringExtra(ROOM_IMAGE_TYPE));

        List mRoomList = Arrays.asList(roomList);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddRooms.this, R.layout.textview_black_background, mRoomList);
        // attaching data adapter to spinner
        spnRoomType.setAdapter(dataAdapter);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //setSpinner();
        try {
            spnRoomType.setSelection(roomImage);
        }catch(Exception e){}
    }
    private void addOrEditRoomServer() {

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("userId", sessionManager.getUSERID());
        if (!roomId.equals("")) {
            jsonParams.put("roomId", roomId);
        }
        mRoomName=edtRoomName.getText().toString();
        jsonParams.put("roomName", edtRoomName.getText().toString());
        jsonParams.put("roomType", roomTypeId);
        jsonParams.put("roomImageId", roomImageID);
        jsonParams.put("messageFrom",URL_GENIE.equals(URL_GENIE_AWS) ? INTERNET : LOCAL_HUB);
        strWebserviceName = "/room/add";
        methodType = Request.Method.POST;

        Log.d("Room_edit_log","jsonparam is =="+jsonParams);

        callWebService(methodType, jsonParams, strWebserviceName);



    }
//used to call web service
    private void callWebService(int methodType, Map<String, String> jsonParams, final String strWebserviceName) {
        showProgressDialog();
        try {
            JsonObjectRequest myRequest = new JsonObjectRequest(
                    methodType, URL_GENIE_AWS + strWebserviceName, new JSONObject(jsonParams), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Room_edit_log","url is =="+URL_GENIE_AWS+strWebserviceName);
                    Log.d("Room_edit_log","jsonparam is =="+response);
                    hideProgressDialog();
                    if (response != null) {
                        try {
                            String status = response.getString("status");

                            if (strWebserviceName.equals("/roomtype/getall")) {
                                String results = response.getString("result");
                                JSONArray resultArray = new JSONArray(results);

                                if (status.equals("SUCCESS")) {
                                    createListForRoom(resultArray);
                                    setSpinnerData();
                                    setSpinner();
                                    //setPreselectedText();
                                }
                            } else if (strWebserviceName.equals("/room/add")) {
                                if (status.equals("SUCCESS")) {
                                    AddRooms.this.finish();
                                    if (operation.equals("Edit")) {
                                        //=================
                                        HashMap<String, String> room = new HashMap<String, String>();
                                        mRoomName=edtRoomName.getText().toString();
                                        room.put(ROOM_NAME, edtRoomName.getText().toString());
                                        room.put(ROOM_TYPE_ID, roomTypeId);
                                        //=================

                                      //  db.updateRoomImage(roomImageID, roomId);   //original

                                        //=========
                                        db.updateRoom(room, roomId);
                                        db.updateRoomImage(roomImageID, roomId);
                                        db.updateSwitchRoomNameByRoomId(roomId,mRoomName);
                                        db.updateSwitchRoomNameInRecent(roomId,mRoomName);
                                        //===========



                                        myToast(AddRooms.this, "Room name updated successfully");

                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));
                                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend3"));




                                        finish();

                                    } else {
                                        myToast(AddRooms.this, txtType + " Added Successfully");
                                    }

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            },                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideProgressDialog();
                            error.printStackTrace();
                            myToast(AddRooms.this, MESSAGE_TRY_AGAIN);

                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("X-AUTH-TOKEN", sessionManager.getSecurityToken());
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(myRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void createListForRoom(JSONArray resultArray) throws JSONException {

        listRoomType.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            HashMap<String, String> roomType = new HashMap<String, String>();
            JSONObject jsonObject = new JSONObject(resultArray.get(i).toString());

            roomType.put(ROOM_TYPE_ID, jsonObject.getString("id"));
            roomType.put(ROOM_TYPE, jsonObject.getString("roomTypeName"));
            listRoomType.add(roomType);
        }

    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void initialiseControls() {
        db = new DatabaseHandler(AddRooms.this);
        listRoomType = new ArrayList<HashMap<String, String>>();
        listRoomType.clear();
        sessionManager = new SessionManager(AddRooms.this);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        edtRoomName = (EditText) findViewById(R.id.edtRoom);
        btnSave = (Button) findViewById(R.id.btnSave);
        spnRoomType = (Spinner) findViewById(R.id.spnRoomType);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        setListeners();
    }
    private void getOperation() {
        Bundle bundle = getIntent().getExtras();
        operation = bundle.getString(OPERATION);
        strHeader = "Add Room";
        if (operation.equals("Edit")) {

            strHeader = "Edit Room";
            roomId = bundle.getString(ROOM_ID);
            if (sessionManager.getUSERID().equals("")) {

                getRoomInfo(roomId);

            } else {

                roomTypeId = bundle.getString(ROOM_TYPE_ID);
                strRoomName = bundle.getString(ROOM_NAME);

            }
        }
    }
    private void setListeners() {

        spnRoomType.setOnItemSelectedListener(this);
        btnSave.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getOperation();
        if (sessionManager.getUSERID().equals("")) {
            listRoomType = db.getRoomType();
             //setSpinnerData();
             //setSpinner();
            //setPreselectedText();
        } else {
            if (isInternetAvailable(AddRooms.this)) {
               //getRoomType();

            } else {
                myToast(AddRooms.this, MESSAGE_INTERNET_CONNECTION);
            }
        }
        setToolbar();
    }

    private void setToolbar() {

        txtHeading.setText(strHeader);
    }

    private void setSpinner() {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddRooms.this, R.layout.textview_black_background, categories);
        // attaching data adapter to spinner
        spnRoomType.setAdapter(dataAdapter);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }
    private void setSpinnerData() {
        // Spinner Drop down elements
        categories = new ArrayList<String>();
        categories.add("Select");
        for (int i = 0; i < listRoomType.size(); i++) {
            categories.add(listRoomType.get(i).get(ROOM_TYPE));
        }
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        roomImageID=position+"";
        if(selectionFlag) {
            edtRoomName.setText(spnRoomType.getSelectedItem().toString());
        }else{
            selectionFlag=true;
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                AddRooms.this.finish();
                break;

            case R.id.btnSave:
                saveRoom();
                break;
        }


    }

    private void saveRoom() {

        if (edtRoomName.getText().toString().length() == 0) {
            myToast(AddRooms.this, "Enter room name");
            return;
        }

        if (sessionManager.getUSERID().equals("")) {
            int isAvailable = 0;
            if (roomId.equals("")) {
                isAvailable = db.isRoomAvailable(edtRoomName.getText().toString());

            } else {
                isAvailable = db.isRoomAvailableWithId(edtRoomName.getText().toString(), roomId);

            }
            if (isAvailable == 1) {
                myToast(AddRooms.this, "Room already exists");
                return;
            }
        }


        HashMap<String, String> room = new HashMap<String, String>();
        mRoomName=edtRoomName.getText().toString();
        room.put(ROOM_NAME, edtRoomName.getText().toString());
        room.put(ROOM_TYPE_ID, roomTypeId);

        if (operation.equals("Edit")) {
            if (sessionManager.getUSERID().equals("")) {
                db.updateRoom(room, roomId);
                db.updateRoomImage(roomImageID, roomId);
                db.updateSwitchRoomNameByRoomId(roomId,mRoomName);
                db.updateSwitchRoomNameInRecent(roomId,mRoomName);
                myToast(AddRooms.this, " Updated Successfully");
                this.finish();
            } else {
                if (isInternetAvailable(AddRooms.this)) {
                    addOrEditRoomServer();

                } else {
                    myToast(AddRooms.this, MESSAGE_INTERNET_CONNECTION);
                }

            }


        } else {
            if (sessionManager.getUSERID().equals("")) {
                db.insertRoom(room);
                myToast(AddRooms.this, txtType + " Added Successfully");
                this.finish();
            } else {
                if (isInternetAvailable(AddRooms.this)) {

                    addOrEditRoomServer();

                } else {

                    btnSave.setEnabled(false);
                    myToast(AddRooms.this, MESSAGE_INTERNET_CONNECTION);
                }
            }
        }
    }
    public void getRoomInfo(String roomId) {

        HashMap<String, String> roomInfo = db.getRoom(roomId);
        roomTypeId = roomInfo.get(ROOM_TYPE_ID);
        strRoomName = roomInfo.get(ROOM_NAME);
    }

    private void setPreselectedText() {
        for (int i = 0; i < listRoomType.size(); i++) {

            if (listRoomType.get(i).get(ROOM_TYPE_ID).equals(roomTypeId)) {
                spnRoomType.setSelection(i + 1);
            }
        }
    }
}
