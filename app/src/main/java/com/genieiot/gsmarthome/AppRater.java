package com.genieiot.gsmarthome;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.zip.Inflater;

/**
 * Created by root on 22/3/17.
 */

public class AppRater {
    private final static String APP_TITLE = "Genie SmartHome";// App Name
    private final static String APP_PNAME = "com.genieiot.gsmarthome";// Package Name

    private final static int DAYS_UNTIL_PROMPT = 7;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 1;//Min number of launches


    public static void app_launched(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false))
        {
            return ;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {

            if (System.currentTimeMillis() >= date_firstLaunch +
                    (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000))
            {
                showRateDialog(mContext, editor);
            }
        }
        editor.commit();
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rateus_alert);

        TextView txtRate= (TextView) dialog.findViewById(R.id.txtRate);
        TextView  txtRemind= (TextView) dialog.findViewById(R.id.txtRemind);
        TextView txtNoThanks= (TextView) dialog.findViewById(R.id.txtNoThanks);

        txtRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + APP_PNAME)));
                dialog.dismiss();

                editor.putBoolean("dontshowagain", true);
                editor.commit();

            }
        });

        txtRemind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        txtNoThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                editor.putBoolean("dontshowagain", true);
                editor.commit();
            }
        });

        dialog.show();






//        dialog.setTitle("Rate " + APP_TITLE);
        //LinearLayout ll = new LinearLayout(mContext);
//        ll.setOrientation(LinearLayout.VERTICAL);
//
//        TextView tv = new TextView(mContext);
//        tv.setText("If you enjoy using " + APP_TITLE + ", please take a moment to rate it. Thanks for your support!");
//        tv.setWidth(360);
//        tv.setHeight(140);
//       // tv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//        tv.setPadding(4, 0, 4, 10);
//        ll.addView(tv);
//
//        TextView b1 = new TextView(mContext);
//        b1.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
//        b1.setText("Rate " + APP_TITLE);
//        b1.setWidth(360);
//        b1.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + APP_PNAME)));
//                dialog.dismiss();
//
//                editor.putBoolean("dontshowagain", true);
//                editor.commit();
//            }
//        });
//        ll.addView(b1);
//
//        TextView b2 = new TextView(mContext);
//        b2.setText("Remind me later");
//        b2.setWidth(360);
//        b2.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
//        b2.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//        ll.addView(b2);
//
//        TextView b3 = new TextView(mContext);
//        b3.setText("No, thanks");
//        b3.setWidth(360);
//        b3.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
//        b3.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                if (editor != null) {
////                    editor.putBoolean("dontshowagain", true);
////                    editor.commit();
////                }
//                dialog.dismiss();
//            }
//        });
//        ll.addView(b3);
//
//        dialog.setContentView(ll);
//        dialog.show();
    }
}
