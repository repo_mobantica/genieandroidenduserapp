package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.hockeyapp.android.CrashManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Session.Constants;
import app.AppController;

import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.R.id.btnSubmit;

/**
 * Created by root on 10/20/16.
 */

public class OTP extends Activity implements View.OnClickListener {

    private TextView resendOTP;
    private Button mBtnSubmit;
    private EditText edtOTP;
    private ProgressDialog pDialog;
    private String mUserId="",mEmailId="";
    private String mOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp);
        initControl();
    }

    private void initControl() {
        mBtnSubmit = (Button) findViewById(btnSubmit);
        resendOTP = (TextView) findViewById(R.id.resendOTP);
        edtOTP= (EditText) findViewById(R.id.edtOTP);

        edtOTP.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        resendOTP.setOnClickListener(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);


        if(getIntent().hasExtra("USERID")){
            mUserId=getIntent().getStringExtra("USERID");
            mEmailId=getIntent().getStringExtra("EmailId");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if(edtOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(this,getResources().getString(R.string.MSG_OTP), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Constants.isInternetAvailable(this)){
                    mOTP=edtOTP.getText().toString().trim();
                    new AsyncOTPVerification().execute();
                }else{
                    Toast.makeText(this,Constants.MESSAGE_INTERNET_CONNECTION,Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.resendOTP:
                if(Constants.isInternetAvailable(this)){
                   new ForgotPasswordAsync().execute();
                }else{
                    Toast.makeText(this,Constants.MESSAGE_INTERNET_CONNECTION,Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    class AsyncOTPVerification extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(OTP.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String res="";
            //email
            try {
                JSONObject jObj = new JSONObject();
                jObj.put("userId",mUserId);
                jObj.put("otp",mOTP);
                res=net.doPostRequest(URL_GENIE_AWS + "/user/verifyOTP", jObj.toString());
            }catch(Exception e){}
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result!=null) {
                if (pDialog != null) {
                    pDialog.cancel();
                }

                if(result!=null) {
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        if (status.equals("SUCCESS")) {
                            JSONObject jsResult = new JSONObject(response.getString("result"));
                            Intent intent = new Intent(OTP.this, SetNewPassword.class);
                            intent.putExtra("USERID", jsResult.getString("userId"));
                            intent.putExtra("x_AUTH_TOKEN", jsResult.getString("x_AUTH_TOKEN"));
                            // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(OTP.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(OTP.this, "Please try again.", Toast.LENGTH_SHORT).show();
                }


            }else{
                Toast.makeText(OTP.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }



    class ForgotPasswordAsync extends AsyncTask<Void,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(OTP.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String res="";
            try {
                JSONObject jObj = new JSONObject();
                jObj.put("email",mEmailId);
                res=net.doPostRequest(URL_GENIE_AWS + "/user/sendPasswordOTP", jObj.toString());

            }catch(Exception e){}
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null){
                pDialog.cancel();
            }
            if(result!=null){
                try {
                    JSONObject response=new JSONObject(result);
                    if (response != null) {
                            if (response.getString("status").equals("SUCCESS")) {
                                Toast.makeText(OTP.this,getResources().getString(R.string.MSG_OTP_RESEND), Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(OTP.this,getResources().getString(R.string.MSG_OTP_FAIL), Toast.LENGTH_SHORT).show();
                            }
                    }
                }catch(Exception e){e.printStackTrace();}

            }else{
                Toast.makeText(OTP.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }
}