package com.genieiot.gsmarthome;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Session.SessionManager;

/**
 * Created by root on 7/1/17.
 */

public class Router_IP_ChangeActivity extends AppCompatActivity implements View.OnClickListener
{

    private EditText edtOldIpAddress;
    private Button btnSubmit;
    private TextView txtHeading;
    private ImageView imgBack;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routeripchange);
        edtOldIpAddress= (EditText) findViewById(R.id.edtOldIpAddress);
        btnSubmit= (Button) findViewById(R.id.btnSubmit);
        txtHeading= (TextView) findViewById(R.id.txtHeading);
        imgBack= (ImageView) findViewById(R.id.imgBack);
        sessionManager=new SessionManager(this);

        txtHeading.setText("Change Router IP");
        edtOldIpAddress.setText(sessionManager.getRouterIP());
        btnSubmit.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                String mNewIP=edtOldIpAddress.getText().toString();
                if(mNewIP.equals("")) {
                    Toast.makeText(Router_IP_ChangeActivity.this,"Please enter router IP",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!validate(mNewIP)){
                    Toast.makeText(this, "Please enter IP in proper Format.", Toast.LENGTH_SHORT).show();
                    return;
                }

                sessionManager.setRouterIP(mNewIP);
                finish();
                Toast.makeText(this,"Update successfully.",Toast.LENGTH_SHORT).show();
                break;
            case R.id.imgBack:
                this.finish();
                break;

        }

    }

    private static final String PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static boolean validate(String ip){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

}
