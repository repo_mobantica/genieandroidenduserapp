package com.genieiot.gsmarthome;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.hockeyapp.android.CrashManager;

import java.text.DateFormat;
import java.util.HashMap;

import static Database.DatabaseHandler.REPEATED_STATUS;


/**
 * Created by root on 10/12/16.
 */

public class  Schedular_Activity extends AppCompatActivity implements TabLayout.OnTabSelectedListener ,View.OnClickListener{

    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

    private TextView mCurrentTime;
    DateFormat mTimeFormat;
    private int mPickerTheme;
    Button btn_scedule;
    final static int RQS_1 = 1;
    private TextView txtHeading;
    ImageView imgBack;
    public static String mSwitchName,mRoomName;
    HashMap<String,String> mapList,mapEditList;
    String  tab_no;
    String  RepeatedStatus;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedular_activity);
        initView();
        mapEditList = new HashMap<>();

        imgBack.setOnClickListener(this);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setTabTextColors(Color.parseColor("#ffffff"), Color.parseColor("#ffffff"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Date, Time"));
        tabLayout.addTab(tabLayout.newTab().setText("Timer"));


        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
        tabLayout.setSelectedTabIndicatorHeight(5);



        viewPager = (ViewPager) findViewById(R.id.pager);
        Intent intent = getIntent();

        mapList = (HashMap<String, String>) intent.getSerializableExtra("SwitchInfo");
        String strMessage = intent.getStringExtra("Operation");

        if (strMessage.equals("EDIT")) {


            mapEditList = (HashMap<String, String>) intent.getSerializableExtra("EditInfo");
            Log.d("edit_tab_info", "===" + mapEditList);
            mSwitchName = intent.getStringExtra("mSwitchName");
            mRoomName = intent.getStringExtra("mRoomName");
            RepeatedStatus = intent.getStringExtra("RepeatedStatus");
            Log.d("edit_tab_info12", "===" + RepeatedStatus);


            Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(), mapList, strMessage, mapEditList);
            viewPager.setCurrentItem(1);
            viewPager.setAdapter(adapter);


            if(RepeatedStatus == null)
            {
                adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(), mapList, strMessage, mapEditList);
                Log.d("edit_tab_info13", "mapList ===" + mapList);
                Log.d("edit_tab_info13", "mapEditList ===" + mapEditList);
                viewPager.setAdapter(adapter);
                tabLayout.setOnTabSelectedListener(this);


               String repeat =mapEditList.get(REPEATED_STATUS);
                if(repeat.equals("1"))
                {
                    viewPager.setCurrentItem(1);
                    tabLayout.getTabAt(1).select();
                    tabLayout.setSelected(true);
                    tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
                    tabLayout.setOnTabSelectedListener(this);

                }
                else
                {

                    viewPager.setCurrentItem(0);
                    tabLayout.getTabAt(0).select();
                    tabLayout.setSelected(true);
                    tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
                    tabLayout.setOnTabSelectedListener(this);
                }

            }
            else
            {
                if(RepeatedStatus.equals("1"))
                {
                    viewPager.setCurrentItem(1);
                    tabLayout.getTabAt(1).select();
                    tabLayout.setSelected(true);
                    tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
                    tabLayout.setOnTabSelectedListener(this);

                }
                else
                {

                    viewPager.setCurrentItem(0);
                    tabLayout.getTabAt(0).select();
                    tabLayout.setSelected(true);
                    tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#ffffff"));
                    tabLayout.setOnTabSelectedListener(this);
                }

            }









        }
        else
        {

            mapEditList = (HashMap<String, String>) intent.getSerializableExtra("EditInfo");
            Log.d("edit_tab_info14", "===" + mapEditList);
            mSwitchName = intent.getStringExtra("mSwitchName");
            mRoomName = intent.getStringExtra("mRoomName");

            Log.d("edit_tab_info14", "===" + mapEditList);

            Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount(), mapList, strMessage, mapEditList);
            viewPager.setAdapter(adapter);
            tabLayout.setOnTabSelectedListener(this);


        }

    }
    private void initView() {
        btn_scedule=(Button)findViewById(R.id.btn_scedule);
        txtHeading=(TextView)findViewById(R.id.txtHeading);
        imgBack=(ImageView)findViewById(R.id.imgBack);
    }
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int adv=tab.getPosition();
        viewPager.setCurrentItem(adv);
    }
    @Override    public void onTabUnselected(TabLayout.Tab tab) {

    }
    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public static String getValues(String input) {
        if(input.equals("Switch"))
        {
            return mSwitchName;
        }
        else if(input.equals("Room"))
        {
            return mRoomName;
        }
        else
        {
            return null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:

                Schedular_Activity.this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);

                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

        txtHeading.setText("Scheduler");

        // txtHeading.setText(mapList.get("SwitchName"));

        CrashManager.register(this);
    }
}