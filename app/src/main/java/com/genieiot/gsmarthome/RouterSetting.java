package com.genieiot.gsmarthome;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Adapter.CustomSpinnerAdapter;
import Session.SessionManager;

/**
 * Created by root on 16/12/16.
 */

public class RouterSetting extends Activity implements View.OnClickListener,AdapterView.OnItemSelectedListener{
    private WifiManager mainWifi;
    private WifiReceiver receiverWifi;
    List<ScanResult> wifiList;
    StringBuilder sb = new StringBuilder();
    private Spinner spSelectWifi;
    private Button btnSubmit;
    private String mSSIDName;
    private EditText edtNewPassword;
    private TextView txtHeading;
    private ImageView imgBack;
    private ProgressDialog pDialog;
   // MyReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routersetting);
         spSelectWifi= (Spinner) findViewById(R.id.spSelectWifi);
         btnSubmit= (Button) findViewById(R.id.btnSubmit);
         edtNewPassword= (EditText) findViewById(R.id.edtNewPassword);
         txtHeading= (TextView) findViewById(R.id.txtHeading);
         imgBack= (ImageView) findViewById(R.id.imgBack);
         btnSubmit.setOnClickListener(this);
         txtHeading.setOnClickListener(this);
         imgBack.setOnClickListener(this);
         txtHeading.setText("Select WiFi");
         spSelectWifi.setOnItemSelectedListener(this);
         receiverWifi = new WifiReceiver();

//        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);
//        if (!(wifi.isWifiEnabled())){
//
//           //wifi is disable
//
//            final IntentFilter filters = new IntentFilter();
//            filters.addAction("android.net.wifi.WIFI_STATE_CHANGED");
//            filters.addAction("android.net.wifi.STATE_CHANGE");
//            super.registerReceiver(receiver, filters);
//        }
    }


    private void startWifiScanning() {

        if (!permissionsGranted()) {
            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION}, 123);
        }
        // Initiate wifi service manager
        mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (mainWifi.isWifiEnabled() == false)
        {
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled",Toast.LENGTH_LONG).show();
            mainWifi.setWifiEnabled(true);
        }
        receiverWifi = new WifiReceiver();
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        mainWifi.startScan();
    }
    private Boolean permissionsGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        statusCheck();
    }
    @Override
    protected void onPause() {
        super.onPause();
        if(receiverWifi!=null) {
            unregisterReceiver(receiverWifi);
            receiverWifi=null;
        }
    }
    public void statusCheck() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            }else{ startWifiScanning();}
        }else{
            startWifiScanning();
        }
    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnSubmit:
                onClickOfSubmit();
                break;
            case R.id.imgBack:
                finish();
                break;
        }
    }
    private void onClickOfSubmit() {
        if(edtNewPassword.getText().toString().isEmpty()){
            Toast.makeText(this, "Please enter password.", Toast.LENGTH_SHORT).show();
            return;
        }else if(edtNewPassword.getText().toString().length()<8){
            Toast.makeText(this, "Please enter 8 digit password.", Toast.LENGTH_SHORT).show();
            return;
        }
        SessionManager session=new SessionManager(this);
        session.saveSSID(mSSIDName);
        session.saveSPassword(edtNewPassword.getText().toString());
        Intent intent=new Intent(this,SetRouterPassword.class);
        startActivity(intent);
        finish();
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if(spinner.getId() == R.id.spSelectWifi)
        {
            HashMap<String,String> selectedItem= (HashMap<String, String>) parent.getItemAtPosition(position);
            mSSIDName=selectedItem.get("NAME");
           // Toast.makeText(this,""+mSSIDName, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    class WifiReceiver extends BroadcastReceiver {
        // This method call when number of wifi connections changed
        public void onReceive(Context c, Intent intent) {

            sb = new StringBuilder();
            wifiList = mainWifi.getScanResults();
            sb.append("\n        Number Of Wifi connections :"+wifiList.size()+"\n\n");
            ArrayList<HashMap<String,String>> ArraList=new ArrayList<>();
            for(int i = 0; i < wifiList.size(); i++){
                HashMap<String,String> map=new HashMap<>();
                sb.append(new Integer(i+1).toString() + ". ");
                sb.append((wifiList.get(i)).toString());
                sb.append("\n\n");
                map.put("NAME",(wifiList.get(i)).SSID);
                map.put("ID",i+"");
                ArraList.add(map);

            }
            //new code to get IP address of devices
            WifiInfo wifiinfo = mainWifi.getConnectionInfo();
            byte[] myIPAddress = BigInteger.valueOf(wifiinfo.getIpAddress()).toByteArray();
            InetAddress myInetIP = null;
            try
            {
                myInetIP = InetAddress.getByAddress(myIPAddress);
            }
            catch (UnknownHostException e)
            {
                e.printStackTrace();
            }

            //String myIP = myInetIP.getHostAddress();
           // Log.select_menu("ID Address Of Devices:", myIP);

            CustomSpinnerAdapter customSpinnerAdapter=new CustomSpinnerAdapter(RouterSetting.this,ArraList);
            spSelectWifi.setAdapter(customSpinnerAdapter);

        }

    }
}
