package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import Session.Constants;
import Session.SessionManager;

/**
 * Created by Genie IoT on 9/19/2016.
 */
public class Settings extends Activity implements View.OnClickListener {

    private EditText edtIp;
    private Button btnSave;
    private TextView txtAccount,tvRouterIP;
    ImageView imgBack;
    TextView txtHeading;
    private TextView txtChangePassword,txtRouterPassword;
    private TextView tvPanel,tvTurnOffAll;
    LinearLayout setRouterPasswordblock, setRouterIpblock;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        initialiseControls();
    }

    private void initialiseControls() {

        txtAccount = (TextView) findViewById(R.id.txtAccount);
        txtChangePassword = (TextView) findViewById(R.id.txtChangePassword);
        txtRouterPassword = (TextView) findViewById(R.id.txtRouterPassword);
        tvRouterIP = (TextView) findViewById(R.id.tvRouterIP);
        tvPanel = (TextView) findViewById(R.id.tvPanel);
        tvTurnOffAll = (TextView) findViewById(R.id.tvTurnOffAll);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        imgBack = (ImageView) findViewById(R.id.imgBack);

        setRouterPasswordblock=(LinearLayout)findViewById(R.id.setRouterPasswordblock);
        setRouterPasswordblock.setVisibility(View.GONE);
        setRouterIpblock=(LinearLayout)findViewById(R.id.setRouterIpblock);
        setRouterIpblock.setVisibility(View.GONE);

        imgBack.setOnClickListener(this);
        tvRouterIP.setOnClickListener(this);
        tvPanel.setOnClickListener(this);
        txtAccount.setOnClickListener(this);
        txtChangePassword.setOnClickListener(this);
        txtRouterPassword.setOnClickListener(this);
        tvTurnOffAll.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        txtHeading.setText("Settings");
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.txtAccount:
                intent = new Intent(this, UserProfileNew.class);
                startActivity(intent);
                break;

            case R.id.txtChangePassword:
                intent = new Intent(this, ChangePassword.class);
                startActivity(intent);
                break;

            case R.id.imgBack:
                Intent intent1 = new Intent(Settings.this, MainActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                break;
            case R.id.txtRouterPassword:
                SessionManager sessionManager=new SessionManager(this);
                if(sessionManager.getDemoUser().equals("DemoUser")){
                    Toast.makeText(this, getResources().getString(R.string.MSG_USER_AUTH), Toast.LENGTH_SHORT).show();
                }else {
                    intent = new Intent(this, RouterSetting.class);
                    startActivity(intent);
                }
                break;
            case R.id.tvRouterIP:
                createAlertPassdDialog();
                break;
            case R.id.tvPanel:
                createAlertForChangePanel();
             break;
            case R.id.tvTurnOffAll:
                callSelectSwitchActivity();
                break;

        }


//        overridePendingTransition(R.anim.enter, R.anim.exit);


    }

    private void callSelectSwitchActivity() {
        Intent intent=new Intent(this,SwitchSelectionActivity.class);
        intent.putExtra("SETTING","Setting");
        startActivity(intent);
    }

    private void createAlertPassdDialog() {

        final Dialog dialog = new Dialog(Settings.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_ip_change);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        final EditText edtPassword = (EditText) dialog.findViewById(R.id.edtPassword);

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtPass=edtPassword.getText().toString();
                if(edtPass.equals("")){
                    Toast.makeText(Settings.this, getResources().getString(R.string.MSG_VAILD_PASS), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(edtPass.equals(Constants.CHANGEIP_PASS)){
                    dialog.dismiss();
                    Intent intentIP=new Intent(Settings.this,Router_IP_ChangeActivity.class);
                    startActivity(intentIP);
                }else{
                    Toast.makeText(Settings.this,getResources().getString(R.string.MSG_CORRECT_PASS), Toast.LENGTH_SHORT).show();
                }

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void createAlertForChangePanel() {

        final Dialog dialog = new Dialog(Settings.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_ip_change);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        final EditText edtPassword = (EditText) dialog.findViewById(R.id.edtPassword);

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtPass=edtPassword.getText().toString();
                if(edtPass.equals("")){
                    Toast.makeText(Settings.this,getResources().getString(R.string.MSG_VAILD_PASS), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(edtPass.equals(Constants.CHANGEIP_PASS)){
                    dialog.dismiss();
                    Intent iTopic=new Intent(Settings.this,ChangePanelTopic.class);
                    startActivity(iTopic);

                }else{
                    Toast.makeText(Settings.this,getResources().getString(R.string.MSG_CORRECT_PASS), Toast.LENGTH_SHORT).show();
                }

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
