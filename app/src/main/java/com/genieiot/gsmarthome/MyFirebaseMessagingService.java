package com.genieiot.gsmarthome;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import Database.DatabaseHandler;
import Session.SessionManager;

/**
 * Created by root on 11/5/16.
 */

public class MyFirebaseMessagingService  extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
       // Log.select_menu(TAG, "From: " + remoteMessage.getFrom());
        //Log.select_menu(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        //Calling method to generate notification
        if(remoteMessage!=null) {
            if(remoteMessage.getNotification()!=null) {
                SessionManager session = new SessionManager(this);
                if (!session.getUSERID().equals("")) {
                    DatabaseHandler database = new DatabaseHandler(this);
                    //database.insertNotificationMessage(remoteMessage.getNotification().getBody());
                    //sendNotification(remoteMessage.getNotification().getBody());
                    try {
                        //Calendar c = Calendar.getInstance();
                        //SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-DD HH:MM:SS");
                        //String formattedDate = df.format(c.getTime());

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                        Date date = new Date();
                        String formattedDate=dateFormat.format(date);


                        JSONObject jMain = new JSONObject(remoteMessage.getData().get("message"));
                        JSONObject jBody = new JSONObject(jMain.getString("body"));

                        //database.insertNotificationMessage(jBody.getString("message"),formattedDate);
                       // database.updateSwitchStatus(jBody.getString("switchId"), jBody.getString("switchStatus"), jBody.getString("dimmerValue"));
                    }catch(Exception e){
                        e.printStackTrace();
                    }

                    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend"));
                }
            }
        }
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Firebase Push Notification")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
