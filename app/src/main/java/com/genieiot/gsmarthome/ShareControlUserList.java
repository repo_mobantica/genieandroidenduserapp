package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Adapter.ShareControlAdapter;
import Session.Constants;
import Session.IOnClickListner;
import Session.SessionManager;

import static Session.Constants.URL_GENIE_AWS;
import static Session.Constants.isInternetAvailable;

/**
 * Created by root on 14/11/16.
 */

public class ShareControlUserList extends Activity implements IOnClickListner,View.OnClickListener
{
    RecyclerView recyclerView;
    ShareControlAdapter adapter;
    ArrayList<HashMap<String,String>> arrayList;
    private ProgressDialog pDialog;
    String id,email,phoneNumber,userType,image;
    private ImageView imgBack,imgAddUser;
    private TextView txtHeading;
    SessionManager sessionManager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sharecontrolscreen);
        initComponent();

        sessionManager=new SessionManager(this);
        if(sessionManager.getUSERID().equals("")){
            prepareUserList();
            setAdpater();
        }else {
            if(Constants.isInternetAvailable(ShareControlUserList.this)) {
                callWebServiceUserList();
            }else{
                Toast.makeText(this, "Please check internet connection...", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void prepareUserList() {

        arrayList=new ArrayList<>();

        HashMap<String,String> map1=new HashMap<>();
        map1.put("ID","1");
        map1.put("userName","Ganesh");
        map1.put("email","ganesh@gsmarthome.com");
        map1.put("image","");
        map1.put("userType","Admin");

        arrayList.add(map1);

        HashMap<String,String> map2=new HashMap<>();
        map2.put("ID","2");
        map2.put("userName","Harish");
        map2.put("email","harish@gsmarthome.com");
        map2.put("image","");
        map2.put("userType","Normal User");

        arrayList.add(map2);
    }

    private void initComponent() {
        arrayList=new ArrayList<>();
        recyclerView= (RecyclerView) findViewById(R.id.recycler);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgAddUser = (ImageView) findViewById(R.id.imgAddUser);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        txtHeading.setText("UserList");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        imgBack.setOnClickListener(this);
        imgAddUser.setOnClickListener(this);
    }
    private void callWebServiceUserList(){
        if(isInternetAvailable(ShareControlUserList.this))
        {
            new AsyncGetSharedUserList().execute();
        }else
        {
            Toast.makeText(this, "Please check internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClickListner(HashMap<String, String> map) {
        //Toast.makeText(this,""+map, Toast.LENGTH_SHORT).show();

        Intent intent=new Intent(this,EditShareControl.class);
        intent.putExtra("UserInfo",map);
        startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBack:
                this.finish();
                break;
            case R.id.imgAddUser:
                    SessionManager session=new SessionManager(ShareControlUserList.this);
                    if(session.getUSERID().equals("")) {
                        Intent intent = new Intent(ShareControlUserList.this, ShareControl.class);
                        startActivity(intent);
                        return;
                    }

                    if(!session.getUserType().equals("0")) {
                        Toast.makeText(this,getResources().getString(R.string.MSG_USER_AUTH), Toast.LENGTH_SHORT).show();
                        return;
                     }else{
                        Intent intent = new Intent(ShareControlUserList.this, ShareControl.class);
                        startActivity(intent);
                    }

                break;

        }

    }


    private class AsyncGetSharedUserList extends AsyncTask<Void,Void,String>{
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShareControlUserList.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        protected String doInBackground(Void... params) {
            Constants con=new Constants();
            String mResponse=null;
            try
            {
                SessionManager session=new SessionManager(ShareControlUserList.this);
                String mRequest=createJSONBody();
                mResponse=con.doPostRequest(URL_GENIE_AWS + "/user/list",mRequest,session.getSecurityToken());
            }
            catch(Exception e){}
            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if(pDialog!=null) {
                pDialog.cancel();
            }
            if(result!=null) {
                Log.d("ShareControl List--> ",result);
                parseResult(result);
            }
            else{
                Toast.makeText(ShareControlUserList.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void parseResult(String result) {
        try
        {
            JSONObject response=new JSONObject(result);
            if(response.getString("status").equals("SUCCESS"))
            {
                String mResult=response.getString("result");
                JSONArray jsnarray=new JSONArray(mResult);

                for(int i=0;i<jsnarray.length();i++)
                {
                    HashMap<String,String> mMap=new HashMap<>();
                    JSONObject jsnobject=jsnarray.getJSONObject(i);

                    email=jsnobject.getString("email");
                    phoneNumber=jsnobject.getString("phoneNumber");
                    String mUserType=jsnobject.getString("userType");
                    if(jsnobject.isNull("image")){
                        mMap.put("image","");
                    }else {
                        mMap.put("image",jsnobject.getString("image"));
                    }

                    mMap.put("id",jsnobject.getString("id"));
                    mMap.put("userName",jsnobject.getString("firstName") +" "+jsnobject.getString("lastName"));

                    if(mUserType.equals("0")){
                        mUserType="Admin";
                    }else if(mUserType.equals("1")){
                        mUserType="Moderate User";
                    }
                    else if(mUserType.equals("2")){
                       mUserType="Normal User";
                    }

                    {
                        mMap.put("userType", mUserType);
                        mMap.put("email", jsnobject.getString("email"));
                        arrayList.add(mMap);
                    }
                }
                setAdpater();
            }
            else
            {
                Toast.makeText(ShareControlUserList.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }

        catch (Exception e){}
    }

    private String createJSONBody() {
        JSONObject jMain=new JSONObject();
        SessionManager manager=new SessionManager(this);
        String userID= manager.getUSERID();
        try{
            jMain.put("userId",userID);

        }catch(Exception e){}
        return jMain+"";
    }
    public  void setAdpater(){

        SessionManager manager=new SessionManager(this);
        adapter = new ShareControlAdapter(this,arrayList,manager.getUserType());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.menu_add:
                //Toast.makeText(this, "add", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
