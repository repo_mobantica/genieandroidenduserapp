package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.hockeyapp.android.CrashManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Session.Constants;
import app.AppController;

import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.R.anim.enter;
import static com.genieiot.gsmarthome.R.id.btnSubmit;

/**
 * Created by root on 26/11/16.
 */

public class ResendOTP extends Activity implements View.OnClickListener

{
    TextView resendOTP;
    Button mBtnSubmit;
    EditText edtOTP;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req";
    HashMap<String,String> maplist;
    String mOTP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp);
        initControl();
         maplist= (HashMap<String, String>) getIntent().getSerializableExtra("data");
    }

    private void initControl() {
        mBtnSubmit = (Button) findViewById(btnSubmit);
        resendOTP = (TextView) findViewById(R.id.resendOTP);
        edtOTP= (EditText) findViewById(R.id.edtOTP);
        edtOTP.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        resendOTP.setOnClickListener(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);

    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if(edtOTP.getText().toString().trim().isEmpty()){
                    Toast.makeText(this,getResources().getString(R.string.MSG_OTP), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Constants.isInternetAvailable(this)){
                    try {

                        mOTP=edtOTP.getText().toString();
                        new AsyncOTPVerificationTask().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(this, Constants.MESSAGE_INTERNET_CONNECTION,Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.resendOTP:
                if(Constants.isInternetAvailable(this)){
                    new ResendOTPAsync().execute();
                }else{
                    Toast.makeText(this, Constants.MESSAGE_INTERNET_CONNECTION,Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    class AsyncOTPVerificationTask extends AsyncTask<Void,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ResendOTP.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
        }
        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String res="";

            try {
                JSONObject jObj = new JSONObject();
                jObj.put("email",maplist.get("email"));
              //  jObj.put("otp",mOTP);
             //   res=net.doPostRequest(URL_GENIE_AWS + "/user/verifyEmailOTP",jObj.toString());
                res=net.doPostRequest(URL_GENIE_AWS + "/user/sendPasswordOTP",jObj.toString());

            }catch(Exception e){}
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.cancel();
            }

            if (result != null) {
                try {
                    JSONObject response=new JSONObject(result);

                    if (response.getString("status").equals("SUCCESS"))
                    {
                        Intent intent=new Intent(ResendOTP.this,Login.class);
                        intent.putExtra("EMAIL",maplist.get("email"));
                        ResendOTP.this.finish();
                        startActivity(intent);
                        overridePendingTransition(enter, R.anim.exit);
                        Toast.makeText(ResendOTP.this,getResources().getString(R.string.MSG_REGISTRATION) , Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(ResendOTP.this,response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }
    }

    class ResendOTPAsync extends AsyncTask<Void,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ResendOTP.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String res="";
            //email
            try {
             //   jMain.put("birthDate",dobValue);
                JSONObject jObj = new JSONObject();
                jObj.put("firstName",maplist.get("firstName"));
                jObj.put("lastName",maplist.get("lastName"));
                jObj.put("email",maplist.get("email"));
                jObj.put("phoneNumber",maplist.get("phoneNumber"));
                jObj.put("password",maplist.get("password"));
                jObj.put("userType",maplist.get("userType"));
                jObj.put("birthDate",maplist.get("birthDate"));

                res=net.doPostRequest(URL_GENIE_AWS + "/user",jObj.toString());
            }catch(Exception e){}
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null){
                pDialog.cancel();
            }
            if(result!=null){
                try {
                    JSONObject response=new JSONObject(result);
                    if (response != null) {
                        try {

                            if (response.getString("status").equals("SUCCESS"))
                            {
                                Toast.makeText(ResendOTP.this,getResources().getString(R.string.MSG_OTP_RESEND) , Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(ResendOTP.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }catch(Exception e){}

            }else{
                Toast.makeText(ResendOTP.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }
}
