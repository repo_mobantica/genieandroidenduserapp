package com.genieiot.gsmarthome;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.HashMap;

import Fragments.Tab1;
import Fragments.Tab2;



/**
 * Created by root on 10/12/16.
 */

public class Pager extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;
    HashMap<String,String> mapSwitchList,mapListEdit;
    String mOperation;
    int tab_no;
    public int position;
    int repeatedStatus;


    //Constructor to the class
    public Pager(FragmentManager fm, int tabCount, HashMap<String, String> mapList,String mOperation,HashMap<String,String> mapListEdit) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        mapSwitchList=mapList;
        this.mapListEdit=mapListEdit;
        this.mOperation=mOperation;

        boolean a = mapList.isEmpty();
        Log.d("display_list","mapListEdit =="+a);

  /*      if(a)
         {

         }
         else
         {

      *//*       repeatedStatus= Integer.parseInt(mapListEdit.get("repeatStatus"));

             Log.d("display_list","mapList =="+repeatedStatus);
             Log.d("display_list","mapListEdit11 =="+mapListEdit);
         repeatedStatus=Integer.parseInt(mapListEdit.get("repeatStatus"));
         position=repeatedStatus;*//*


         }*/


    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {


     //   position=repeatedStatus;
     //   Log.d("repeated_status12","==="+repeatedStatus);

        switch (position) {
            case 0:
                Bundle bundle=new Bundle();
                bundle.putSerializable("SwitchInfo",mapSwitchList);
                bundle.putString("Operation",mOperation);
                bundle.putSerializable("EditInfo",mapListEdit);
                Tab1 tab1 = new Tab1();

                tab1.setArguments(bundle);
                return tab1;

            case 1:
                Bundle bundle1=new Bundle();
                bundle1.putSerializable("SwitchInfo",mapSwitchList);
                bundle1.putString("Operation",mOperation);
                bundle1.putSerializable("EditInfo",mapListEdit);
                Tab2 tab2 = new Tab2();
                tab2.setArguments(bundle1);
                return tab2;

            default:
                return null;
        }
    }


    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {


        return tabCount;
    }
}