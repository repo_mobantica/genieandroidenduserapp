package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

import Session.Constants;
import Session.SessionManager;

import static com.genieiot.gsmarthome.MainActivity.isInternetAvailable;

/**
 * Created by Genie IoT on 9/14/2016.
 */
public class SplashScreen extends Activity {
    MainActivity mainActivity=new MainActivity();
    private static ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final SessionManager session=new SessionManager(this);


        Thread timerThread =
                new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    if (session.getUSERID().equals("")) {

                         if(session.getDemoUser().equals("DemoUser")){
                             Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                             startActivity(intent);
                         }else {
                             Intent intent = new Intent(SplashScreen.this, FirstPage.class);
                             startActivity(intent);
                         }

                    } else {

                        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(intent);

                      /*  if(session.getUserSync()) {
                            Log.d("splash_val","value="+session.getUserSync());
                                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                                startActivity(intent);
                        }else{
                            Log.d("splash_val","value1="+session.getUserSync());
                            Intent intent = new Intent(SplashScreen.this, BlankScreen.class);
                            startActivity(intent);
                        }*/

                        }
                    }

                }

        };
        timerThread.start();

    }

    public class AsyncHubIP extends AsyncTask<Void,Void,Void>{
        ProgressDialog processsDailog=new ProgressDialog(SplashScreen.this);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            processsDailog.setMessage("Searching Genie Hub...");
            processsDailog.setCancelable(false);
            processsDailog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            BufferedReader br = null;
            boolean isFirstLine = true;

            try {
                br = new BufferedReader(new FileReader("/proc/net/arp"));
                String line;

                while ((line = br.readLine()) != null) {
                    if (isFirstLine) {
                        isFirstLine = false;
                        continue;
                    }

                    String[] splitted = line.split(" +");

                    if (splitted != null && splitted.length >= 4) {

                        String ipAddress = splitted[0];
                        String macAddress = splitted[3];

                        boolean isReachable = InetAddress.getByName(
                                splitted[0]).isReachable(500);  // this is network call so we cant do that on UI thread, so i take background thread.
                        if (isReachable) {
                            Log.d("Device Information", ipAddress + " : "
                                    + macAddress);
                           // HashMap<String,String> mMap=new HashMap<>();
                           // mMap.put("IP",ipAddress);
                           // mMap.put("MAC",macAddress);
                            Constants constant=new Constants();
                            String response=constant.doGetRequest("http://www.macvendorlookup.com/api/v2/"+macAddress);
                            // String response=constant.doPostRequest("http://www.macvendorlookup.com/api/v2/"+mac,"");
                            // Log.select_menu("Response : ",""+response);
                            if(response!=null && !response.isEmpty()){

                                JSONArray jArr=new JSONArray(response);
                                String strCompanyName=jArr.getJSONObject(0).getString("company");

                                SessionManager session=new SessionManager(SplashScreen.this);
                                if(strCompanyName!=null &&!strCompanyName.isEmpty()){
                                    if(strCompanyName.equals("Raspberry Pi Foundation")){
                                        session.setAppUrl(ipAddress);
                                        session.setAppUrlFlag(true);
                                        Log.d("Hub Info : ",ipAddress);
                                        break;
                                    }
                                }


                            }

                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(processsDailog!=null && processsDailog.isShowing()){
                processsDailog.cancel();
            }

            SessionManager sessionManager=new SessionManager(SplashScreen.this);
            if(sessionManager.getAPPURL().equals("")){
                Toast.makeText(SplashScreen.this,"Hub is not found.", Toast.LENGTH_SHORT).show();
            }


            if(sessionManager.getAppUrlFlag())
            {

                Constants.URL_GENIE = "http://"+sessionManager.getAPPURL()+":8080/GSmart_final_9jan/";
                if (sessionManager.getUSERID().equals("")) {
                    Intent intent = new Intent(SplashScreen.this, FirstPage.class);
                    SplashScreen.this.startActivity(intent);
                } else {

                    if(isInternetAvailable(SplashScreen.this))
                    {
                        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                        SplashScreen.this.startActivity(intent);
                    }

                }
            }

        }
    }
    static class NetworkSniffTask extends AsyncTask<Void, Void, Void> {
        private static final String TAG = "nstask";
        Context mContext;

        ArrayList<HashMap<String,String>> DeviceInfo=new ArrayList<>();
        private WeakReference<Context> mContextRef;


        public NetworkSniffTask(Context context) {
            mContextRef = new WeakReference<Context>(context);
            mContext=context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress=new ProgressDialog(mContext);
            progress.setMessage("Please wait...");
            progress.setCancelable(false);
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "Let's sniff the network");

            try {
                Context context = mContextRef.get();

                if (context != null) {

                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

                    WifiInfo connectionInfo = wm.getConnectionInfo();
                    int ipAddress = connectionInfo.getIpAddress();
                    String ipString = Formatter.formatIpAddress(ipAddress);


                    Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
                    Log.d(TAG, "ipString: " + String.valueOf(ipString));

                    String prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1);
                    Log.d(TAG, "prefix: " + prefix);


                    SessionManager session=new SessionManager(mContext);
                    for (int i = 0; i < 255; i++) {

                        String testIp = prefix + String.valueOf(i);
                        InetAddress address = InetAddress.getByName(testIp);
                        boolean reachable = address.isReachable(500);
                        String hostName = address.getCanonicalHostName();

                        if (reachable) {
                            HashMap<String,String> map=new HashMap<>();
                            String companyName=getMacFromArpCache(hostName);
                            map.put("IP",hostName);
                            map.put("CompanyName",companyName);


                            DeviceInfo.add(map);
                            Log.i("Device Info ","Company Name "+companyName+" Host Ip : "+hostName);
                            Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");

                            if(companyName!=null &&!companyName.isEmpty()){
                                if(companyName.equals("Raspberry Pi Foundation")){
                                    session.setAppUrl(hostName);
                                    session.setAppUrlFlag(true);
                                    break;
                                }
                            }

                        }
                    }

                }
            } catch (Throwable t) {
                Log.e(TAG, "Well that's not good.", t);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            SessionManager sessionManager=new SessionManager(mContext);
            if(sessionManager.getAPPURL().equals("")){
                Toast.makeText(mContext,"Hub is not found.", Toast.LENGTH_SHORT).show();
            }

            if(progress!=null){
                progress.cancel();
            }

            if(sessionManager.getAppUrlFlag())
            {

                Constants.URL_GENIE = "http://"+sessionManager.getAPPURL()+":8080/GSmart_final_9jan/";
                if (sessionManager.getUSERID().equals("")) {
                    Intent intent = new Intent(mContext, FirstPage.class);
                    mContext.startActivity(intent);
                } else {

                    if(isInternetAvailable(mContext))
                    {
                        Intent intent = new Intent(mContext, MainActivity.class);
                        mContext.startActivity(intent);
                    }

                }
            }
        }
    }
    public static String getMacFromArpCache(String ip) {
        if (ip == null)
            return null;
        String strCompanyName="";
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("/proc/net/arp"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] splitted = line.split(" +");
                if (splitted != null && splitted.length >= 4 && ip.equals(splitted[0])) {
                    // Basic sanity check
                    String mac = splitted[3];
                    if (mac.matches("..:..:..:..:..:..")) {

                        Constants constant=new Constants();
                        String response=constant.doGetRequest("http://www.macvendorlookup.com/api/v2/"+mac);
                       // String response=constant.doPostRequest("http://www.macvendorlookup.com/api/v2/"+mac,"");
                       // Log.select_menu("Response : ",""+response);
                        if(response!=null && !response.isEmpty()){
                            JSONArray jArr=new JSONArray(response);
                            strCompanyName=jArr.getJSONObject(0).getString("company");
                        }
                        return strCompanyName;
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

}
