package com.genieiot.gsmarthome;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import Adapter.SelectSwitchesAdapter;
import Database.DatabaseHandler;
import Session.Constants;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.SELECTED_SWITCH;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_NUMBER;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.Constants.isInternetAvailable;

/**
 * Created by root on 18/1/17.
 */

public class SwitchSelectionActivity extends AppCompatActivity implements View.OnClickListener
{
    RecyclerView recyclerView;
    SelectSwitchesAdapter adapter;
    ArrayList<HashMap<String,String>> arrayList;
    private ProgressDialog pDialog;
    private ImageView imgBack ;
    private TextView txtHeading;
    SessionManager sessionManager;
    DatabaseHandler database;
    Button btnConfirmSelect;
    ArrayList<HashMap<String,String>> arrListProfile=new ArrayList<>();
    int[] mSwitchON = new int[]{R.drawable.on_light_bulb,R.drawable.on_ac ,R.drawable.on_chandelier,R.drawable.on_cooler,R.drawable.on_desk_lamp,
            R.drawable.on_desktop,R.drawable.on_dish,R.drawable.on_exost,R.drawable.on_fan,R.drawable.on_refrigerator,
            R.drawable.on_microwave,R.drawable.on_mixer,R.drawable.on_purifier,R.drawable.on_socket,R.drawable.on_sound,
            R.drawable.on_stove,R.drawable.on_table_fan,R.drawable.on_television,R.drawable.on_tube,R.drawable.on_washing_machine,
            R.drawable.on_water_heater};

    private ArrayList<Integer> mIconArrayList;
    private String messageType;
    private ArrayList<String> mSwitchIDOFF;
    private CheckBox chkSelectAll;
    private static final String TAG = "SwitchSelectionActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allswitches);
        initializeControl();
        mIconArrayList=new ArrayList<>();
        mIconArrayList.clear();



        if(URL_GENIE.equals(URL_GENIE_AWS)){
          messageType=INTERNET;
        }else{
          messageType=LOCAL_HUB;
        }

        for (int index = 0; index < mSwitchON.length; index++)
        {
            mIconArrayList.add(mSwitchON[index]);
        }

        sessionManager=new SessionManager(this);
        database=new DatabaseHandler(this);

        setAdpater();
    }

    private void setAdpater() {

        arrayList= database.getAllSwitches();
        Log.d("array_list","==="+arrayList);

        String selctedId=getIntent().getStringExtra("ID");
        Log.d("selcetd_id", "Menu_onCheckedChanged: "+selctedId);

        if(getIntent().hasExtra("EDIT")) {

            String mEditList=getIntent().getStringExtra("EDIT");
            if(mEditList!=null ) {
                String splitList[] = mEditList.split(":");

                for(int i1=0;i1<=(splitList.length-1);i1++)
                {
                    Log.d("tag_1", "Menu_onCheckedChanged: "+splitList[i1]);
                }


                Log.d(TAG, "Menu_onCheckedChanged: "+arrayList);
                for (int i = 0; i < arrayList.size(); i++) {

                    HashMap<String, String> tempMap = arrayList.get(i);

                    for (int j = 0; j < splitList.length; j++) {
                        Log.d("array_list_split","==="+splitList[j]);
                        if (arrayList.get(i).get(SELECTED_SWITCH).equals("1") )
                        {

                            tempMap.put(SELECTED_SWITCH, "1");
                        //    break;
                        }
                        else
                        {
                            tempMap.put(SELECTED_SWITCH, "0");
                        }
                    }
                    arrayList.set(i, tempMap);
                }
            }
            Log.d("Swirch ", "setAdpater: "+mEditList);

        }else if(getIntent().hasExtra("ACTION")){
            for(int i=0;i<arrayList.size();i++){
             HashMap<String,String> tempMap=arrayList.get(i);
                tempMap.put(SELECTED_SWITCH,"0");
                arrayList.set(i,tempMap);
            }
        }
        arrListProfile=database.getAllModeInfo();
        Log.d("Swirch_id==", "setAdpater: "+arrListProfile);
        String checkSelected="false";
        adapter = new SelectSwitchesAdapter(this,arrayList,mIconArrayList,arrListProfile,selctedId,checkSelected);
        Log.d("arratlist_data","==="+arrayList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void callWebServiceSwitchList(ArrayList<String> mSwitchId, ArrayList<HashMap<String, String>> mMapList){
        if(isInternetAvailable(SwitchSelectionActivity.this))
        {
            new AsyncGetSwitchList(mSwitchId,mMapList).execute();
        }else
        {
            Toast.makeText(this, "Please check internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeControl()  {
        arrayList=new ArrayList<>();
        btnConfirmSelect= (Button) findViewById(R.id.btnConfirmSelect);
        recyclerView= (RecyclerView) findViewById(R.id.recycler);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        chkSelectAll=(CheckBox)findViewById(R.id.chkSelectAll);
        txtHeading.setText("Select Switch");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        imgBack.setOnClickListener(this);
        btnConfirmSelect.setOnClickListener(this);


        chkSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chkSelectAll.isChecked()){
                    for(int i=0;i<arrayList.size();i++){
                        HashMap<String,String> tempMap=arrayList.get(i);
                        tempMap.put(SELECTED_SWITCH,"1");
                        arrayList.set(i,tempMap);
                    }

                }else{
                    for(int i=0;i<arrayList.size();i++){
                        HashMap<String,String> tempMap=arrayList.get(i);
                        tempMap.put(SELECTED_SWITCH,"0");
                        arrayList.set(i,tempMap);
                    }
                }



                setSelectionAdapter();
            }
        });

    /*    chkSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d("switch_selected", "Menu_onCheckedChanged: "+"aaa"+arrayList);
                if(isChecked){
                    for(int i=0;i<arrayList.size();i++){
                        HashMap<String,String> tempMap=arrayList.get(i);
                        tempMap.put(SELECTED_SWITCH,"1");
                        arrayList.set(i,tempMap);
                    }

                }else{
                    for(int i=0;i<arrayList.size();i++){
                        HashMap<String,String> tempMap=arrayList.get(i);
                        tempMap.put(SELECTED_SWITCH,"0");
                        arrayList.set(i,tempMap);
                    }
                }



                setSelectionAdapter();
            }
        });*/

    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(message, new IntentFilter("unSelectChcekbox"));
    }

    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

                chkSelectAll.setChecked(false);

        }
    };


    private void setSelectionAdapter(){
        String checkSelected;
        if(chkSelectAll.isChecked())
        {
             checkSelected="true";
        }
        else
        {
             checkSelected="true1";
        }
        String selctedId=getIntent().getStringExtra("ID");
        arrListProfile=database.getAllModeInfo();

        adapter = new SelectSwitchesAdapter(this,arrayList,mIconArrayList,arrListProfile,selctedId,checkSelected);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
         switch (v.getId())
         {
             case R.id.imgBack:
                 this.finish();
                 break;
             case R.id.btnConfirmSelect:
                 OnClickOfConfirm();
                 break;
         }

    }

    private void OnClickOfConfirm() {

         ArrayList<HashMap<String,String>> mMapList= (ArrayList<HashMap<String, String>>) adapter.getItem();
         int totalCount=0;

        ArrayList<String> mSwitchId=new ArrayList<>();
        ArrayList<String> isSelected=new ArrayList<>();
        ArrayList<String> mSwitchNumber=new ArrayList<>();
        ArrayList<String> mSwitchpanel=new ArrayList<>();
        ArrayList<String> mDimmerStatus=new ArrayList<>();

        mSwitchIDOFF=new ArrayList<>();
        mSwitchIDOFF.clear();

        Log.d("switch_name_array","=="+mMapList);
         for(int i=0;i<mMapList.size();i++){
             if(mMapList.get(i).get(SELECTED_SWITCH).equals("1"))
             {
                 mSwitchId.add(mMapList.get(i).get(SWITCH_ID));
                 mSwitchNumber.add(mMapList.get(i).get(SWITCH_NUMBER));
                 mSwitchpanel.add(mMapList.get(i).get(PANEL_NAME));
                 mDimmerStatus.add(mMapList.get(i).get(DIMMER_STATUS));



                 Log.d("switch_name","==");

                 totalCount=totalCount+1;
             }else{
              //   mSwitchIDOFF.add(mMapList.get(i).get(SWITCH_ID));
                 mSwitchIDOFF.add(mMapList.get(i).get(SWITCH_NUMBER));
             }
         }

        if(!getIntent().getStringExtra("ACTION").equals("00")) {

            if (mSwitchId.size() > 0) {
                callWebServiceSwitchList(mSwitchId, mMapList);
            } else {
                Toast.makeText(this, "Select switch to perform action.", Toast.LENGTH_SHORT).show();
            }

        }else{
            String strSwitchONID="",strSwitchOFFID="",panelNumber="",dimmerStatus="",strSwitchSelectId="";


            Log.d("switch_name1","=="+mSwitchId.size());
            for (int i=0;i<mSwitchId.size();i++){
                if(i<mSwitchId.size()-1) {
                    strSwitchSelectId = strSwitchSelectId + mSwitchId.get(i) + ":";

                    strSwitchONID = strSwitchONID + mSwitchNumber.get(i) + ":";
                    panelNumber = panelNumber + mSwitchpanel.get(i) + ":";
                    dimmerStatus =dimmerStatus + mDimmerStatus.get(i) + ":";
                }else{
                    strSwitchSelectId = strSwitchSelectId + mSwitchId.get(i) + ":";

                    strSwitchONID = strSwitchONID + mSwitchNumber.get(i);
                    panelNumber = panelNumber + mSwitchpanel.get(i) + ":";
                    dimmerStatus =dimmerStatus + mDimmerStatus.get(i) + ":";
                }
            }

            for (int i=0;i<mSwitchIDOFF.size();i++){

                if(i<mSwitchIDOFF.size()-1) {
                    strSwitchOFFID = strSwitchOFFID + mSwitchIDOFF.get(i) + ":";
                //    panelNumber = panelNumber + mSwitchpanel.get(i) + ":";
                }else{
                    strSwitchOFFID = strSwitchOFFID + mSwitchIDOFF.get(i);
                  //  panelNumber = panelNumber + mSwitchpanel.get(i) + ":";
                }
            }

            Log.d("Switch ON",strSwitchONID);
            Log.d("Switch OFF",strSwitchOFFID);


      //      database.updateSeletctedSwitch(mMapList.get(i).get(SWITCH_NUMBER),mMapList.get(i).get(PANEL_NAME),"1");
            database.updateModeStatus(strSwitchONID,strSwitchOFFID,panelNumber,dimmerStatus,getIntent().getStringExtra("ID"),strSwitchSelectId);

            onBackPressed();

//            if (mSwitchId.size() > 0) {
//                callWebServiceSwitchList(mSwitchId, mMapList);
//            }

        }


    }

    private class AsyncGetSwitchList extends AsyncTask<Void,Void,String>
    {
        ArrayList<String> mArraySwitchId;
        ArrayList<HashMap<String,String>> mAllSwitch;

        public AsyncGetSwitchList(ArrayList<String> mSwitchId,ArrayList<HashMap<String,String>> allSwitch) {
            mArraySwitchId=mSwitchId;
            mAllSwitch=allSwitch;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SwitchSelectionActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params)
        {

            Constants req=new Constants();
            String response=null;
            try {
                String mBody = CreateAllSwitchOFFBody(mArraySwitchId);
                Log.d("All Switches-->",mBody);
                response=req.doPostRequest(URL_GENIE + "/switch/turnOnOffAll", mBody, sessionManager.getSecurityToken());
            }catch (Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null) {
                pDialog.cancel();
            }
            if(result!=null) {
                Log.d("All OFF Result",result);
                parseResult(result,mAllSwitch);
            }
            else{
                Toast.makeText(SwitchSelectionActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String CreateAllSwitchOFFBody(ArrayList<String> mArraySwitchId) {

        SessionManager sessionManager=new SessionManager(SwitchSelectionActivity.this);
        JSONObject jObj=new JSONObject();
        String mSwitchId="",mSwitchIdOFF="",profileStatus="";

        String bSwitchStatus="";
        try{

            jObj.put("userId",sessionManager.getUSERID());
            jObj.put("onoffstatus",getIntent().getStringExtra("ACTION"));

            for (int i=0;i<mArraySwitchId.size();i++){
                if(i<mArraySwitchId.size()-1) {
                    mSwitchId = mSwitchId + mArraySwitchId.get(i) + ":";
                }else{
                    mSwitchId = mSwitchId + mArraySwitchId.get(i);
                }
            }

            jObj.put("switchId",mSwitchId);
            jObj.put("profileSwitchId","NA");
            jObj.put("profileOnOffStatus","NA");
            jObj.put("modeName","");
            jObj.put("messageFrom",messageType);

        }catch (Exception e){}

        return jObj+"";
    }

    private void parseResult(String result, ArrayList<HashMap<String, String>> mAllSwitch)
    {
        DatabaseHandler database=new DatabaseHandler(this);
        //String toastMessage="";
        try{
            JSONObject jResult=new JSONObject(result);
            if(jResult.getString("status").equals("SUCCESS")){
                String switchStatus="";
                sessionManager.setSelectedSwitch(true);
                for (int i=0;i<mAllSwitch.size();i++){
                    if(getIntent().getStringExtra("ACTION").equals("1"))
                    {
                        //toastMessage="All switches gets turn ON successfully";
                        switchStatus = (mAllSwitch.get(i).get(SELECTED_SWITCH).equals("1")) ? "1" :"0";
                     }
                    else
                    {
                        switchStatus = (mAllSwitch.get(i).get(SELECTED_SWITCH).equals("1")) ? "0" :"1";
                        //toastMessage="All switches gets turn OFF successfully";
                    }

                    database.updateSelectedSwitchStatus(mAllSwitch.get(i).get(SWITCH_ID),mAllSwitch.get(i).get(SELECTED_SWITCH),switchStatus);
                }

                Intent intent=new Intent();
                setResult(2,intent);
                finish();


                //Toast.makeText(this, toastMessage,Toast.LENGTH_SHORT).show();


            }else{
                Toast.makeText(this, "Please try again.", Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){e.printStackTrace();}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.select_menu, menu);
        CheckBox checkBox = (CheckBox) menu.findItem(R.id.action_checkbox).getActionView();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                   // setAdapter("0");
                }else{
                   // setAdapter("1");
                }
                //Log.d(TAG, "onOptionsItemSelected: "+isChecked);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
