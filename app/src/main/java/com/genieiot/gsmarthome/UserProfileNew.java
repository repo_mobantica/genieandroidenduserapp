package com.genieiot.gsmarthome;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import Fragments.Rooms;
import Session.Constants;
import Session.NetworkConnectionInfo;
import Session.SessionManager;
import Session.Utility;
import de.hdodenhof.circleimageview.CircleImageView;

import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.Constants.URL_IMAGE;
import static com.genieiot.gsmarthome.R.id.imgProfileImage;
import static com.squareup.picasso.NetworkPolicy.NO_CACHE;

/**
 * Created by root on 6/1/17.
 */

public class UserProfileNew extends Activity implements View.OnClickListener {

    private EditText mEdtFirstName, mEdtLastName, mEdtEmail, mEdtPhone, mEdtPassword, edBirthdate;
    private CircleImageView imgProfileImage;
    private RoundedImageView imageEdit;
    private String userChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private ImageView imgBack;
    private TextView txtHeading;
    private Button btnRegister;
    private String mBase64String = "";
    private ProgressDialog pDialog;
    private SessionManager sessionManager;
    private String mUserId;
    private String mFirstname, mLastname;
    public static final int RequestPermissionCode = 1;
    String encodedImage, encodedImage1;
    DatePickerDialog datePickerDialog;

    int dayOfMonth;
    Calendar calendar;
    String dobValue;
    int year, month;
    String selectedimagestring = "";
    Bitmap bm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_new);
        initialseControls();
        setProfileValue();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

    }

    private void initialseControls() {

        sessionManager = new SessionManager(UserProfileNew.this);
        mEdtFirstName = (EditText) findViewById(R.id.edtFirstname);
        mEdtLastName = (EditText) findViewById(R.id.edtLastName);
        mEdtEmail = (EditText) findViewById(R.id.edtEmail);
        mEdtPhone = (EditText) findViewById(R.id.edtPhone);
        mEdtPassword = (EditText) findViewById(R.id.edtPassword);
        edBirthdate = (EditText) findViewById(R.id.edBirthdate);
        imgProfileImage = (CircleImageView) findViewById(R.id.imgProfileImage);
        imageEdit = (RoundedImageView) findViewById(R.id.imageedit);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        txtHeading = (TextView) findViewById(R.id.txtHeading);

        btnRegister = (Button) findViewById(R.id.btnRegister);

        imgProfileImage.setOnClickListener(this);
        imageEdit.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        edBirthdate.setOnClickListener(this);

        sessionManager = new SessionManager(this);

    }

    private void setProfileValue() {

        mEdtFirstName.setText(sessionManager.getFirstName());
        mEdtLastName.setText(sessionManager.getLastName());
        mEdtEmail.setText(sessionManager.getEmail());
        mEdtPhone.setText(sessionManager.getPhone());
        edBirthdate.setText(sessionManager.getDOB());
        String dob = sessionManager.getDOB();
        String dob1[] = dob.split("/");
        Log.d("dob_val", "==" + dob);

        mUserId = sessionManager.getUSERID();

        String path = sessionManager.getUserImage();
        if (!path.equals("")) {
           /* String image_url = URL_IMAGE + path;
            Glide.with(UserProfileNew.this).load(image_url)
                    .error(R.drawable.profile)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfileImage);*/

            obj.execute();

        }

    }

    MyAsync obj = new MyAsync() {

        @Override
        protected void onPostExecute(Bitmap bmp) {
            super.onPostExecute(bmp);

            if (bmp != null) {
                imgProfileImage.setImageBitmap(null);
                imgProfileImage.destroyDrawingCache();
                imgProfileImage.setImageBitmap(bmp);
            }else {
                imgProfileImage.setImageResource(R.drawable.profile);
            }

            Log.d("UserProfileNew", "bitmap image: " + bm);
        }
    };

    public class MyAsync extends AsyncTask<Void, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Void... params) {

            String path = sessionManager.getUserImage();
            String image_url = URL_IMAGE + path;
            try {
                URL url = new URL(image_url);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgProfileImage:

                selectImage();
                break;
            case R.id.imageedit:
                selectImage();
                break;
            case R.id.imgBack:
                finish();
                break;
            case R.id.btnRegister:
                onClickOfEditProfile();
                break;
            case R.id.edBirthdate:
                editDOB();
                break;


        }
    }


    public void editDOB() {


        Toast.makeText(UserProfileNew.this, "click", Toast.LENGTH_SHORT).show();
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(UserProfileNew.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                        edBirthdate.setText(day + "/" + (month + 1) + "/" + year);


                        dobValue = String.valueOf(day).concat(String.valueOf(month + 1)).concat(String.valueOf(year));
                        Log.d("Dob_value", "==" + dobValue);
                    }
                }, year, month, dayOfMonth);
        //   datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();


    }

    public void EnableRuntimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(UserProfileNew.this, android.Manifest.permission.CAMERA)) {
            Toast.makeText(UserProfileNew.this, "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {
            ActivityCompat.requestPermissions(UserProfileNew.this, new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);

        }
    }

    private void onClickOfEditProfile() {
        mFirstname = mEdtFirstName.getText().toString();
        mLastname = mEdtLastName.getText().toString();
        if (mEdtFirstName.getText().toString().equals("")) {
            Toast.makeText(this, "Enter First Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (mEdtLastName.getText().toString().equals("")) {
            Toast.makeText(this, "Enter Last Name", Toast.LENGTH_SHORT).show();
            return;
        }
        /*if (!isImageSelect) {
            Toast.makeText(this, "Please Select Profile Image", Toast.LENGTH_SHORT).show();
            return;
        }*/

        if (bm != null){
            selectedimagestring = getStringImage(bm);
        }

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);


        if (Constants.isInternetAvailable(this)) {
               new AsyncEditProfileTask().execute();

        } else {
            Toast.makeText(this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }


    }

    private BroadcastReceiver message12 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

         //   new AsyncEditProfileTask().execute();
        }
    };


  /*  @Override
    protected void onResume() {
        super.onResume();
        txtHeading.setText("User Account");


        LocalBroadcastManager.getInstance(this).registerReceiver(message12, new IntentFilter("userProfileUpdate"));

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(message12);
    }
*/

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }*/

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(UserProfileNew.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= Utility.checkPermission(UserProfileNew.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask ="Choose from Library";
                    if(result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
        /*Intent gallery =
                new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, SELECT_FILE);*/
    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.mkdirs();
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        bm = thumbnail;
        imgProfileImage.setImageBitmap(null);
        imgProfileImage.destroyDrawingCache();
        imgProfileImage.setImageBitmap(thumbnail);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if(userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bitmaps=null;
        if (data != null) {
            try {
                bitmaps = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }

            /*Uri imageUri = data.getData();

            try {
                bitmaps = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

                final ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmaps.compress(Bitmap.CompressFormat.PNG, 90, stream);
                byte[] byteArray = stream.toByteArray();

               String encodeded = Base64.encodeToString(byteArray, Base64.DEFAULT);

                byte[] decodedString = Base64.decode(encodeded, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                bm = bitmaps;
                imgProfileImage.setImageBitmap(null);
                imgProfileImage.destroyDrawingCache();
                imgProfileImage.setImageBitmap(bitmaps);
                image.setImageBitmap(decodedByte);
                imageEdit.setImageBitmap(decodedByte);

            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }

        imgProfileImage.setImageBitmap(null);
        imgProfileImage.destroyDrawingCache();
        imgProfileImage.setImageBitmap(bitmaps);

    }


    private class AsyncEditProfileTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(UserProfileNew.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String response = "";
            JSONObject jBoby = CreateJSONBody();
            Log.d("UserProfileNew", "jbody: " + jBoby);
            Constants res = new Constants();
            try {
                response = res.doPostRequest(URL_GENIE_AWS + "/user/updateprofile", jBoby + "", sessionManager.getSecurityToken());
                Log.d("Update_Profile", "URL===" + URL_GENIE_AWS + "/user/updateprofile");
                Log.d("Update_Profile", "OBJ===" + jBoby);
                Log.d("Update_Profile", "Response===" + response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }

            if (result != null) {
                Log.d("Profile Response ", result);
                parseProfileResult(result);


            } else {
                Toast.makeText(UserProfileNew.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private JSONObject CreateJSONBody() {
        JSONObject jMain = new JSONObject();
        try {

            jMain.put("userId", mUserId);
            jMain.put("firstName", mFirstname);
            jMain.put("lastName", mLastname); //

            //jMain.put("birthDate", "25/10/1994");
            jMain.put("birthDate", edBirthdate.getText().toString());
            if (selectedimagestring.equals("")) {
                jMain.put("imageStatus", "NA");
            } else {
                jMain.put("imageStatus", "UPLOAD");

            }

            jMain.put("phoneNumber", sessionManager.getPhone());
            jMain.put("image", selectedimagestring);

        } catch (Exception e) {
        }

        return jMain;
    }

    private void parseProfileResult(String result) {
        try {
            JSONObject jResult = new JSONObject(result);
            if (jResult.getString("status").equals("SUCCESS")) {
                //sejResult.getString("image")
                JSONObject jImage = new JSONObject(jResult.getString("result"));
                // new AsyncUserDataTask().execute();

                if (!jImage.getString("image").equals("")) {

                    sessionManager.setUserImage(jImage.getString("image"));

                  //  obj.execute();
                }

                sessionManager.setFirstName(mFirstname);
                sessionManager.setLastName(mLastname);
                sessionManager.setDOB(jImage.getString("birthDate"));
                Toast.makeText(this, "Profile updated successfully.", Toast.LENGTH_SHORT).show();
                this.finish();


            } else {
                Toast.makeText(this, "Profile Changed fail.", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class AsyncUserDataTask extends AsyncTask<Void, Void, String> {

        SessionManager sessionManager = new SessionManager(UserProfileNew.this);

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(UserProfileNew.this);
            pDialog.setMessage("Syncing data to pyramid...");
            pDialog.setCancelable(false);
            pDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            //Constants.URL_GENIE = "http://"+sessionManager.getAPPURL()+":8080/GSmart_final_9jan/";
            String mResponse = null;
            Constants req = new Constants();
            try {
                JSONObject jBody = new JSONObject();
                jBody.put("id", sessionManager.getUSERID());
                jBody.put("firstName", sessionManager.getFirstName());
                jBody.put("lastName", sessionManager.getLastName());
                jBody.put("email", sessionManager.getEmail());
                jBody.put("password", sessionManager.getPassword());
                jBody.put("phoneNumber", sessionManager.getPhone());
                jBody.put("userType", sessionManager.getUserType());
                jBody.put("image", sessionManager.getUserImage());
                jBody.put("deviceId", sessionManager.getDeviceToken());
                jBody.put("deviceType", "ANROID");
                jBody.put("isEmailVerified", 1);
                jBody.put("isFirstLogin", 0);
                jBody.put("homeid", sessionManager.getHomeId());

                Log.d("syncShareControlData ", jBody + "");
                String url = "http://" + sessionManager.getAPPURL() + ":8080/smart_home_local/user/syncShareControlData";
                Log.d("URL ", url);
                mResponse = req.doPostRequest(url, jBody + "", sessionManager.getSecurityToken());
            } catch (Exception e) {
            }
            ;

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }

            if (result != null) {
                Log.d("syncUserControl Res ", result + "");
                try {

                    JSONObject jMain = new JSONObject(result);
                    if (jMain.getString("status").equals("SUCCESS")) {
                        sessionManager.setUserSync(true);
                        Log.d("Local profile", "Success");
                    } else {
                        Log.d("Local Profile", "Failure");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            } else {

            }
            finish();
        }
    }

    //Below code is used for the Save image in the database
    public String getStringImage(Bitmap bmp) {
        if (bmp != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            return encodedImage;
        } else {
            String encodedImage = "";
            //  Toast.makeText(Profile_Edit.this, "Select Image", Toast.LENGTH_LONG).show();
            return encodedImage;
        }

    }

}
