package com.genieiot.gsmarthome;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import Session.SessionManager;

/**
 * Created by root on 11/5/16.
 */

public class MyFirebaseInstanceIDService  extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Displaying token on logcat
      //  Toast.makeText(this, ""+refreshedToken,Toast.LENGTH_SHORT).show();
        SessionManager session=new SessionManager(this);
        session.saveDeviceToken(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}
