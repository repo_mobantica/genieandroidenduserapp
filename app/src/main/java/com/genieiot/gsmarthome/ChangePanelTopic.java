package com.genieiot.gsmarthome;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import Session.SessionManager;

/**
 * Created by root on 9/1/17.
 */

public class ChangePanelTopic extends AppCompatActivity implements View.OnClickListener{
    EditText edtChangeTopic;
    private Button btnSubmit;
    private TextView txtHeading;
    private ImageView imgBack;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_paneltopic_activity);
        initComponent();
    }

    private void initComponent() {
        edtChangeTopic= (EditText) findViewById(R.id.edtChangeTopic);
        btnSubmit= (Button) findViewById(R.id.btnSubmit);
        txtHeading= (TextView) findViewById(R.id.txtHeading);
        imgBack= (ImageView) findViewById(R.id.imgBack);
        sessionManager=new SessionManager(this);

        txtHeading.setText("Change Topic");
        edtChangeTopic.setText(sessionManager.getTopicName());
        btnSubmit.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:

                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                String mNewTopic=edtChangeTopic.getText().toString();
                if(mNewTopic.equals("")) {
                    Toast.makeText(ChangePanelTopic.this,"Please enter topic Name.",Toast.LENGTH_SHORT).show();
                    return;
                }

                sessionManager.setTopicName(mNewTopic);
                finish();
                Toast.makeText(this,"Update Topic name successfully.",Toast.LENGTH_SHORT).show();
                break;
            case R.id.imgBack:
                this.finish();
                break;

        }


    }
}
