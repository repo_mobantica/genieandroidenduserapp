package com.genieiot.gsmarthome;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Database.DatabaseHandler;
import Session.SessionManager;

import static Database.DatabaseHandler.ROOM_ID;

/**
 * Created by root on 31/12/16.
 */

public class ChangeIPActivity extends Activity implements View.OnClickListener
{

    EditText mEdtOldIp,mEdtNewIp;
    private Button mBtnSumit;
    private TextView txtHeading;
    String mRoomId;
    String mAddress="";
    SessionManager sessionManager;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.changeipaddress);
        Intent intent=getIntent();
        mRoomId=intent.getStringExtra(ROOM_ID);
        sessionManager=new SessionManager(this);
        getRoomIP();
        initializeControls();
    }

    private String getRoomIP() {

        if(mRoomId.equals("1")){
            mAddress=sessionManager.getFirstString();
        }
        else if(mRoomId.equals("2")){
            mAddress=sessionManager.getSecondString();
        }
        else if(mRoomId.equals("3")){
            mAddress=sessionManager.getThirdString();
        }

        return mAddress;
    }

    private void initializeControls() {
        mEdtOldIp= (EditText) findViewById(R.id.edtOldIpAddress);
        mEdtNewIp= (EditText) findViewById(R.id.edtNewIpAddress);
        mBtnSumit= (Button) findViewById(R.id.btnSubmit);
        txtHeading= (TextView) findViewById(R.id.txtHeading);
        txtHeading.setText("Change IP");
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        mEdtOldIp.setText(getRoomIP());
        mEdtNewIp.setText(getRoomIP());
        mBtnSumit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                onClickOfSubmitButton();
                break;
            case R.id.imgBack:ChangeIPActivity.this.finish();
                  break;
        }

    }

    private void onClickOfSubmitButton() {

        String mNewIP=mEdtNewIp.getText().toString();

        if(mNewIP.equals("")){
            Toast.makeText(this, "Please enter IP.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!validate(mNewIP)){
            Toast.makeText(this, "Please enter IP in proper Format.", Toast.LENGTH_SHORT).show();
            return;
        }


        if(mRoomId.equals("1")){
            sessionManager.setFirstString(mNewIP);
        }else if(mRoomId.equals("2")){
            sessionManager.setSecondString(mNewIP);
        }else if(mRoomId.equals("3")){
            sessionManager.setThirdString(mNewIP);
        }

        DatabaseHandler db=new DatabaseHandler(this);
        db.deleteRoomSwitchData(mRoomId);

        finish();

    }

    private static final String PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static boolean validate(String ip){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

}
