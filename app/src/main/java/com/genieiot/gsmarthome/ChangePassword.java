package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.hockeyapp.android.CrashManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Session.Constants;
import Session.SessionManager;
import app.AppController;

import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.MESSAGE_TRY_AGAIN;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.MainActivity.myToast;

/**
 * Product Name : Square
 * Company Name : Genie Smart Home
 * Package Name : com.genieiot.geniesmarthome
 * version      : 1
 * Author       : Digvijay,Reshma
 * Description  : This page is used to.
 */

public class ChangePassword extends Activity implements View.OnClickListener {

    private EditText edtOldPassword;
    private EditText edtNewPassword;
    private EditText edtConfirmPassword;
    private Button btnSave;
    private ProgressDialog pDialog;
     SessionManager sessionManager;
    private String mOldPassword,mNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        initControls();
    }

    private void initControls() {

        edtOldPassword = (EditText) findViewById(R.id.edtOldPassword);
        edtNewPassword = (EditText) findViewById(R.id.edtNewPassword);
        edtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);
        sessionManager=new SessionManager(ChangePassword.this);
       // Constants.URL_GENIE = "http://"+sessionManager.getAPPURL()+":8080/GSmart_final/";
        btnSave = (Button) findViewById(R.id.btnSave);
        btnSave.setOnClickListener(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);

    }

    @Override
    public void onClick(View v) {


        if (isFieldSubmtted())
        {

                if(sessionManager.getUSERID().equals(""))
                {
                    Toast.makeText(ChangePassword.this,getResources().getString(R.string.MSG_DEMO_USER),Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }

                if(MainActivity.isInternetAvailable(ChangePassword.this))
                {
                    mOldPassword=edtOldPassword.getText().toString();
                    mNewPassword=edtNewPassword.getText().toString();
                    new AsyncNewPasswordTask().execute();

                }
                else
                {
                  myToast(ChangePassword.this, MESSAGE_INTERNET_CONNECTION);
                }

         }

    }

    class AsyncNewPasswordTask extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String response=null;

            try{
                JSONObject jBody=new JSONObject();
                jBody.put("userId",sessionManager.getUSERID());
                jBody.put("oldPassword",mOldPassword);
                jBody.put("newPassword",mNewPassword);
                Log.d("Change Password ",jBody+"");
                response=net.doPostRequest(URL_GENIE_AWS+ "/user/updatepassword",jBody+"",sessionManager.getSecurityToken());
            }catch(Exception e){}
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null && pDialog.isShowing()){
                pDialog.cancel();
            }

            Log.d("Change Password ",""+result);

            if(result!=null){
              try{

                  JSONObject response=new JSONObject(result);
                  String status = response.getString("status");
                  String msg = response.getString("msg");

                  if (status.equals("SUCCESS")) {
                      myToast(ChangePassword.this, msg);
                      ChangePassword.this.finish();
                  }else{
                      myToast(ChangePassword.this, msg);
                  }

              }catch(Exception e){}
            }else{
                Toast.makeText(ChangePassword.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isFieldSubmtted() {

        if (edtOldPassword.getText().toString().length() < 1) {
            myToast(ChangePassword.this,getResources().getString(R.string.MSG_OLD_PASS));
            return false;

        } else if (edtNewPassword.getText().toString().length() < 1) {

            myToast(ChangePassword.this,getResources().getString(R.string.MSG_NEW_PASS));
            return false;

        }
        else if(edtNewPassword.getText().toString().length() < 6){
            myToast(ChangePassword.this,getResources().getString(R.string.MSG_VAILD_PASS));
            return false;
        }else if (edtConfirmPassword.getText().toString().length() < 1) {

            myToast(ChangePassword.this,getResources().getString(R.string.MSG_CONFIRM_PASS));
            return false;

        } else if (!edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {

            myToast(ChangePassword.this,getResources().getString(R.string.MSG_CONFIRM_MATCH));
            return false;

        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }
}
