package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;

import org.json.JSONObject;

import Session.Constants;
import Session.SessionManager;

import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by Genie IoT 2 on 9/27/2016.
 */
public class ShareControl extends Activity implements View.OnClickListener {

    EditText edtEmail,edtPhoneNo;
    RadioButton radioAdmin, radioNormal,radioModerateuser;
    Button btnShare;
    ImageView imgBack;
    private TextView txtHeading;
    private RadioGroup rgShareName;
    private String mSelectedUser;
    private ProgressDialog pDialog;
    private String mEmail,mUserId,mPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sharecontrol);
        initializeControls();
    }

    private void initializeControls() {

        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPhoneNo = (EditText) findViewById(R.id.edtPhoneNo);
        radioAdmin = (RadioButton) findViewById(R.id.radioadmin);
        radioModerateuser = (RadioButton) findViewById(R.id.radioModerateuser);
        radioNormal = (RadioButton) findViewById(R.id.radionormaluser);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        btnShare = (Button) findViewById(R.id.btnSave);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        rgShareName= (RadioGroup) findViewById(R.id.rgShareName);

        txtHeading.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        rgShareName.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioadmin:
                        mSelectedUser=0+"";
                        break;

                    case R.id.radioModerateuser:
                        mSelectedUser=1+"";
                        break;

                    case R.id.radionormaluser:
                        mSelectedUser=2+"";
                        break;

                }

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSave:
                if (isFieldSubmtted()) {


                    mEmail=edtEmail.getText().toString();
                    mPhoneNumber=edtPhoneNo.getText().toString();

                    SessionManager session=new SessionManager(this);
                    mUserId=session.getUSERID();

                    if(!session.getDemoUser().equals("DemoUser")) {

                        final Dialog dialog = new Dialog(ShareControl.this);
                        // Include dialog.xml file
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dialog_password);
                        final EditText edtPasswordShareControl = (EditText) dialog.findViewById(R.id.edtPasswordShareControl);
                        TextView btnok = (TextView) dialog.findViewById(R.id.txtOk);
                        TextView btncancel = (TextView) dialog.findViewById(R.id.txtCancel);

                        btnok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String strPass = edtPasswordShareControl.getText().toString();
                                if (strPass.length() < 1) {
                                    myToast(getResources().getString(R.string.MSG_VAILD_PASS));
                                }
                                else
                                {
                                    SessionManager session = new SessionManager(ShareControl.this);
                                    if (session.getUserType().equals("0"))
                                    {
                                        if (session.getPassword().equals(strPass)) {

                                            dialog.dismiss();
                                            new AsyncRegisterShareUser().execute(URL_GENIE_AWS, "0");

                                        }
                                        else {
                                            myToast(getResources().getString(R.string.MSG_CORRECT_PASS));
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(ShareControl.this,getResources().getString(R.string.MSG_DEMO_USER),Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                        });
                        btncancel.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });
                        dialog.show();
                    }
                    else {
                        Toast.makeText(this,getResources().getString(R.string.MSG_USER_AUTH),Toast.LENGTH_SHORT).show();
                    }

                } else {

                }
                break;

            case R.id.imgBack:
                Intent intent = new Intent(ShareControl.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
        }
    }

    public boolean validateEmail() {
        String emailInput, emailPattern;

        emailInput = edtEmail.getText().toString().trim();

        emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        if (!emailInput.matches(emailPattern)) {
            return false;
        }

        return true;
    }
    private boolean isFieldSubmtted() {

        if (edtEmail.getText().toString().length() < 1) {
            myToast(getResources().getString(R.string.MSG_VALID_MAIL));
            return false;

        }else if(edtPhoneNo.getText().toString().length()<10){
             myToast(getResources().getString(R.string.MSG_VALID_PHONE));
            return false;
        } else if (!validateEmail()) {
            myToast(getResources().getString(R.string.MSG_VALID_MAIL));
            return false;
        } else if (!radioAdmin.isChecked() && !radioNormal.isChecked() && !radioModerateuser.isChecked()) {
            myToast(getResources().getString(R.string.MSG_ChOS_USER));
            return false;
        }

        return true;
    }
    private void myToast(String message) {

        Toast.makeText(ShareControl.this, message, Toast.LENGTH_SHORT).show();
    }
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
        txtHeading.setText("Share Control");
    }

    class AsyncRegisterShareUser extends AsyncTask<String,Void,String> {
        String mURLFlag;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ShareControl.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            mURLFlag=params[1];
            Constants con=new Constants();
            String mResponse=null;
            try{
                SessionManager session=new SessionManager(ShareControl.this);
                String mRequest=createJSONBody();
                mResponse=con.doPostRequest(params[0]+"/user/shareControl",mRequest,session.getSecurityToken());

                Log.d("Sharecontrol","url==="+params[0]+"/user/shareControl");
                Log.d("Sharecontrol","resquest==="+mRequest);
                Log.d("Sharecontrol","response==="+mResponse);

            }catch(Exception e){}
            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null && pDialog.isShowing()){
                pDialog.cancel();
            }
            if(result!=null){
                ParseShareResponse(result,mURLFlag);
            }else{
                Toast.makeText(ShareControl.this,getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_LONG).show();
            }
        }
    }

    private String createJSONBody() {
        JSONObject jMain=new JSONObject();
        try{
            jMain.put("email",mEmail);
            jMain.put("userType",mSelectedUser);
            jMain.put("adminUserId",mUserId);
            jMain.put("phoneNumber",mPhoneNumber);

        }catch(Exception e){}
        return jMain+"";
    }

    private void ParseShareResponse(String result,String strURLFlag) {
        try{
            JSONObject response=new JSONObject(result);

            if (response.getString("status").equals("SUCCESS")) {
                    Toast.makeText(ShareControl.this,getResources().getString(R.string.MSG_ADD_USER), Toast.LENGTH_LONG).show();
                    finish();

            }else{
                Toast.makeText(ShareControl.this,response.getString("msg"), Toast.LENGTH_LONG).show();
            }

        }catch (Exception e){}
    }


}
