package com.genieiot.gsmarthome;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Session.Constants;
import Session.SessionManager;
import app.AppController;
import de.hdodenhof.circleimageview.CircleImageView;

import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.MESSAGE_TRY_AGAIN;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by Genie IoT on 9/21/16.
 * coding by DJ & Reshma
 */

public class UserProfile extends Activity implements View.OnClickListener {
    EditText mEdtFirstName, mEdtLastName, mEdtEmail, mEdtPhone;
    Button mBtnRegister;
    ImageView mImageedit, mImgBack;
    CircleImageView imgProfileImage;
    private TextView txtHeading;
    private ProgressDialog pDialog;
    private SessionManager sessionManager;
    private Uri mImageCaptureUri;
    private File outPutFile = null;
    Bitmap bmImg;
    String imgString;

    private final static int REQUEST_PERMISSION_REQ_CODE = 34;
    private static final int CAMERA_CODE = 101, GALLERY_CODE = 201, CROPING_CODE = 301;
    private String mFirstName,mLastName,mEmail,mPhone;
    private String mBase64String="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        outPutFile = new File(Environment.getExternalStorageDirectory(), "temp.jpg");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        initialseControls();
        txtHeading.setText("User Account");
        setUserDetail();

        mImgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.imgBack:
                        Intent intent = new Intent(UserProfile.this, MainActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        break;
                }
            }
        });

        mImageedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  selectImage();
                //  selectImageOption();
                SessionManager sessionManager=new SessionManager(UserProfile.this);
                if(sessionManager.getDemoUser().equals("DemoUser")){

                }else {
                    selectimage();
                }

            }
        });
        //setUserImage();
    }

    private  void selectimage()  {
        final Dialog dialog = new Dialog(UserProfile.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert);

        TextView capturephoto= (TextView) dialog.findViewById(R.id.capturephoto);
        TextView choosefromgalary= (TextView) dialog.findViewById(R.id.choosefromgallery);
        TextView cancel= (TextView) dialog.findViewById(R.id.cancel);


        capturephoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                mImageCaptureUri = Uri.fromFile(f);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
                startActivityForResult(intent, CAMERA_CODE);
                dialog.dismiss();
            }
        });
        choosefromgalary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, GALLERY_CODE);
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                sessionManager.saveUserImage(null);
                imgProfileImage.setImageResource(R.drawable.avatar);
                dialog.dismiss();

            }
        });
        dialog.show();
        // dialog.getWindow().setAttributes(lp);
    }
    private void CropingIMG() {

        final ArrayList<CropingOption> cropOptions = new ArrayList<CropingOption>();

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setType("image/*");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(this, "Cann't find image croping app", Toast.LENGTH_SHORT).show();
            return;
        } else {
            intent.setData(mImageCaptureUri);
            intent.putExtra("outputX", 512);
            intent.putExtra("outputY", 512);
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("scale", true);

            //Create output file here
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(outPutFile));

            if (size == 1) {
                Intent i = new Intent(intent);
                ResolveInfo res = (ResolveInfo) list.get(0);

                i.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

                startActivityForResult(i, CROPING_CODE);
            } else {
                for (ResolveInfo res : list) {
                    final CropingOption co = new CropingOption();
                    co.title = getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
                    co.icon = getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
                    co.appIntent = new Intent(intent);
                    co.appIntent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                    cropOptions.add(co);
                }

                CropingOptionAdapter adapter = new CropingOptionAdapter(getApplicationContext(), cropOptions);

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Choose Croping App");
                builder.setCancelable(false);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        startActivityForResult(cropOptions.get(item).appIntent, CROPING_CODE);
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                        if (mImageCaptureUri != null) {
                            getContentResolver().delete(mImageCaptureUri, null, null);
                            mImageCaptureUri = null;
                        }
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
    private Bitmap decodeFile(File f) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 512;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }
    private void setUserDetail() {
        SessionManager sessionManager = new SessionManager(UserProfile.this);
        if (!sessionManager.getUSERID().equals("")) {
            mEdtEmail.setEnabled(false);
            mEdtFirstName.setText(sessionManager.getFirstName());
            mEdtLastName.setText(sessionManager.getLastName());
            mEdtEmail.setText(sessionManager.getEmail());
            mEdtPhone.setText(sessionManager.getPhone());

        } else {
            mEdtFirstName.setEnabled(false);
            mEdtLastName.setEnabled(false);
            mEdtPhone.setEnabled(false);
            mEdtEmail.setEnabled(false);
        }

        //  mEdtPassword.setText(sessionManager.getPASSWORD());
        // mViewImage.setImageResource(Integer.parseInt(sessionManager.getUserImage()));
    }
    protected void onResume() {
        super.onResume();
        //setUserImage();
        if( ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_REQ_CODE);
            return;
        }

        if (ContextCompat.checkSelfPermission(UserProfile.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.CALL_PHONE}, REQUEST_PERMISSION_REQ_CODE);
            return;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SessionManager sessionManager = new SessionManager(UserProfile.this);
        if (requestCode == GALLERY_CODE && resultCode == RESULT_OK && null != data) {
            mImageCaptureUri = data.getData();
            sessionManager.saveUserImage(outPutFile.toString());
            System.out.println("Gallery Image URI : " + mImageCaptureUri);
            CropingIMG();


        } else if (requestCode == CAMERA_CODE && resultCode == Activity.RESULT_OK) {
            sessionManager.saveUserImage(outPutFile.toString());

            System.out.println("Camera Image URI : " + mImageCaptureUri);
            CropingIMG();

        } else if (requestCode == CROPING_CODE) {
            try {
                if (outPutFile.exists()) {
                    Bitmap photo = decodeFile(outPutFile);
                    ByteArrayOutputStream bao = new ByteArrayOutputStream();
                    photo.compress(Bitmap.CompressFormat.JPEG, 100, bao);
                    byte [] ba = bao.toByteArray();
                    mBase64String=Base64.encodeToString(ba,Base64.DEFAULT);
                    imgProfileImage.setImageBitmap(photo);
                } else {
                    Toast.makeText(getApplicationContext(), "Error while save image", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } } }

    //initialization of multiple views so we can use this anywhere using perticular ID
    private void initialseControls() {
        sessionManager = new SessionManager(UserProfile.this);
        mEdtFirstName = (EditText) findViewById(R.id.edtFirstname);
        mEdtLastName = (EditText) findViewById(R.id.edtLastName);
        mEdtEmail = (EditText) findViewById(R.id.edtEmail);
        mEdtPhone = (EditText) findViewById(R.id.edtPhone);
        // mEdtPassword = (EditText) findViewById(R.id.edtPassword);
        mBtnRegister = (Button) findViewById(R.id.btnRegister);
        mImageedit = (ImageView) findViewById(R.id.imageedit);
       // mViewImage = (ImageView) findViewById(R.id.viewImage);
        imgProfileImage= (CircleImageView) findViewById(R.id.imgProfileImage);
        mImgBack = (ImageView) findViewById(R.id.imgBack);
        mBtnRegister.setOnClickListener(this);
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        txtHeading.setOnClickListener(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        int[] posXY = new int[2];
        imgProfileImage.getLocationOnScreen(posXY);
        int x = posXY[0];
        int y = posXY[1];

//        float x=mViewImage.getX();
//        float y=mViewImage.getY();

        mImageedit.setX(x);
        mImageedit.setY(y);


    }
    //apply on click event on perticular view with specified ID
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:


                if(!sessionManager.getDemoUser().equals("DemoUser")) {
                    if (isFieldSubmtted()) {

                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        if (!sessionManager.getUSERID().equals("")) {
                            if (MainActivity.isInternetAvailable(UserProfile.this)) {
                                //updateUserAccount();
                                mFirstName = mEdtFirstName.getText().toString();
                                mLastName = mEdtLastName.getText().toString();
                                mEmail = mEdtEmail.getText().toString();
                                mPhone = mEdtPhone.getText().toString();
                                new AsyncUpdateProfileTask().execute();
                            }
                            else
                            {
                                myToast(MESSAGE_INTERNET_CONNECTION);
                            }

                        }
                        else
                        {
                            Toast.makeText(this, "You are a demo user.", Toast.LENGTH_SHORT).show();
                            //saveUserDetailInSharedPreference();
                        }
                    }
                }
                else
                {
                    Toast.makeText(this, "You are a demo user.", Toast.LENGTH_SHORT).show();
                }
        }
    }

    class AsyncUpdateProfileTask extends AsyncTask<String,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected String doInBackground(String... params) {
            Constants net=new Constants();
            String mResponse=null;
            try{
                JSONObject jBody=new JSONObject();
                jBody.put("userId",sessionManager.getUSERID());
                jBody.put("firstName",mFirstName);
                jBody.put("lastName",mLastName);
                jBody.put("image",mBase64String);
                jBody.put("phoneNumber",mPhone);

                Log.d("Update Profile request ",jBody+"");
                mResponse=net.doPostRequest(URL_GENIE_AWS,jBody+"",sessionManager.getSecurityToken());
            }catch(Exception e){}
            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("Profile response ",result);
            hideProgressDialog();
            if(result!=null){
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend"));
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend3"));
             parseUpdateResponse(result);
            }else{
                Toast.makeText(UserProfile.this, "Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void parseUpdateResponse(String result) {
        try{
            JSONObject jData=new JSONObject(result);
            if(jData.getString("status").equals("SUCCESS"))
            {

            }
            else
            {
                Toast.makeText(this,jData.getString("msg"),Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){}

    }

    private void saveUserDetailInSharedPreference() {

        sessionManager.saveUser("", mEdtFirstName.getText().toString(), mEdtLastName.getText().toString(), mEdtEmail.getText().toString(), mEdtPhone.getText().toString(), "");
        myToast("Successfully Updated");
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        UserProfile.this.finish();

    }
    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private void setUserImage() {

        SessionManager sessionManager = new SessionManager(UserProfile.this);
        String path = sessionManager.getUserImage();
        if (!path.equals("")) {

            try {
                bmImg = BitmapFactory.decodeFile(path);

                if(bmImg!=null) {
                    imgProfileImage.setImageBitmap(bmImg);
                    bmImg.recycle();
                    bmImg=null;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
    // convert from bitmap to byte array
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }
    private void updateUserAccount() {
        showProgressDialog();

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", sessionManager.getUSERID());
        jsonParams.put("firstName", mEdtFirstName.getText().toString());
        jsonParams.put("lastName", mEdtLastName.getText().toString());
        jsonParams.put("email", mEdtEmail.getText().toString());
        //jsonParams.put("phoneNumber", mEdtPhone.getText().toString());
        // jsonParams.put("password",mEdtPassword.getText().toString());

        imgString = Base64.encodeToString(getBytesFromBitmap(bmImg),Base64.NO_WRAP);
        jsonParams.put("image",imgString);


        Log.d("param", jsonParams.toString());
        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.POST, URL_GENIE_AWS + "" + "user",
                new JSONObject(jsonParams),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        if (response != null) {
                            String msg = "";
                            try {
                                String status = response.getString("status");
                                msg = response.getString("msg");

                                if (status.equals("SUCCESS")) {

                                    sessionManager.saveUser(sessionManager.getUSERID(), mEdtFirstName.getText().toString(), mEdtLastName.getText().toString(), mEdtEmail.getText().toString(), mEdtPhone.getText().toString(),"");
                                    myToast("Successfully Updated");
                                    UserProfile.this.finish();

                                } else
                                {
                                    myToast(msg);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                                myToast("try again");
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        error.printStackTrace();
                        myToast(MESSAGE_TRY_AGAIN);

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("X-AUTH-TOKEN", sessionManager.getSecurityToken());

                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(myRequest);
    }
    //apply validations on each field
    private boolean isFieldSubmtted() {

        if (mEdtFirstName.getText().toString().length() < 1) {

            myToast("Enter First Name");
            return false;

        } else if (mEdtLastName.getText().toString().length() < 1) {
            myToast("Enter Last Name");
            return false;
        } else if (mEdtEmail.getText().toString().length() < 1) {
            myToast("Enter Email");
            return false;
        } else if (!validateEmail()) {
            myToast("Enter Valid Email ID");
            return false;
        } else if (mEdtPhone.getText().toString().length() < 10) {
            myToast("Enter Valid Mobile Number");
            return false;


        }
        return true;
    }

    public boolean phoneNumber(){
        String phonepattern = "/^[!0]*[0-9-\\)\\(]+$/";
        String phoneno=mEdtPhone.getText().toString().trim();
        if(phoneno.matches(phonepattern)) {

            return true;
        }
        return false;
    }

    //use to validate proper email ID or is it int the well-form
    public boolean validateEmail() {
        String emailInput, emailPattern;

        emailInput = mEdtEmail.getText().toString().trim();

        emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


        if (!emailInput.matches(emailPattern)) {

            return false;

        }
        return true;
    }

    private void myToast(String message) {

        Toast.makeText(UserProfile.this, message, Toast.LENGTH_SHORT).show();
    }


}