package com.genieiot.gsmarthome;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.icu.text.LocaleDisplayNames;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import Database.DatabaseHandler;
import Fragments.Home;
import Fragments.ProfileFragment;
import Fragments.Rooms;
import Session.Constants;
import Session.MqttlConnection;
import Session.SessionManager;
import Session.Utility;
import de.hdodenhof.circleimageview.CircleImageView;

import static Database.DatabaseHandler.CREATED_BY;
import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.ROOM_TYPE_ID;
import static Database.DatabaseHandler.SCHEDULE_DATETIME;
import static Database.DatabaseHandler.SCHEDULE_ID;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_NUMBER;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;
import static Database.DatabaseHandler.TIME;

import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MY_PREFS_NAME;
import static Session.Constants.NOTINTERNET;
import static Session.Constants.TOPIC_LOCAL;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static Session.Constants.URL_IMAGE;
import static com.genieiot.gsmarthome.R.anim.enter;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,MqttCallback {

    private DrawerLayout drawerLayout;
    Toolbar toolbar;
    private TextView txtSettings, txtShareControl, txtFaq, txtCallUs, txtHelp, txtAbout, txtLogout, txtSchedule;
    LinearLayout nav_header;
    TextView username, usertype;
    private String userType = "";
    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;
    SessionManager sessionManager;
    private CircleImageView imgProfileImage;
    String broker = "", subTopic = "";
    String clientId = "";
    MqttConnectOptions connOpts;
    MqttClient mqqtClient = null;
    MemoryPersistence persistence;
    private String messageType = "";
    SharedPreferences.Editor editor;
    LinearLayout shedulelistBlock;
    Bundle b = new Bundle();
    private FirebaseAnalytics mFirebaseAnalytics;
    public static ArrayList<HashMap<String, String>> switchData;
    MqttConnectOptions mqttConnection;
    Constants conn = new Constants();
    //  private BroadcastReceiver MyReceiver = null;
    String ConnectionValue;
    String restoredText;
    String status = null;
    private static final String LOG_TAG = "CheckNetworkStatus";
    private NetworkChangeReceiver receiver;
    private boolean isConnected = false;
    private TextView networkStatus,networkStatus2;
    String status1 = null;
    public String connectionValue;
    MqttlConnection mqttlConnection;
    ImageView refresh;
    IntentFilter filter;
    public ProgressDialog pDialog;
    Home homefragment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialiseControls();
        setUserImage();
        setShareControl();

        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);

        AppRater appRater = new AppRater();
        appRater.app_launched(MainActivity.this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "id");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "name");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        editor = getApplication().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        /*     MyReceiver = new MyReceiver();*/
        mqttlConnection=new MqttlConnection();

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.hall);
        toolbar.setTitle("Genie SmartHome");

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.refreshbutton);
        toolbar.setOverflowIcon(drawable);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        SharedPreferences prefs = getSharedPreferences("MyPREFERENCES", MODE_PRIVATE);


        setDrawerToggleEvent();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        // registerReceiver(receiver, filter);

        if (sessionManager.getDemoUser().equals("DemoUser"))
        {

            setHome();
        }
        else
        {

            setHome();

        }

        Constants.URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";

        setHome();


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver);
    }

    private void setShareControl() {
        SessionManager sessionManager = new SessionManager(MainActivity.this);
        userType = sessionManager.getUserType();

        if (userType.equals("0")) {
            usertype.setText("(Admin User)");
        }
        if (userType.equals("1")) {
            usertype.setText("(Moderate User)");
        }
        if (userType.equals("2")) {
            usertype.setText("(Guest User)");
        }

    }

    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(message, new IntentFilter("INTERNET_TEST_HUB"));
        registerReceiver(receiver, filter);

        //  CrashManager.register(this);
        //UpdateManager.register(this);
    }

    private MqttClient getMqttConnection(String broker) throws MqttException {
        if (mqqtClient != null) {
            System.out.println("MainActivity reusing connection...");
            return mqqtClient;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new AsynMqttCallbackTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, broker);
            } else {
                new AsynMqttCallbackTask().execute(broker);
            }
            return mqqtClient;
        }
    }

    private class AsynMqttCallbackTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            // setBroker(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (messageType.equals(INTERNET)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    //   new AsyncInternetRoomListTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    //  new AsyncInternetRoomListTask().execute();
                }
            }
        }
    }

    private void setBrokerLocal(String mBroker) {
        try {


            broker= "tcp://192.168.0.119:1883";

            subTopic = "updateStatus1_"+sessionManager.getHomeId();
            broker = mBroker;
            clientId = System.currentTimeMillis() + "";
            persistence = new MemoryPersistence();


            mqqtClient = new MqttClient(broker, clientId, persistence);
            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);

            Log.d("client_id151","broker=="+broker);
            Log.d("client_id151","clientId=="+clientId);
            Log.d("client_id151","persistence=="+persistence);
            Log.d("client_id151","topic=="+subTopic);


            mqqtClient.connect(connOpts);
            Log.d("client_id151","connected");
            mqqtClient.setCallback(this);

            mqqtClient.subscribe(subTopic);
            Log.d("client_id151","subscribe");
            Log.d("Set_Broker_MainActivity", broker + " " + subTopic);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.d("SubScribe Exception", e.toString());
        }
    }


    private void setBrokerInternet(String mBroker) {
        try {


            broker= "tcp://103.12.211.52:1883";

            subTopic = "updateStatus2_"+sessionManager.getHomeId();
            broker = mBroker;
            clientId = System.currentTimeMillis() + "";
            persistence = new MemoryPersistence();


            mqqtClient = new MqttClient(broker, clientId, persistence);
            connOpts = new MqttConnectOptions();
            connOpts.setConnectionTimeout(300);
            connOpts.setCleanSession(true);

            mqqtClient.connect(connOpts);
            mqqtClient.setCallback(this);
            Log.d("connection_val","===="+"internet_all12");
            mqqtClient.subscribe(subTopic);
            Log.d("Set_Broker_MainActivity", broker + " " + subTopic);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.d("SubScribe Exception", e.toString());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.register(this);
        //   unregisterReceiver(MyReceiver);
    }

    private void setUserImage() {

        username.setText(sessionManager.getFirstName() + " " + sessionManager.getLastName());
        String path = sessionManager.getUserImage();
        if (!path.equals("")) {
            String image_url = URL_IMAGE + path;
            Glide.with(MainActivity.this).load(image_url)
                    .error(R.drawable.profile)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imgProfileImage);
        }

    }

    class AsyncSetImageFromServer extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        SessionManager sessionManager = new SessionManager(MainActivity.this);
                        String path = sessionManager.getUserImage();
                        if (!path.equals("")) {
                            Picasso.with(MainActivity.this).load("http://preprod.genieiot.com:65000/share/kjsdnc8s8f8sdfdFDFSwcsds88eud8de8/" + path).placeholder(R.drawable.profile).into(imgProfileImage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            return null;
        }
    }

    private void initialiseControls() {

        sessionManager = new SessionManager(MainActivity.this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtSettings = (TextView) findViewById(R.id.txtSettings);
        txtShareControl = (TextView) findViewById(R.id.txtShareControl);
        txtFaq = (TextView) findViewById(R.id.txtFaq);
        txtCallUs = (TextView) findViewById(R.id.txtCallUs);
        txtHelp = (TextView) findViewById(R.id.txtHelp);
        txtAbout = (TextView) findViewById(R.id.txtAbout);
        txtLogout = (TextView) findViewById(R.id.txtLogout);
        refresh=(ImageView)toolbar.findViewById(R.id.refresh);



        txtSchedule = (TextView) findViewById(R.id.txtSchedule);

        shedulelistBlock=(LinearLayout)findViewById(R.id.shedulelistBlock);
        //shedulelistBlock.setVisibility(View.GONE);

        nav_header = (LinearLayout) findViewById(R.id.nav_header);
        imgProfileImage = (CircleImageView) findViewById(R.id.imgProfileImage);
        username = (TextView) findViewById(R.id.username);
        usertype = (TextView) findViewById(R.id.usertype);
        nav_header.setOnClickListener(this);
        txtSchedule.setOnClickListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setItemIconTintList(null);
        LinearLayout llContactUs = (LinearLayout) findViewById(R.id.llContactUs);
        LinearLayout llFAQ = (LinearLayout) findViewById(R.id.llFAQ);

        if (sessionManager.getDemoUser().equals("DemoUser")) {
            llContactUs.setVisibility(View.GONE);
            llFAQ.setVisibility(View.GONE);
            username.setText("Demo User");
        }
        setListener();
        setToolbarTitle();
    }

    private void setToolbarTitle() {
        toolbar.setTitle("Genie SmartHome");
        /*  toolbar.inflateMenu(R.menu.genie_main);*/

    }

    private void setListener() {
        txtSettings.setOnClickListener(this);
        txtShareControl.setOnClickListener(this);
        // txtFaq.setOnClickListener(this);
        txtCallUs.setOnClickListener(this);
        txtHelp.setOnClickListener(this);
        txtAbout.setOnClickListener(this);
        txtLogout.setOnClickListener(this);
        refresh.setOnClickListener(this);
    }

    private void setHome() {
        Fragment fragment = new Home();
        redirectFragment(fragment);
    }

    private void setDrawerToggleEvent() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);

                setUserImage();

            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        actionBarDrawerToggle.syncState();
    }

    private void redirectFragment(Fragment fragment) {
        if (fragment != null) {

            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);

            fragmentTransaction.commit();
        }
    }


    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        } else {
            Toast.makeText(getBaseContext(), "Press again to exit", Toast.LENGTH_SHORT).show();
        }

        mBackPressed = System.currentTimeMillis();

    }

    @Override
    public void onClick(View v) {
        Fragment fragment = null;
        Intent intent = null;
        switch (v.getId()) {

            case R.id.refresh:
                //    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("refreshRoom"));

                refresh();


                break;
            case R.id.txtSettings:
                intent = new Intent(this, Settings.class);
                startActivity(intent);
                overridePendingTransition(enter, R.anim.exit);
                drawerLayout.closeDrawers();

                break;
            case R.id.txtShareControl:
                // if(userType.equals("0")) {
                intent = new Intent(MainActivity.this, ShareControlUserList.class);
                startActivity(intent);
                overridePendingTransition(enter, R.anim.exit);
                //   }else{
                //       Toast.makeText(this, "You don't have Authority to share the control.", Toast.LENGTH_SHORT).show();
                //   }

                drawerLayout.closeDrawers();
                break;
//            case R.id.txtFaq:
//
//                intent = new Intent(MainActivity.this, Faq.class);
//                startActivity(intent);
//                overridePendingTransition(enter, R.anim.exit);
//                drawerLayout.closeDrawers();
//
//                break;
            case R.id.txtCallUs:

                TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
                    myToast(MainActivity.this, "Device not supported");
                } else {
                    Uri number = Uri.parse("tel:9822197411");
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                    startActivity(callIntent);
                }
                drawerLayout.closeDrawers();
                break;
            case R.id.txtHelp:
                //  intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "support@gsmarthome.com"));
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "contact@mobantica.com"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Help");
                startActivity(intent);
                drawerLayout.closeDrawers();
                break;
            case R.id.txtAbout:
                intent = new Intent(MainActivity.this, AboutUs.class);

                startActivity(intent);
                overridePendingTransition(enter, R.anim.exit);
                drawerLayout.closeDrawers();
                break;
            case R.id.txtLogout:
                final Dialog dialog = new Dialog(MainActivity.this);
                // Include dialog.xml file
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.exit);
                TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
                TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
                txtOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        sessionManager.logoutUser();
                        deleteAll();
                        Intent intent = new Intent(MainActivity.this, FirstPage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        drawerLayout.closeDrawers();
                    }
                });

                txtCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                });
                dialog.show();
                break;

            case R.id.nav_header:
                SessionManager sessionManager1 = new SessionManager(MainActivity.this);
                if (sessionManager1.getDemoUser().equals("DemoUser")) {
                    intent = new Intent(MainActivity.this, UserProfile.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(MainActivity.this, UserProfileNew.class);
                    startActivity(intent);
                }

                overridePendingTransition(enter, R.anim.exit);
                drawerLayout.closeDrawers();
                break;
            case R.id.txtSchedule:
                Intent iSchedule = new Intent(this, ScheduleListActivity.class);
                startActivity(iSchedule);
                overridePendingTransition(enter, R.anim.exit);
                drawerLayout.closeDrawers();
                break;
        }

    }

    private void refresh()
    {
        new AsyncRoomListTask().execute();
    }



    private void deleteAll() {
        DatabaseHandler db = new DatabaseHandler(this);
        db.deleteRoomData();
        db.deleteSwitchData();
        db.deleteNotificationData();
        db.deleteRecentData();
        db.deleteROOM_TYPE();
        db.deleteSWITCH_TYPE();
        db.deleteModeType();
        db.deleteScheduleList();
        sessionManager.setCountVal("true");
        sessionManager.setCountValSwitch("true");
        editor.putString("room_key", "true");
        editor.putString("switch_key", "true");
        editor.apply();
    }

    public static void myToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static boolean isInternetAvailable(Context context) {

        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    public void connectionLost(Throwable throwable) {
        try {
            Log.d("MainActivity -->", "Connection Lost");
            Thread.sleep(4000);
            getMqttConnection(broker);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        // Log.select_menu("MainActivity ","MessArrived "+s);
        //   parseMqttCallBack(mqttMessage.toString());

        Log.d("Update_status_log15", "msg== " + mqttMessage);
        Log.d("Update_status_log15", "topic== " + s);

        try
        {
            DatabaseHandler db = new DatabaseHandler(MainActivity.this);
            String temp = mqttMessage.toString();

            if (temp.charAt(0) == '$') {

                String switchNumber = String.valueOf(temp.charAt(2));
                String swtchStatus = String.valueOf(temp.charAt(3));
                String dimmerval = String.valueOf(temp.charAt(4)) + String.valueOf(temp.charAt(5));
                String panel=temp.charAt(7)+""+temp.charAt(8)+""+temp.charAt(9)+""+temp.charAt(10)+""+temp.charAt(11)+""+temp.charAt(12);

                Log.d("switch_stat12", "msg=" + temp);
                Log.d("switch_stat12", "switchNumber=" + switchNumber);
                Log.d("switch_stat12", "swtchStatus=" + swtchStatus);
                Log.d("switch_stat12", "dimmerval=" + dimmerval);
                Log.d("switch_stat12", "panel=" + panel);


                db.updateSwitchStatusMqtt(switchNumber, swtchStatus, dimmerval,panel);

                switchData = db.getSwitchLog(switchNumber,panel);
                Log.d("seitch_data_after_log","==="+switchData);




                Calendar calander;
                String time;
                calander = Calendar.getInstance();

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                time= simpleDateFormat.format(calander.getTime());

                HashMap<String, String> switcch = new HashMap<String, String>();

                switcch.put(DIMMER_STATUS, switchData.get(0).get(DIMMER_STATUS));
                switcch.put(PANEL_NAME,switchData.get(0).get(PANEL_NAME));
                switcch.put(SWITCH_TYPE_ID, switchData.get(0).get(SWITCH_TYPE_ID));
                switcch.put(SWITCH_NAME, switchData.get(0).get(SWITCH_NAME));
                switcch.put(SWITCH_NUMBER,switchData.get(0).get(SWITCH_NUMBER));
                switcch.put(DIMMER_VALUE, switchData.get(0).get(DIMMER_VALUE));

                switcch.put(ROOM_ID, switchData.get(0).get(ROOM_ID));
                switcch.put(SWITCH_STATUS,switchData.get(0).get(SWITCH_STATUS));

                switcch.put(SWITCH_ID, switchData.get(0).get(SWITCH_ID));
                switcch.put(ROOM_NAME,switchData.get(0).get(ROOM_NAME));
                switcch.put(SWITCH_IMAGE_ID,switchData.get(0).get(SWITCH_IMAGE_ID));
                switcch.put(TIME,time);


                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Date date = new Date();
                String formattedDate=dateFormat.format(date);
                switcch.put(CREATED_BY,formattedDate);

                db.insertSwitchRecentlog(switcch);



                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend1"));
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend2"));

            } else {
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    private void parseMqttCallBack(String response) {
        DatabaseHandler db = new DatabaseHandler(MainActivity.this);
        Log.d("mqtt_callback-->", "MqttMessage===" + response);

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            Date date = new Date();
            String formattedDate = dateFormat.format(date);
            final JSONObject jObj = new JSONObject(response);

            if (jObj.getString("status").equals("Switch_ON_OFF")) {
                //  db.updateSwitchStatusMqtt(jObj.getString("switchid"), jObj.getString("switchstatus"), jObj.getString("dimmervalue"));
                db.updateRecentSwitchStatus(jObj.getString("switchid"), jObj.getString("switchstatus"), jObj.getString("dimmervalue"));

            } else if (jObj.getString("status").equals("Switch_Name_update")) {
                db.updateSwitchNameMqtt(jObj.getString("switchid"), jObj.getString("switchname"), jObj.getString("switchimageId"));

            } else if (jObj.getString("status").equals("Switch_Hide_Unhide")) {
                db.updateHideMqtt(jObj.getString("switchid"), jObj.getString("hidestatus"), jObj.getString("switchstatus"));

            } else if (jObj.getString("status").equals("Switch_Lock_Unlock")) {
                db.updateLockMqtt(jObj.getString("switchid"), jObj.getString("lockstatus"));

            } else if (jObj.getString("status").equals("Room_Name_update")) {
                db.updateRoomNameMqtt(jObj.getString("RoomId"), jObj.getString("RoomName"), jObj.getString("roomimageId"));

            } else if (jObj.getString("status").equals("TurnOffAll")) {

                if (messageType.equals(INTERNET) || !jObj.getString("userId").equals(sessionManager.getUSERID())) {
                    Log.d("Callback", "parseMqttCallBack: turnOff");
                    String switchIds = jObj.getString("switchid");
                    if (!switchIds.isEmpty()) {
                        String[] arr = switchIds.split(":");
                        for (int i = 0; i < arr.length; i++) {//Log.d("switchId", arr[i]);
                            db.updateSwitchStatusAllMqtt(arr[i], jObj.getString("switchstatus"));
                        }
                    }

                    if (!jObj.getString("profileSwitchid").isEmpty()) {
                        String[] arrOFF = jObj.getString("profileSwitchid").split(":");
                        for (int m = 0; m < arrOFF.length; m++) {
                            db.updateSwitchStatusAllMqtt(arrOFF[m], "0");
                        }
                    }
                }

            } else if (jObj.getString("status").equals("Schedule_Switch_update")) {
                if (messageType.equals(INTERNET) && jObj.getString("userId").equals(sessionManager.getUSERID())) {
                    HashMap<String, String> mMap = createHashMap(jObj);
                    Log.d("Schedule Update", "" + mMap);
                    db.updateSwitchSchedule(mMap.get(SCHEDULE_ID), mMap.get(SCHEDULE_DATETIME), mMap.get(SWITCH_STATUS), mMap.get(TIME), mMap.get(DIMMER_VALUE));
                }
            } else if (jObj.getString("status").equals("Schedule_Switch")) {
                if (messageType.equals(INTERNET) && jObj.getString("userId").equals(sessionManager.getUSERID())) {
                    HashMap<String, String> mMap = createHashMap(jObj);
                    Log.d("Schedule Data", "" + mMap);
                    db.insertSchedulerInfo(mMap);
                }
            } else if (jObj.getString("status").equals("Schedule_Switch_delete")) {
                if (messageType.equals(INTERNET) && jObj.getString("userId").equals(sessionManager.getUSERID())) {
                    db.deleteSchedule(jObj.getString("schedulerid"));
                }
            } else if (jObj.getString("status").equals("changeStatusByRoom")) {//&& !jObj.getString("userId").equals(sessionManager.getUSERID())
                db.changeRoomSwitchStatus(jObj.getString("RoomId"), jObj.getString("switchstatus"));
                db.updateDimmerSwitch(jObj.getString("RoomId"), jObj.getString("switchstatus"));

            } else if (jObj.getString("status").equals("Room_Switch_List")) {

                //Toast.makeText(this, ""+response, Toast.LENGTH_SHORT).show();
                if (jObj.getString("userId").equals(sessionManager.getUSERID()) && messageType.equals(INTERNET)) {
                    JSONArray jswitchAndRoomList = new JSONArray(jObj.getString("switchAndRoomList"));
                    for (int i = 0; i < jswitchAndRoomList.length(); i++) {
                        JSONObject jRoomObj = jswitchAndRoomList.getJSONObject(i);
                        HashMap<String, String> mMapRoom = new HashMap<>();
                        String mRoomID = jRoomObj.getString("roomId");
                        String mRoomName = jRoomObj.getString("roomName");
                        mMapRoom.put(ROOM_ID, mRoomID);
                        mMapRoom.put(ROOM_IMAGE_TYPE, jRoomObj.getString("roomImageId"));
                        mMapRoom.put(ROOM_TYPE_ID, "1");
                        mMapRoom.put(ROOM_NAME, mRoomName);

                        db.insertRoomNew(mMapRoom);

                        JSONArray jArrSwitchList = new JSONArray(jRoomObj.getString("switchList"));
                        for (int j = 0; j < jArrSwitchList.length(); j++) {
                            JSONObject jSwitchObj = jArrSwitchList.getJSONObject(j);
                            HashMap<String, String> mMapSwitch = new HashMap<>();
                            mMapSwitch.put(SWITCH_ID, jSwitchObj.getString("id"));
                            mMapSwitch.put(SWITCH_IMAGE_ID, jSwitchObj.getString("switchImageId"));
                            mMapSwitch.put(HIDE, jSwitchObj.getString("hideStatus"));
                            mMapSwitch.put(SWITCH_TYPE_ID, jSwitchObj.getString("switchType"));
                            mMapSwitch.put(LOCK, jSwitchObj.getString("lockStatus"));
                            mMapSwitch.put(SWITCH_NAME, jSwitchObj.getString("switchName"));
                            mMapSwitch.put(SWITCH_STATUS, jSwitchObj.getString("switchStatus"));
                            mMapSwitch.put(DIMMER_STATUS, jSwitchObj.getString("dimmerStatus"));
                            mMapSwitch.put(DIMMER_VALUE, jSwitchObj.getString("dimmerValue"));
                            mMapSwitch.put(ROOM_ID, mRoomID);
                            mMapSwitch.put(ROOM_NAME, mRoomName);

                            db.insertSwitchNew(mMapSwitch);
                        }

                    }

                }
            }


            if (jObj.has("activityid")) {
                db.insertNotificationMessage(jObj.getString("activityid"), jObj.getString("message"), formattedDate, jObj.getString("Time"), jObj.getString("RoomName"), jObj.getString("userimage"));
            }

            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("NotificationSend"));

            if (jObj.getString("userId").equals("") || !jObj.getString("userId").equals(sessionManager.getUSERID()) || messageType.equals(INTERNET)) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent("MqttCallBack"));
            }


//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        if(!sessionManager.getUSERID().equals(jObj.getString("userId"))) {
//                            Utility.showToast(MainActivity.this, jObj.getString("message"), jObj.getString("userimage"));
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//
//                }
//            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private HashMap<String, String> createHashMap(JSONObject jObj) {
        HashMap<String, String> mMap = new HashMap<>();
        try {
            mMap.put(SWITCH_ID, jObj.getString("switchid"));
            mMap.put(SWITCH_NAME, jObj.getString("switchname"));
            mMap.put(SWITCH_STATUS, jObj.getString("switchstatus"));
            String[] mString = jObj.getString("scheduleDateTime").split(" ");
            mMap.put(SCHEDULE_DATETIME, mString[0]);
            mMap.put(TIME, mString[1].substring(0, 5));
            mMap.put(IP, "");
            mMap.put(ROOM_NAME, jObj.getString("RoomName"));
            mMap.put(SCHEDULE_ID, jObj.getString("schedulerid"));
            mMap.put(DIMMER_VALUE, jObj.getString("dimmervalue"));
            mMap.put(DIMMER_STATUS, jObj.getString("dimmerstatus"));
        } catch (Exception e) {
        }

        return mMap;
    }

    private class AsyncTaskCheckHub extends AsyncTask<Void, Void, String> {
        ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Connecting to Genie Pyramid...");
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String response = "";
            Constants req = new Constants();
            try {

                //   String appUrl="http://"+sessionManager.getRouterIP()+":8080/smart_home_local/home/verifygeniehub";

                String appUrl = "http://192.168.0.2:6060/home/verifygeniehub";
                JSONObject json = new JSONObject();
                json.put("homeId", sessionManager.getHomeId());

                Log.d("MainActivity-->", "Homeid==" + sessionManager.getHomeId());


                Log.d("mainactivity_response1", "appurl==" + appUrl);
                Log.d("mainactivity_response1", "json==" + json);

                // response=req.doPostRequest(appUrl,json+"");


                response = req.doGetRequest(appUrl);

            } catch (Exception e) {
                Log.d("responese_error", "= " + e.getMessage());
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("mainactivity_response1", "response==" + result);


            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }

            if (result != null && !result.isEmpty()) {
                parseHubResultData(result);
                Log.d("MainActivity-->1", "" + result);
            } else {
                if (isInternetAvailable(MainActivity.this)) {
                    try {

                        // getMqttConnection("tcp://geniewish.genieiot.com:1883");   //ORIGINAL place

                        messageType = INTERNET;
                        URL_GENIE = URL_GENIE_AWS;

                        //subTopic="refreshIntBackGenieHomeId_"+sessionManager.getHomeId();

                        subTopic = "updateStatus2";
                        //  getMqttConnection("tcp://gsmarthome.genieiot.in:1883");
                        getMqttConnection("tcp://192.168.0.2:1883");
                        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("INTERNET_TEST").putExtra("INTERNET_TEST", "1"));
                        Log.d("check_url_type", "internet");
                    } catch (Exception e) {
                        Log.d("error", "" + e.getMessage());
                    }
                    setHome();
                } else {
                    LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("INTERNET_TEST").putExtra("INTERNET_TEST", "0"));
                    Toast.makeText(MainActivity.this, "Please check network connection.", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }

    private void parseHubResultData(String result) {
        try {
            JSONObject jObj = new JSONObject(result);

            if (jObj.getString("status").equals("SUCCESS")) {
                //subTopic="refreshBack";
                // subTopic = "updateStatus1_"+sessionManager.getHomeId();
                subTopic = "updateStatus1_68";

                //original code start

                messageType = LOCAL_HUB;
                URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";
                getMqttConnection("tcp://" + sessionManager.getRouterIP() + ":1883");
                LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("INTERNET_TEST").putExtra("INTERNET_TEST", "1"));
                Log.d("check_url_type", "Local");

                //original code end
//
                setHome();
            }

        } catch (Exception e) {
            Log.d("error_is", "" + e.getMessage());

        }

    }


    class AsyncInternetRoomListTask extends AsyncTask<Void, Void, String> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Retrieving your appliances status...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(Void... params) {
            Constants request = new Constants();
            String mResponse = null;
            try {
                JSONObject jMain = new JSONObject();
                jMain.put("userId", sessionManager.getUSERID());
                jMain.put("messageFrom", messageType);
                Log.d("Internet room request ", jMain + "");

                mResponse = request.doPostRequest(URL_GENIE_AWS + "/switch/getswitchbyuser", jMain + "", sessionManager.getSecurityToken());
                Log.d("current_status_retrive", "===" + mResponse);

                try {
                    Thread.sleep(4000);
                } catch (Exception e) {
                }

                Log.d("Internet room/switch-->", "" + mResponse);
            } catch (Exception e) {
            }
            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("NotificationSend"));
            }

        }
    }

    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra("INTERNET_TEST")){

                setInternetSataus(intent.getStringExtra("INTERNET_TEST"));
                /*Intent intent1 = new Intent(MainActivity.this,MqqtConnectionService.class);
                intent1.putExtra("INTERNET_TEST",intent.getStringExtra("INTERNET_TEST"));
                startService(intent1);*/
            }
        }
    };


    public void  chkStatus() {
        final ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting ()) {

           /* if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                new AsyncTaskCheckInternet().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                new AsyncTaskCheckInternet().execute();*/
            String result = null;
            try {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    result = new AsyncTaskCheckInternet().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get(2000, java.util.concurrent.TimeUnit.MILLISECONDS);

                } else {
                    result = new AsyncTaskCheckInternet().execute().get(2000, java.util.concurrent.TimeUnit.MILLISECONDS);
                }

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }catch (TimeoutException e) {
                e.printStackTrace();
            }
            if (result != null) {
                Log.d("Get_room_response : ", result + "");
                try {
                    JSONObject response = new JSONObject(result);
                    String status = response.getString("status");
                    String results = response.getString("response");
                    String statusCode = response.getString("statusCode");

                    if (status.equals("SUCCESS")) {
                        if (statusCode.equalsIgnoreCase("0")){
                            connectionValue = INTERNET;
                        }else {
                            connectionValue = LOCAL_HUB;
                        }
                    } else {
                        connectionValue = INTERNET;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                connectionValue = INTERNET;
            }

        } else if (mobile.isConnectedOrConnecting()) {

            connectionValue = INTERNET;
          //  return connectionValue;
        } else {
            connectionValue = NOTINTERNET;

          //  return connectionValue;
        }
        connectToMQTT();
    }

    private void setInternetSataus(String internet_test) {
        if(getApplicationContext()!=null){
            if(internet_test.equals("1")){

                chkStatus();

            }  else if(internet_test.equals("2")){
                Log.d("connection_val","===="+"internet_all");
                subTopic = "updateStatus2_"+sessionManager.getHomeId();
                // mqttlConnection.getMqttConnectionInternet().subscribe(subTopic);
                setBrokerInternet("tcp://103.12.211.52:1883");
                Log.d("connection_val","===="+"internet_all1");

            }
            else if(internet_test.equals("0"))
            {
                Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show();
                //   Toast.makeText(getApplicationContext(), "ok3", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void connectToMQTT(){
        // new AsyncTaskCheckRouter1().execute();
        //   if(connectionValue == LOCAL_HUB || true)
        homefragment = (Home)getSupportFragmentManager().findFragmentById(R.id.container_body);
        if(homefragment != null){
            homefragment.tvInternetMsg.setText(""+connectionValue);
        }
        if(connectionValue == LOCAL_HUB )
        {

            Log.d("connection_val","===="+"wifi1");
            subTopic = "updateStatus1_"+sessionManager.getHomeId();
            Log.d("connection_val","===="+"wifi2");
            Log.d("connection_val","topic"+subTopic);
            //   mqttlConnection.getMqttConnectionLocal();
            setBrokerLocal("tcp://192.168.0.119:1883");
            Log.d("connection_val","===="+"wifi3");
        }
        else
        {

            subTopic = "updateStatus2_"+sessionManager.getHomeId();
            Log.d("connection_val","===="+"internet1");
            //  mqttlConnection.getMqttConnectionInternet().subscribe(subTopic);
            setBrokerInternet("tcp://103.12.211.52:1883");
            Log.d("connection_val","===="+"internet2");
        }
    }


    public class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {

            Log.v("NetworkChangeReceiver", ".....................Receieved notification about network status");
            isNetworkAvailable(context);
            getConnectivityStatusString(context);

        }




        public  String getConnectivityStatusString(Context context)
        {

            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    status1 = "1";
//                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("INTERNET_TEST_HUB").putExtra("INTERNET_TEST", "1"));
                    setInternetSataus(status1);
                    return status1;
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    status1 = "2";
//                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("INTERNET_TEST_HUB").putExtra("INTERNET_TEST", "2"));
                    setInternetSataus(status1);
                    return status1;
                }
            } else {
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("INTERNET_TEST_HUB").putExtra("INTERNET_TEST", "0"));
                status = "No internet is available";
                return status1;
            }


            return status;
        }



        private boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            if(!isConnected){
                                Log.v(LOG_TAG, "Now you are connected to Internet!");
                                // networkStatus.setText("Now you are connected to Internet!");
                                isConnected = true;

                            }
                            return true;
                        }
                    }
                }
            }
            Log.v(LOG_TAG, "You are not connected to Internet!");
            //    networkStatus.setText("You are not connected to Internet!");
            isConnected = false;
            return false;
        }
    }


 /*   public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu, this adds items to the action bar if it is present.
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.refreshbutton);
        toolbar.setOverflowIcon(drawable);
        getMenuInflater().inflate(R.menu.refresh_page, menu);
        return true;
    }*/

    private class AsyncTaskCheckRouter1 extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            InetAddress routername = null;

            Log.d("mqtt_checking","checked");
            try {
                routername = InetAddress.getByName("192.168.0.119");
                if (routername.isReachable(300)) //5000
                {
                    Log.d("connection_val","Router is   reachable");
                    connectionValue = LOCAL_HUB;
                }
                else
                {
                    Log.d("connection_val","Router is not  reachable");
                    connectionValue = INTERNET;
                }

            } catch (java.io.IOException e) {
                Log.d("network_checking12","Router EXCEPTION"+e.getMessage());
                e.printStackTrace();
            }

            Log.d("connection_val_2",connectionValue);
            return connectionValue;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            connectToMQTT();
        }
    }

    private class AsyncTaskCheckInternet extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {
            Constants request=new Constants();
            String mResponse=null;
            try {

                JSONObject jMain=new JSONObject();
                jMain.put("homeId",sessionManager.getHomeId());

                mResponse=request.doPostInternetRequest("http://192.168.0.119:8080/smart_home_local/home/verifygohub ",jMain+"");
                Log.d("Get_room_response","response val ="+mResponse);


            }catch(Exception e){

            }

            return mResponse;
        }

    }

    public class AsyncRoomListTask extends AsyncTask<Void,Void,String>{

        public ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(Void... params) {
            Constants request=new Constants();
            String mResponse=null;
            try {

                JSONObject jMain=new JSONObject();

                String token =sessionManager.getSecurityToken();
                Log.d("Room Url",sessionManager.getSecurityToken());
                jMain.put("userId",sessionManager.getUSERID());
                jMain.put("messageFrom",messageType);   //INTERNET //messageType

                mResponse=request.doPostRoomRequest(URL_GENIE_AWS+"/room/getlistbyuser",jMain+"",sessionManager.getSecurityToken());
                String url =URL_GENIE_AWS+"/room/getlistbyuser";                   //url_genie original
                Log.d("Get_room_response","url val ="+url);
                Log.d("Get_room_response","json val ="+jMain);
                Log.d("Get_room_response","response val ="+mResponse);

                Log.d("room_api_call","api call");


            }catch(Exception e){}

            return mResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            pDialog.dismiss();
            if(result!=null){
                Log.d("Get_room_response : ",result+"");
                try {

                    //  LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(message, new IntentFilter("NotificationSend1"));
                    JSONObject response=new JSONObject(result);
                    String status = response.getString("status");
                    String results = response.getString("result");
                    JSONArray resultArray = new JSONArray(results);

                    if (status.equals("SUCCESS")) {

                        createListForRoom(resultArray);
                        getApplicationContext().startService(new Intent(getApplicationContext(), GetSwitchByRoomService.class).putExtra("connectionValue",connectionValue));
                        //  Toast.makeText(MainActivity.this, "refresh", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(getApplicationContext(), ""+response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }else{
            }

            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent("NotificationSend1"));
            //    setRoomAdapter();
        }
    }

    public  void createListForRoom(JSONArray resultArray) {
        ArrayList<HashMap<String, String>> roomDatas;
        DatabaseHandler db =new DatabaseHandler(getApplicationContext());
        roomDatas=new ArrayList<>();
        roomDatas.clear();
        for (int i = 0; i < resultArray.length(); i++) {

            HashMap<String, String> roomType = new HashMap<String, String>();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(resultArray.get(i).toString());

                roomType.put(ROOM_ID, jsonObject.getString("id"));
                roomType.put(ROOM_NAME, jsonObject.getString("roomName"));
                roomType.put(ROOM_TYPE_ID, jsonObject.getString("roomType"));
                roomType.put(ROOM_IMAGE_TYPE, jsonObject.getString("roomImage")); //roomImageId
                roomDatas.add(roomType);
                db.insertRoomNew(roomType);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private  class AsyncTaskCheckRouter extends AsyncTask<Void, Void, String> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Checking Hub..");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected String doInBackground(Void... voids) {

            InetAddress routername = null;
            String type = null;
            Log.d("mqtt_checking","checked");
            try {
                routername = InetAddress.getByName(sessionManager.getRouterIP());
                if (routername.isReachable(300))
                {
                    Log.d("network_checking","Router is   reachable");
                    type=LOCAL_HUB;
                }
                else
                {
                    if (isInternetAvailable(MainActivity.this))
                    {
                        Log.d("network_checking","Router is not  reachable");
                        type=INTERNET;
                    }else
                    {
                        type=NOTINTERNET;
                    }


                }

            } catch (java.io.IOException e) {
                e.printStackTrace();
            }

            return type;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();

            }

            if(result.equals(LOCAL_HUB))
            {

                subTopic = "updateStatus1_"+sessionManager.getHomeId();
                messageType = LOCAL_HUB;
                URL_GENIE = "http://" + sessionManager.getRouterIP() + ":8080/smart_home_local";
                try {
                    getMqttConnection("tcp://" + sessionManager.getRouterIP() + ":1883");

                    //     conn.mqttConnectionLocal().subscribe(subTopic);


                } catch (MqttException e) {
                    e.printStackTrace();
                }
                LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("INTERNET_TEST").putExtra("INTERNET_TEST", "1"));
                Log.d("check_url_type", "Local= "+messageType);

                setHome();
            }
            else
            {

                if (isInternetAvailable(MainActivity.this)) {
                    try {

                        // getMqttConnection("tcp://geniewish.genieiot.com:1883");   //ORIGINAL place
                        //subTopic="refreshIntBackGenieHomeId_"+sessionManager.getHomeId();

                        messageType = INTERNET;
                        URL_GENIE = URL_GENIE_AWS;
                        subTopic = "updateStatus2_"+sessionManager.getHomeId();

                        getMqttConnection("tcp://103.12.211.52:1883");
                        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("INTERNET_TEST").putExtra("INTERNET_TEST", "1"));
                        Log.d("check_url_type", "internet");

                    } catch (Exception e) {
                        Log.d("error", "" + e.getMessage());
                    }

                    setHome();
                } else {
                    LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent("INTERNET_TEST").putExtra("INTERNET_TEST", "0"));
                    Toast.makeText(MainActivity.this, "Please check network connection.", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }
}
