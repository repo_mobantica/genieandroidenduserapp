package com.genieiot.gsmarthome;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import Session.Constants;

import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.MainActivity.isInternetAvailable;

/**
 * Created by Genie IoT on 9/16/2016.
 */
public class Register extends Activity implements View.OnClickListener {

    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtEmail;
    private EditText edtPassword;

    private EditText edtPhone;
    EditText edtBirthDate;
    private Button btnRegister;
    private ProgressDialog pDialog;
    ImageView calender;

    // These tags will be used to cancel the requests
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    final private static int DIALOG_LOGIN = 1;
    private String mOTP;
    private String mEmailId;
    String url1,url2;
    private int mYear, mMonth, mDay, mHour, mMinute;
    int year,month,day;
    static  final int DILOG_ID =0;
    DatePickerDialog datePickerDialog;

    int dayOfMonth;
    Calendar calendar;
    String dobValue;


    Calendar myCalendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        initialseControls();

        edtBirthDate = (EditText) findViewById(R.id.edtBirthDate);
       // calender=(ImageView)findViewById(R.id.calender);

        edtBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                datePickerDialog = new DatePickerDialog(Register.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day)
                            {
                                edtBirthDate.setText(day + "/" + (month + 1) + "/" + year);
                                edtBirthDate.setError(null);

                                dobValue=String.valueOf(day).concat(String.valueOf(month+1)).concat(String.valueOf(year));
                                Log.d("Dob_value","=="+dobValue);
                            }
                        }, year, month, dayOfMonth);
             //   datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();


            }
        });

        }





    private void initialseControls() {

        edtFirstName = (EditText) findViewById(R.id.edtFirstname);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPhone = (EditText) findViewById(R.id.edtPhone);

        edtBirthDate = (EditText) findViewById(R.id.edtBirthDate);


        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        btnRegister.setOnClickListener(this);





    }

    @Override
    public void onClick(View v) {

        if (isFieldSubmtted()) {
            if (isInternetAvailable(Register.this)) {
                //registerUser();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                new AsyncRegisterTask().execute();

            } else {
                myToast(MESSAGE_INTERNET_CONNECTION);
            }
        }

    }



    private boolean isFieldSubmtted() {


        if(edtFirstName.getText().toString().length() > 0 && edtLastName.getText().toString().length() > 0 && edtEmail.getText().toString().length() > 0 &&
                edtPhone.getText().toString().length() > 0 && validateEmail() && validatePhone() && edtPassword.getText().toString().length() > 0 &&
                edtBirthDate.getText().toString().length() > 0)

        {
            return true;
        }
        else
        {


            if (edtFirstName.getText().toString().length() < 1)
            {
                edtFirstName.setError("Enter First Name");
            }
            if (edtLastName.getText().toString().length() < 1) {

                edtLastName.setError("Enter Last Name");
            }
            if (edtEmail.getText().toString().length() < 1) {

                edtEmail.setError("Enter Valid Email");

            }
            if (!validateEmail()) {

                edtEmail.setError("Enter Valid Email");

            }
            if (edtPhone.getText().toString().length() < 10)
            {
                //  myToast(getResources().getString(R.string.MSG_VALID_PHONE));
                edtPhone.setError("Enter Valid Number");
                }
            if(!validatePhone())
            {
                edtPhone.setError("Phone No Invalid");

            }
            if (edtPassword.getText().toString().length() < 6)
            {
                edtPassword.setError("Password length should be 6");
              }
            if(!validatePassword())
            {
                edtPassword.setError("Password Contain Space");
                return false;
            }
            if (edtBirthDate.getText().toString().length() < 1)
            {
                edtBirthDate.setError("Enter Date Of Birth");
            }


            return false;
        }



     /*   if (edtFirstName.getText().toString().length() < 1) {

            edtFirstName.setError("Enter First Name");
            return false;
        } else if (edtLastName.getText().toString().length() < 1) {

            edtLastName.setError("Enter Last Name");
            return false;
        } else if (edtEmail.getText().toString().length() < 1) {

            edtEmail.setError("Enter Valid Email");
            return false;
        } else if (!validateEmail()) {

            edtEmail.setError("Enter Valid Email");
            return false;
        } else if (edtPhone.getText().toString().length() < 10) {
            //  myToast(getResources().getString(R.string.MSG_VALID_PHONE));
            edtPhone.setError("Enter Valid Number");
            return false;
        } else if(!validatePhone())
        {
            edtPhone.setError("Phone No Invalid");
            return false;
        } else if (edtPassword.getText().toString().length() < 6) {

            edtPassword.setError("Password length should be 6");
            return false;
        } else if(!validatePassword())
        {
            edtPassword.setError("Password Contain Space");
            return false;
        }
        else if (edtBirthDate.getText().toString().length() < 1) {

            edtBirthDate.setError("Enter Date Of Birth");
            return false;
        }


        return true;*/
    }

    private void myToast(String message) {
        Toast.makeText(Register.this, message, Toast.LENGTH_SHORT).show();
    }

    public boolean validateEmail() {
        String emailInput, emailPattern;

        emailInput = edtEmail.getText().toString().trim();

        emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


        if (!emailInput.matches(emailPattern)) {

            return false;

        }

        return true;
    }

    public boolean validatePassword() {
        String password;

        password = edtPassword.getText().toString();


        if (password.contains(" "))
        {
            return false;
        }

        return true;
    }

    public boolean validatePhone() {
        String phone;

        phone = edtPhone.getText().toString();


        if (phone.equals("0000000000"))
        {
            return false;
        }

        return true;
    }

    class AsyncRegisterTask extends AsyncTask<Void,Void,String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Register.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String strResult="";
            Constants req=new Constants();
            try {
                String mJSONBODY = createJSONBody();

                 url1 = URL_GENIE_AWS + "/user";

                Log.d("reg_url", "url== " + url1);
                Log.d("reg_url", "url== " + mJSONBODY);

                strResult = req.doPostRequest(url1, mJSONBODY);



            }catch(Exception e){}
            return strResult;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null){
                pDialog.cancel();
            }
            Log.d("reg_url","url=  "+url2);
            Log.d("reg_url","result==  "+result);
            HashMap<String,String> map=createMapData();
            if (result != null) {
                try {

                    JSONObject response=new JSONObject(result);
                    String msg = response.getString("msg");
                    if (response.getString("status").equals("SUCCESS")) {

                   //     new AsyncRegisterTask().execute();

                        Intent intent = new Intent(Register.this, ResendOTP.class);
                        intent.putExtra("data",map);
                        startActivity(intent);
                        //finish();

                    }
                    else
                    {

                        if(msg.equals("Email Already Present And Verified"))
                        {
                            Toast.makeText(Register.this, getResources().getString(R.string.MSG_REG_MAIL), Toast.LENGTH_LONG).show();

                        }
                        else if(msg.equals("Email Already Present But Not Verified"))
                        {
                            Intent intent = new Intent(Register.this, ResendOTP.class);
                            intent.putExtra("data",map);
                            startActivity(intent);
                            //finish();
                            //createDeviceIdDialog();
                        }else{
                           myToast(response.getString("msg"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{
                Toast.makeText(Register.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private HashMap<String,String> createMapData() {

       // jMain.put("birthDate",dobValue);
        final Map<String, String> jsonParams = new HashMap<String, String>();
        mEmailId=edtEmail.getText().toString();
        jsonParams.put("firstName", edtFirstName.getText().toString());
        jsonParams.put("lastName", edtLastName.getText().toString());
        jsonParams.put("email",mEmailId);
        jsonParams.put("phoneNumber", edtPhone.getText().toString());
        jsonParams.put("password", edtPassword.getText().toString());
        jsonParams.put("userType", "0");
        jsonParams.put("birthDate",dobValue);
        HashMap<String,String> map= (HashMap<String, String>) jsonParams;

        return map;

    }

    private String createJSONBody() {
        JSONObject jMain=new JSONObject();
        mEmailId=edtEmail.getText().toString();

        try {
            jMain.put("firstName", edtFirstName.getText().toString());
            jMain.put("lastName",  edtLastName.getText().toString());
            jMain.put("email",edtEmail.getText().toString() );
            jMain.put("phoneNumber",  edtPhone.getText().toString());
            jMain.put("password",edtPassword.getText().toString());
            jMain.put("userType","0");
            jMain.put("birthDate",dobValue);

            Log.d("password_value","=="+edtPassword.getText().toString());
        }catch(Exception e){
        }
        return jMain+"";

    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }

}
