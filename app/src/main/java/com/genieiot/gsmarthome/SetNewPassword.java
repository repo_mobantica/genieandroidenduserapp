package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import net.hockeyapp.android.CrashManager;

import org.json.JSONObject;

import Session.Constants;

import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 25/10/16.
 */
public class SetNewPassword extends Activity implements View.OnClickListener{
    private Button btnConfirm;
    private EditText edtNewPassword,edtConfirmPassword;
    private String mOTP="",mEMAIL="",mUSERID="",x_AUTH_TOKEN="";
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req";
    private String mNewPassword="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setnewpassword);
        initControl();
    }

    private void initControl() {

        if(getIntent().hasExtra("x_AUTH_TOKEN") && getIntent().hasExtra("USERID")) {
            mUSERID=getIntent().getStringExtra("USERID");
            x_AUTH_TOKEN=getIntent().getStringExtra("x_AUTH_TOKEN");
        }

        edtNewPassword= (EditText) findViewById(R.id.edtNewPassword);
        edtConfirmPassword= (EditText) findViewById(R.id.edtConfirmPassword);
        btnConfirm= (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnConfirm:
                onClickOfConfirm();
                break;
        }
    }


    private void onClickOfConfirm() {

        if(edtNewPassword.getText().toString().trim().isEmpty() || edtConfirmPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Please enter all Details.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!edtNewPassword.getText().toString().trim().equals(edtConfirmPassword.getText().toString().trim())){
            Toast.makeText(this,getResources().getString(R.string.MSG_CONFIRM_MATCH), Toast.LENGTH_SHORT).show();
            return;
        }
        if(edtNewPassword.getText().toString().length()< 6) {
            myToast(getResources().getString(R.string.MSG_VAILD_PASS));
            return;
        }

        mNewPassword=edtNewPassword.getText().toString();

        new UpdatePasswordOTP().execute();

    }

    class UpdatePasswordOTP extends AsyncTask<Void,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(SetNewPassword.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            Constants net=new Constants();
            String res="";
            try{
                JSONObject jObj=new JSONObject();
                jObj.put("userId",mUSERID);
                jObj.put("newPassword",mNewPassword);

                res=net.doPostRequest(URL_GENIE_AWS + "/user/updatepasswordOTP",jObj.toString(),x_AUTH_TOKEN);

            }catch (Exception e){}
            return res;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null && pDialog.isShowing()){
                pDialog.cancel();
            }
            if(result!=null){
                try {
                    JSONObject jObj=new JSONObject(result);

                    if(jObj.getString("status").equals("SUCCESS")){

                        Intent intent=new Intent(SetNewPassword.this,Login.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        Toast.makeText(SetNewPassword.this,jObj.getString("msg"),Toast.LENGTH_SHORT).show();

                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    }else{
                        Toast.makeText(SetNewPassword.this,jObj.getString("msg"), Toast.LENGTH_SHORT).show();
                    }

                }catch(Exception e){}
            } else{
                Toast.makeText(SetNewPassword.this,getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void myToast(String message) {
        Toast.makeText(SetNewPassword.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }
}
