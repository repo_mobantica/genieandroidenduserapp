package com.genieiot.gsmarthome;
import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by root on 13/10/16.
 */

public class CropingOption {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}