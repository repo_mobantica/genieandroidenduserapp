package com.genieiot.gsmarthome;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import Adapter.AdapterCustomSwitch;

public class MyReceiver extends BroadcastReceiver {
    String type;
    String messageType;
    SharedPreferences sharedpreferences ;


    @Override
    public void onReceive(Context context, Intent intent) {
        String status = null;



        status = NetworkUtil.getConnectivityStatusString(context);
        if(status.equals("2"))
        {
            status="Data Connection";
        }
        else
        {

            String netAddress = null;
            try
            {
                netAddress = new NetTask().execute("192.168.0.119").get();
                Log.d("inetaddress",""+netAddress);
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
            }

            if(netAddress.equals("LOCAL_HUB"))
            {
                status="LOCAL_HUB";
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("INTERNET_TEST_HUB").putExtra("INTERNET_TEST", "1"));

            }
            else
            {
                status="INTERNET";
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("INTERNET_TEST_HUB").putExtra("INTERNET_TEST", "0"));

            }

        }




        Log.d("inetaddressaaa","aaaa"+status);
        Bundle b= intent.getExtras();
        Intent i =new Intent(context, MainActivity.class);
        i.putExtra("ConnVal",status);
        context.startActivity(i);

    }



    public class NetTask extends AsyncTask<String, Integer, String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            InetAddress addr = null;
            try
            {
                addr = InetAddress.getByName(params[0]);
                try {
                    if (addr.isReachable(300)) //5000
                    {
                        Log.d("network_checking1","Router is   reachable");
                        type="LOCAL_HUB";
                    }
                    else
                    {
                        Log.d("network_checking1","Router is not  reachable");
                        type="INTERNET";
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            catch (UnknownHostException e)
            {
                e.printStackTrace();
            }
            return type;
        }
    }






}