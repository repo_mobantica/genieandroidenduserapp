package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import Adapter.SchedularAdapter;
import Database.DatabaseHandler;
import Session.Constants;
import Session.IOnClickOfScheduleList;
import Session.SessionManager;

import static Database.DatabaseHandler.DIMMER_STATUS;
import static Database.DatabaseHandler.DIMMER_VALUE;
import static Database.DatabaseHandler.HIDE;
import static Database.DatabaseHandler.IMAGE_OFF;
import static Database.DatabaseHandler.IMAGE_ON;
import static Database.DatabaseHandler.IP;
import static Database.DatabaseHandler.LOCK;
import static Database.DatabaseHandler.PANEL_NAME;
import static Database.DatabaseHandler.REPEATED_STATUS;
import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.ROOM_TYPE_ID;
import static Database.DatabaseHandler.SCHEDULE_DATETIME;
import static Database.DatabaseHandler.SCHEDULE_ID;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;
import static Database.DatabaseHandler.SWITCH_NUMBER;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.SWITCH_TYPE_ID;

import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 17/3/17.
 */

public class ScheduleListActivity extends AppCompatActivity implements View.OnClickListener, IOnClickOfScheduleList {
    private RecyclerView recyclerView;
    private SchedularAdapter adapter;
    private TextView txtHeading;
    private ImageView imgBack;
    private ArrayList<HashMap<String, String>> arrayListSchedule;
    private DatabaseHandler db;
    private ProgressDialog pDialog;
    private static final String TAG = "ScheduleListActivity";
    private String mScheduleId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shedule_activity);
        arrayListSchedule = new ArrayList<>();
        db = new DatabaseHandler(this);
        initControl();
       // setAdpater();


        new AsyncSheduleListTask().execute();
      //  setAdpater();

    }

    private void setAdpater() {
        arrayListSchedule = db.getSchedulerInfo();
        Log.d("shedule_tab_no", "===" + arrayListSchedule);
        Collections.reverse(arrayListSchedule);
        adapter = new SchedularAdapter(this, arrayListSchedule, "SCHEDULE");
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initControl() {
        txtHeading = (TextView) findViewById(R.id.txtHeading);
        imgBack = (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        txtHeading.setText("Schedule List");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;
        }

    }

    @Override
    public void onClickOfScheduleList(final HashMap<String, String> mMap) {
        Log.d("repeated_stat","==="+mMap);
        final CharSequence[] strArray = {"Edit", "Delete"};
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ScheduleListActivity.this);
        alertDialogBuilder.setItems(strArray,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:

                                Intent intent = new Intent(ScheduleListActivity.this, Schedular_Activity.class);
                                DatabaseHandler db = new DatabaseHandler(ScheduleListActivity.this);
                                intent.putExtra("SwitchInfo", db.getSwitchInfoBySwitchId(mMap.get(SWITCH_ID)));
                                intent.putExtra("RepeatedStatus", mMap.get(REPEATED_STATUS));
                                intent.putExtra("Operation", "EDIT");
                                intent.putExtra("EditInfo", mMap);
                                startActivity(intent);

                                break;
                            case 1:
                                alertDialogDelete(mMap);
                                break;
                        }
                    }
                });
        alertDialogBuilder.show();

    }

    private void alertDialogDelete(final HashMap<String, String> mMap) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete);

        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        TextView txtDelete = (TextView) dialog.findViewById(R.id.txtDelete);

        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AsyncDeleteScheduleTask().execute(mMap);
                dialog.dismiss();
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class AsyncDeleteScheduleTask extends AsyncTask<HashMap<String, String>, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ScheduleListActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(HashMap<String, String>... params) {
            Constants res = new Constants();
            SessionManager session = new SessionManager(ScheduleListActivity.this);
            String response = "", messageType = "";
            HashMap<String, String> mList = params[0];
            if (URL_GENIE.equals(URL_GENIE_AWS)) {
                messageType = INTERNET;
            } else {
                messageType = LOCAL_HUB;
            }
            try {

                JSONObject jBody = new JSONObject();
                mScheduleId = mList.get(SCHEDULE_ID);
                jBody.put("userId", session.getUSERID());
                jBody.put("scheduleSwitchId", mList.get(SCHEDULE_ID));
                jBody.put("messageType", messageType);
                jBody.put("switchId", mList.get(SWITCH_ID));
                jBody.put("switchStatus", mList.get(SWITCH_STATUS));
                jBody.put("lockStatus", "0");
                String scheduleTime = mList.get(SCHEDULE_DATETIME) + " " + mList.get(TIME) + ":00";
                jBody.put("scheduleDateTime", scheduleTime);

                Log.d(TAG, "Schedule Delete " + jBody);
                response = res.doPostRequest(URL_GENIE + "/schedule/deletescheduleswitch", jBody + "", session.getSecurityToken());
            } catch (Exception e) {
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }

            if (result != null) {
                Log.d(TAG, "onPostExecute: result" + result);
                try {
                    JSONObject jObj = new JSONObject(result);
                    if (jObj.getString("status").equals("SUCCESS")) {
                        db.deleteSchedule(mScheduleId);
                        setAdpater();

                        Toast.makeText(ScheduleListActivity.this, getResources().getString(R.string.SCHEDULE_DELETE), Toast.LENGTH_SHORT).show();
                    } else {
                        //Toast.makeText(AddNewTask.this, "Fail to delete Schedule.", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(ScheduleListActivity.this, getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }

        }
    }


    private class AsyncSheduleListTask extends AsyncTask<HashMap<String, String>, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ScheduleListActivity.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(HashMap<String, String>... params) {
            Constants res = new Constants();
            SessionManager session = new SessionManager(ScheduleListActivity.this);
            String response = "", messageType = "";
            try {
                JSONObject jBody = new JSONObject();
                jBody.put("userId", session.getUSERID());
                response = res.doPostRequest(URL_GENIE + "/schedule/getscheduleswitch", jBody + "", session.getSecurityToken());
                Log.d("shedulelist","url=="+URL_GENIE + "/schedule/getscheduleswitch");
                Log.d("shedulelist","input =="+URL_GENIE + jBody);
                Log.d("shedulelist","response =="+response);
            } catch (Exception e) {
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        /*    if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
            }
*/
            if (result != null) {
                Log.d(TAG, "onPostExecute: result" + result);
                JSONObject jResult = null;
                ArrayList<HashMap<String, String>> roomInfo;
                String switchID;
                try {
                    jResult = new JSONObject(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    try {
                        if (jResult.getString("status").equals("SUCCESS")) {
                            JSONArray jArrResult = new JSONArray(jResult.getString("result"));


                            if(jArrResult.length() <=0)
                            {
                                pDialog.dismiss();
                                Toast.makeText(ScheduleListActivity.this, jResult.getString("msg"), Toast.LENGTH_SHORT).show();
                            }

                            else
                            {
                              //  Toast.makeText(ScheduleListActivity.this, "not null", Toast.LENGTH_SHORT).show();

                                for (int i = 0; i < jArrResult.length(); i++) {
                                    HashMap<String, String> sheduleListData = new HashMap<String, String>();
                                    JSONObject jsonObject = null;
                                    String mDimmer;
                                    try {
                                        jsonObject = new JSONObject(jArrResult.get(i).toString());
                                        String time1 = null;
                                        String dateTime=jsonObject.getString("scheduleDateTime");
                                        Log.d("dtaetime_value_is","===="+dateTime);

                                        String splitList[] = dateTime.split(" ");
                                        time1 =splitList[1];

                                        switchID=jsonObject.getString("switchId");

                                        roomInfo = db.getSwitchInfo1(switchID);
                                        Log.d("switch_id_in","==="+roomInfo);
                                        String switchName = roomInfo.get(0).get(SWITCH_NAME);
                                        String roomName = roomInfo.get(0).get(ROOM_NAME);

                                        Log.d("switch_id_in","switchName==="+switchName);
                                        Log.d("switch_id_in","roomName==="+roomName);

                                        sheduleListData.put(SWITCH_ID, jsonObject.getString("switchId"));
                                        sheduleListData.put(SWITCH_ID, jsonObject.getString("switchId"));
                                        sheduleListData.put(SWITCH_NAME, switchName);

                                        sheduleListData.put(SWITCH_STATUS, jsonObject.getString("switchStatus"));
                                        // sheduleListData.put(SCHEDULE_DATETIME, jsonObject.getString("scheduleDateTime"));
                                        sheduleListData.put(SCHEDULE_DATETIME, splitList[0]);
                                        sheduleListData.put(TIME, time1);

                                        sheduleListData.put(ROOM_NAME,roomName);
                                        sheduleListData.put(IP, splitList[1]);
                                        sheduleListData.put(SCHEDULE_ID,  jsonObject.getString("id"));
                                        sheduleListData.put(DIMMER_STATUS, jsonObject.getString("dimmerStatus"));
                                        sheduleListData.put(DIMMER_VALUE, jsonObject.getString("dimmerValue"));
                                        sheduleListData.put(REPEATED_STATUS, jsonObject.getString("repeatStatus"));
                                        Log.d("switch_id_repeated","roomName==="+jsonObject.getString("repeatStatus"));

                                        db.insertSchedulerInfo(sheduleListData);

                                        pDialog.dismiss();

                                    } catch (JSONException e) {
                                        e.printStackTrace();


                                    }

                                }
                            }



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }

                setAdpater();
            }

            }
        }
    }

