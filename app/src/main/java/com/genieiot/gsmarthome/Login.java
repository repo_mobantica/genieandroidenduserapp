package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;

import net.hockeyapp.android.CrashManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Database.DatabaseHandler;
import Fragments.Rooms;
import Session.Constants;
import Session.SessionManager;
import app.AppController;

import static Database.DatabaseHandler.ROOM_ID;
import static Database.DatabaseHandler.ROOM_IMAGE_TYPE;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.ROOM_TYPE_ID;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.MESSAGE_INTERNET_CONNECTION;
import static Session.Constants.MY_PREFS_NAME;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;
import static com.genieiot.gsmarthome.MainActivity.isInternetAvailable;
import static com.genieiot.gsmarthome.MainActivity.myToast;

public class Login extends Activity implements View.OnClickListener {
    private EditText edtPassword;
    private EditText edtUsername;
    private Button btnLogin;
    private TextView txtForgot;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req";
    String mDevideId="",mSharedOTP="",mPassword="";
    private String mUserName;
    private String mid,lastName,firstName,token,mphoneNumber,email,userType,dob;
    SessionManager sessionManager;
    WifiManager manager;
    Dialog dialog;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login1);
        initialseControls();
        sessionManager = new SessionManager(Login.this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString("room_key", "true");
        editor.putString("switch_key","true");
        editor.apply();


        sessionManager.setCountVal("true");
        sessionManager.setCountValSwitch("true");


        if(refreshedToken!=null)
        {
            sessionManager.saveDeviceToken(refreshedToken);
        }


    }



    private void initialseControls() {

        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtForgot = (TextView) findViewById(R.id.forgot);
        manager = (WifiManager)getApplicationContext().getSystemService(this.WIFI_SERVICE);
        txtForgot.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        edtUsername.setText(getIntent().getStringExtra("EMAIL"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnLogin:
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if (isFieldSubmtted()) {

                    if (isInternetAvailable(Login.this)) {
                        login();
                    }
                    else {
                        myToast(MESSAGE_INTERNET_CONNECTION);
                    }

                }
                break;
            case R.id.forgot:
                Intent intent = new Intent(Login.this, ForgotPassword.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
    }

    private void createDeviceIdDialog() {

        dialog = new Dialog(Login.this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_device_id);
        TextView txtOk = (TextView) dialog.findViewById(R.id.txtOk);
        TextView txtCancel = (TextView) dialog.findViewById(R.id.txtCancel);
        final EditText edtDeviceId = (EditText) dialog.findViewById(R.id.edtDeviceId);
        final EditText edtenterOTP = (EditText) dialog.findViewById(R.id.edtenterOTP);
        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDevideId=edtDeviceId.getText().toString();
                mSharedOTP=edtenterOTP.getText().toString();

                if(mDevideId.equals("") && mSharedOTP.equals("")){
                    myToast(getResources().getString(R.string.MSG_DEVICE_ID));
                    return;
                }

                mUserName=edtUsername.getText().toString();

                new VerifyDeviceIdTask().execute();

            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        });

        dialog.show();
    }

    private void redirectToMainActivity() {

        deleteAll();
        sessionManager.setAppUrl("192.168.0.119");
        sessionManager.setRouterIP("192.168.0.119");

       // sessionManager.setAppUrl("192.168.2.15");
       // sessionManager.setRouterIP("192.168.2.15");


      // sessionManager.setAppUrl("192.168.2.4");
      // sessionManager.setRouterIP("192.168.2.4");

//        Intent intent = new Intent(Login.this, BlankScreen.class);
        Intent intent = new Intent(Login.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private void deleteAll() {
        DatabaseHandler db=new DatabaseHandler(this);
        db.deleteRoomData();
        db.deleteSwitchData();
        db.deleteNotificationData();
        db.deleteRecentData();
        db.deleteROOM_TYPE();
        db.deleteSWITCH_TYPE();
    }
    private boolean isFieldSubmtted() {

        if (edtUsername.getText().toString().length() < 1) {
            myToast(getResources().getString(R.string.MSG_VALID_MAIL));
            return false;

        } else if (!validateEmail()) {
            myToast(getResources().getString(R.string.MSG_VALID_MAIL));
            return false;
        } else if (edtPassword.getText().toString().length() < 6) {
            myToast(getResources().getString(R.string.MSG_VAILD_PASS));
            return false;
        }
        return true;
    }
    private void myToast(String message) {

        Toast.makeText(Login.this, message, Toast.LENGTH_LONG).show();
    }
    public boolean validateEmail() {
        String emailInput, emailPattern;

        emailInput = edtUsername.getText().toString().trim();

        emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


        if (!emailInput.matches(emailPattern)) {
            return false;
        }

        return true;
    }
    private void showProgressDialog() {
        if (pDialog != null)
            pDialog.show();
    }
    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }
    /**
     * Making json object request
     */
    private void login() {
        showProgressDialog();
        Map<String, String> jsonParams = new HashMap<String, String>();


        jsonParams.put("username", edtUsername.getText().toString());
        jsonParams.put("password", edtPassword.getText().toString());

        Log.d("param", jsonParams.toString());

        String url=URL_GENIE_AWS+"/login";
        Log.d("login_url","url==="+url);
        Log.d("login_url","json==="+jsonParams);
        JsonObjectRequest myRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideProgressDialog();
                        if (response != null) {
                            Log.d("login_url","response==="+response);
                            try {
                                String status = response.getString("status");
                                String result = response.getString("result");
                                JSONObject resultObject = new JSONObject(result);

                                if (status.equals("SUCCESS")) {

                                    mid = resultObject.getString("id");
                                    lastName = resultObject.getString("lastName");
                                    firstName = resultObject.getString("firstName");
                                    token = resultObject.getString("token");
                                    mphoneNumber = resultObject.getString("phoneNumber");
                                    email = resultObject.getString("email");

                                    if(resultObject.has("userType")) {
                                       userType = resultObject.getString("userType");

                                    }
                                    if(resultObject.has("image")){
                                       sessionManager.setUserImage(resultObject.getString("image"));
                                    }

                                    if(resultObject.has("homeId")) {
                                        sessionManager.setHomeId(resultObject.getString("homeId"));
                                        Log.d("home_id_is","===");
                                    }

                                    mPassword=edtPassword.getText().toString();

                                     dob= String.valueOf(resultObject.get("birthDate"));
                                     Log.d("dob_val1","=="+dob);

                                    if(resultObject.getString("isFirstTimeLogin").equals("true")) {
                                        createDeviceIdDialog();
                                    }
                                    else
                                    {
                                        sessionManager.saveUser(mid, firstName, lastName, email,mphoneNumber,userType);
                                        sessionManager.saveSecurityToken(token);
                                        sessionManager.savePassword(mPassword);
                                        sessionManager.setDOB(String.valueOf(resultObject.get("birthDate")));
                                        Toast.makeText(Login.this, "Welcome to Genie SmartHome", Toast.LENGTH_LONG).show();
                                        redirectToMainActivity();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d("error",""+e.getMessage());

                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideProgressDialog();
                        error.printStackTrace();
                        Log.d("error_list","error is="+error.getMessage());
                        myToast("Enter valid username/password");
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("DEVICE-ID", sessionManager.getDeviceToken());
                headers.put("DEVICE-TYPE", "ANROID");
//                headers.put("User-agent", "My useragent");
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(myRequest, tag_json_obj);
    }

    class VerifyDeviceIdTask extends AsyncTask<Void,Void,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Login.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }
        @Override
        protected String doInBackground(Void... params) {
            Constants res=new Constants();
            String response=null;
            try{
                JSONObject jmain=new JSONObject();
                jmain.put("email",mUserName);
                if(!mDevideId.equals("")){
                   jmain.put("deviceId",mDevideId);
                }
                else
                {
                    jmain.put("shareControlOTP",mSharedOTP);
                }
                response=res.doPostRequest(URL_GENIE_AWS +"/user/validateUser",jmain+"");
                return response;

            }catch (Exception e){}
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(pDialog!=null && pDialog.isShowing()){
               pDialog.cancel();
            }

            if(result!=null){
                Log.d("validateUser Response ",""+result);
                try {
                    JSONObject response = new JSONObject(result);
                    if (response.getString("status").equals("SUCCESS")) {

                        JSONObject jResult=new JSONObject(response.getString("result"));
                        userType=jResult.getString("userType");
                        sessionManager.setHomeId(jResult.getString("homeId"));

                        sessionManager.saveUser(mid, firstName, lastName, email,mphoneNumber,userType);
                        sessionManager.saveSecurityToken(token);
                        sessionManager.savePassword(mPassword);

                        redirectToMainActivity();
                    }
                    else
                    {
                         if(dialog!=null)
                         dialog.dismiss();
                         AlertDeviceAlreadyPresent(response.getString("msg"));
                    }

                }
                catch(Exception e) {e.printStackTrace();}
             }
            else
            {
                Toast.makeText(Login.this,getResources().getString(R.string.MSG_TRY_AGAIN),Toast.LENGTH_SHORT).show();
            }
            }
    }

    private void AlertDeviceAlreadyPresent(String msg) {
         final Dialog dialogDeviceID = new Dialog(Login.this);
        // Include dialog.xml file
        dialogDeviceID.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDeviceID.setContentView(R.layout.deviceidused);
        TextView txtOk = (TextView) dialogDeviceID.findViewById(R.id.txtOk);
        TextView tvAlertMsg = (TextView) dialogDeviceID.findViewById(R.id.tvAlertMsg);
        TextView txtCancel = (TextView) dialogDeviceID.findViewById(R.id.txtCancel);

        tvAlertMsg.setText(msg);

        txtOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDeviceID.dismiss();
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        dialogDeviceID.dismiss();
    }
});
        dialogDeviceID.show();

    }

}


