package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Product Name : Square
 * Company Name : Genie Smart Home
 * Package Name : com.genieiot.geniesmarthome
 * version      : 1
 * Author       : Digvijay,Reshma
 * Description  : This page gives information about company details.
 */
public class AboutUs extends Activity implements View.OnClickListener {

    private WebView webView;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
        setToolbar();
        webView = (WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
       // webView.loadUrl("http://gsmarthome.com/");http://mobantica.com/contact/
     //   webView.loadUrl("http://mobantica.com/contact/");
        pd = new ProgressDialog(AboutUs.this);
        pd.setMessage("Please wait Loading...");
        pd.show();
        webView.setWebViewClient(new MyWebViewClient());
     /*   webView.setInitialScale(1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);*/
        webView.loadUrl("http://mobantica.com/contact/");
    }


    private void setToolbar() {

        TextView txtHeading = (TextView) findViewById(R.id.txtHeading);
        ImageView imgBack = (ImageView) findViewById(R.id.imgBack);
        ImageView imgOption = (ImageView) findViewById(R.id.imgOption);
        txtHeading.setText("Contact Us");
        imgBack.setOnClickListener(this);
        imgOption.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgBack:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            if (!pd.isShowing()) {
                pd.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            if (pd.isShowing()) {
                pd.dismiss();
            }

        }
    }
}
