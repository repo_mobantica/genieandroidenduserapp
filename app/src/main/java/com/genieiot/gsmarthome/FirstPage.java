package com.genieiot.gsmarthome;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import Session.Constants;
import Session.SessionManager;

/**
 * Created by Genie IoT on 9/16/2016.
 */
public class FirstPage extends Activity implements View.OnClickListener {
    private Button btnLogin, btnRegister, btnTryDemo;
    private TextView tvVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_page);
        initialiseControls();
        setListener();
    }


    private void setListener() {

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        btnTryDemo.setOnClickListener(this);
    }

    private void initialiseControls() {

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnTryDemo = (Button) findViewById(R.id.btnTryDemo);
        btnTryDemo.setVisibility(View.INVISIBLE);
        tvVersion = (TextView) findViewById(R.id.tvVersion);

        try {
        PackageManager manager =getPackageManager();
        PackageInfo info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
        String version = info.versionName;
         tvVersion.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {

        Intent intent = null;
        switch (v.getId()) {
            case R.id.btnLogin:
               // Crashlytics.getInstance().crash(); // Force a crash
                intent = new Intent(FirstPage.this, Login.class);
                startActivity(intent);
                break;

            case R.id.btnRegister:
                intent = new Intent(FirstPage.this, Register.class);
                startActivity(intent);
                break;

            case R.id.btnTryDemo:
                intent = new Intent(FirstPage.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                SessionManager sessionManager = new SessionManager(FirstPage.this);
                sessionManager.logoutUser();
                sessionManager.setDemoUser("DemoUser");
                sessionManager.setFirstString("panel1");
                sessionManager.setSecondString("panel2");
                sessionManager.setThirdString("panel3");
                sessionManager.setRouterIP("192.168.0.119");
                sessionManager.setTopicName("home1");
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        CrashManager.register(this);
        UpdateManager.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.unregister();
    }
}
