package com.genieiot.gsmarthome;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import Adapter.ProfileAdapter;
import Adapter.SchedularAdapter;
import Database.DatabaseHandler;
import Fragments.ProfileFragment;
import Session.Constants;
import Session.IOnClickOfScheduleList;
import Session.SessionManager;

import static Database.DatabaseHandler.ISMODE_SELECTED;
import static Database.DatabaseHandler.MODE_NAME;
import static Database.DatabaseHandler.SCHEDULE_DATETIME;
import static Database.DatabaseHandler.SCHEDULE_ID;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_STATUS;
import static Database.DatabaseHandler.TIME;
import static Session.Constants.INTERNET;
import static Session.Constants.LOCAL_HUB;
import static Session.Constants.URL_GENIE;
import static Session.Constants.URL_GENIE_AWS;

/**
 * Created by root on 29/11/16.
 */

public class

AddNewTask extends Activity implements View.OnClickListener, IOnClickOfScheduleList {

    TextView txtHeading,txtAddTask;
    RecyclerView recyclerView;
    ArrayList<HashMap<String,String>> arrayList;
    SchedularAdapter adapter;
    SessionManager sessionManager;
    ImageView imgBack;
    private ProgressDialog pDialog;
    private String mScheduleId;
    DatabaseHandler db;
    private String messageType="";
    private static final String TAG = "AddNewTask";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addnewtask);
          //  prepareScheduleList();
        initializeControls();
        arrayList=new ArrayList<>();

        db=new DatabaseHandler(this);
        sessionManager=new SessionManager(this);
        if(URL_GENIE.equals(URL_GENIE_AWS)){
            messageType=INTERNET;
        }else{
            messageType=LOCAL_HUB;
        }
        //take data from database
        setAdapter();

    }

    private void prepareScheduleList(){

        arrayList=new ArrayList<>();
        HashMap<String,String> map1=new HashMap<>();
        map1.put("SwitchName","Bulb");
        map1.put("SwitchStatus","1");
        map1.put("Time","7.00 A.M");
        map1.put("Date","31/11/2016");
        arrayList.add(map1);

        HashMap<String,String> map2=new HashMap<>();
        map2.put("SwitchName","Lamp");
        map2.put("SwitchStatus","0");
        map2.put("Time","8.00 A.M");
        map2.put("Date","1/12/2016");
        arrayList.add(map2);
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(message, new IntentFilter("NotificationSend"));
        super.onResume();
        txtHeading.setText("Schedule List");
    }
    private void initializeControls() {
        txtHeading= (TextView) findViewById(R.id.txtHeading);
        recyclerView= (RecyclerView) findViewById(R.id.recycler);
        txtAddTask= (TextView) findViewById(R.id.tvaddnewtask);
        imgBack= (ImageView) findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);
        txtAddTask.setOnClickListener(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

    }
    public void setAdapter(){


        HashMap<String,String> maplist= (HashMap)getIntent().getSerializableExtra("SwitchInfo");
        arrayList=db.getSchedulerSwitchWiseInfo(maplist.get(SWITCH_ID));
        Log.d("arraylist12","=="+arrayList);
        Collections.reverse(arrayList);
        adapter=new SchedularAdapter(this,arrayList,"Add");
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tvaddnewtask:
                Intent intent = new Intent(AddNewTask.this, Schedular_Activity.class);
                HashMap maplist;
                HashMap<String,String> mapListEdit;
                mapListEdit=new HashMap<>();
                Intent intent1=getIntent();
                maplist= (HashMap) intent1.getSerializableExtra("SwitchInfo");
                intent.putExtra("SwitchInfo", maplist);
                intent.putExtra("Operation","ADD");
                intent.putExtra("EditInfo",mapListEdit);
                startActivity(intent);
                AddNewTask.this.finish();

                break;
            case R.id.imgBack:
                AddNewTask.this.finish();
                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                break;
        }
    }

    @Override
    public void onClickOfScheduleList(final HashMap<String, String> mMap) {

        final CharSequence[] strArray = {"Edit", "Delete"};
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddNewTask.this);
        alertDialogBuilder.setItems(strArray,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:

                                Intent intent = new Intent(AddNewTask.this,Schedular_Activity.class);
                                HashMap maplist;
                                Intent intent1=getIntent();
                                maplist= (HashMap) intent1.getSerializableExtra("SwitchInfo");
                                intent.putExtra("SwitchInfo", maplist);
                                intent.putExtra("Operation","EDIT");
                                intent.putExtra("EditInfo",mMap);
                                startActivity(intent);
                                AddNewTask.this.finish();

                                break;
                            case 1:
                                alertDialogDelete(mMap);
                                break;
                        }
                    }
                });
        alertDialogBuilder.show();

    }

    private void alertDialogDelete(final HashMap<String,String> mMap)  {
        final Dialog dialog = new Dialog(AddNewTask.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_delete);

        TextView txtCancel= (TextView) dialog.findViewById(R.id.txtCancel);
        TextView txtDelete= (TextView) dialog.findViewById(R.id.txtDelete);

        txtDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AsyncDeleteScheduleTask().execute(mMap);
                dialog.dismiss();
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private class AsyncDeleteScheduleTask extends AsyncTask<HashMap<String,String>,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddNewTask.this);
            pDialog.setMessage("Please Wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(HashMap<String,String>... params) {
            Constants res=new Constants();
            SessionManager session=new SessionManager(AddNewTask.this);
            String response="";
            HashMap<String, String> mList = params[0];
            try {

                JSONObject jBody=new JSONObject();
                mScheduleId=mList.get(SCHEDULE_ID);
                jBody.put("userId",session.getUSERID());
                jBody.put("scheduleSwitchId",mList.get(SCHEDULE_ID));
                jBody.put("messageType",messageType);
                jBody.put("switchId",mList.get(SWITCH_ID));
                jBody.put("switchStatus",mList.get(SWITCH_STATUS));
                jBody.put("lockStatus","");
                String scheduleTime=mList.get(SCHEDULE_DATETIME)+" "+mList.get(TIME)+":00";
                jBody.put("scheduleDateTime",scheduleTime);

                Log.d(TAG,"Schedule Delete "+jBody);
                response=res.doPostRequest(URL_GENIE+"/schedule/deletescheduleswitch",jBody+"",session.getSecurityToken());
            }catch(Exception e){}

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(pDialog!=null && pDialog.isShowing()){
                pDialog.cancel();
            }

            if(result!=null){
                Log.d(TAG, "onPostExecute: result"+result);
                try{
                    JSONObject jObj=new JSONObject(result);
                    if(jObj.getString("status").equals("SUCCESS")){
                         db.deleteSchedule(mScheduleId);
                         setAdapter();
                         Toast.makeText(AddNewTask.this,getResources().getString(R.string.SCHEDULE_DELETE), Toast.LENGTH_SHORT).show();
                    }else{
                        //Toast.makeText(AddNewTask.this, "Fail to delete Schedule.", Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){e.printStackTrace();}
            }else{
                Toast.makeText(AddNewTask.this,getResources().getString(R.string.MSG_TRY_AGAIN), Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(message);
    }
    private BroadcastReceiver message = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            setAdapter();
        }
    };
}
