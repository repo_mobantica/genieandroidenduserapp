package Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.genieiot.gsmarthome.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import Database.DatabaseHandler;

import static Database.DatabaseHandler.MODE_ID;
import static Database.DatabaseHandler.ROOM_NAME;
import static Database.DatabaseHandler.SELECTED_SWITCH;
import static Database.DatabaseHandler.SWITCH_ID;
import static Database.DatabaseHandler.SWITCH_IMAGE_ID;
import static Database.DatabaseHandler.SWITCH_NAME;

/**
 * Created by root on 17/1/17.
 */

public class SelectSwitchesAdapter extends RecyclerView.Adapter<SelectSwitchesAdapter.MyViewHolder> {

    DatabaseHandler db;
    Context context;
    ArrayList<HashMap<String, String>> arrayList;
    ArrayList<HashMap<String, String>> arrModeProfile;
    ArrayList<Integer> mIconArrayList;
    ArrayList<String> switchId;
    String switchId1[];
    HashMap<String,String> map1= new HashMap<String,String>();
    String selctedId;

    HashMap<String,String> modedata;
    String mode_id;
    String checkStatus;


    public SelectSwitchesAdapter(Context context, ArrayList<HashMap<String, String>> arrayList, ArrayList<Integer> mIconArrayList,ArrayList<HashMap<String, String>> arrModeProfile,String selctedId,String checkStatus) {
        this.context=context;
        this.arrayList=arrayList;
        db=new DatabaseHandler(context);
        this.mIconArrayList=mIconArrayList;
        this.arrModeProfile=arrModeProfile;
        this.selctedId=selctedId;
        this.checkStatus=checkStatus;


        if(checkStatus.equals("true") || checkStatus.equals("true1"))
        {
            Log.d("checking_adapter","==="+checkStatus);
        }
        else
        {
            Log.d("checking_adapter","1==="+checkStatus);
            for(int i=0;i<=arrModeProfile.size()-1;i++)
            {
                String switchesId=arrModeProfile.get(i).get(SWITCH_ID);
                mode_id=arrModeProfile.get(i).get(MODE_ID);

                if(switchesId == null)
                {
                    switchId1=null;
                }
                else
                {

                    if(mode_id.equals(selctedId))
                    {
                        switchId1 = switchesId.split(":");

                        for(int i1=0;i1<=(switchId1.length-1);i1++)
                        {
                            map1.put(switchId1[i1],switchId1[i1]);
                            Log.d("switc_id","=="+switchId1[i1]);
                        }
                    }
                }
            }
        }


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.selectswitches, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SelectSwitchesAdapter.MyViewHolder holder, final int position) {
     final HashMap<String,String> mMap=arrayList.get(position);
        holder.cbSwitchSelect.setOnCheckedChangeListener(null);

        holder.txtItemSwitch.setText(arrayList.get(position).get(SWITCH_NAME));
        holder.txtItemRoom.setText(arrayList.get(position).get(ROOM_NAME));

        holder.imgItemSwitch.setImageResource(mIconArrayList.get(Integer.parseInt(arrayList.get(position).get(SWITCH_IMAGE_ID))));

       // if(arrayList.get(position).get(SELECTED_SWITCH).equals("1")) {

        if(checkStatus.equals("true") || checkStatus.equals("true1"))
        {

            if(checkStatus.equals("true"))
            {
                Log.d("checking_adapter","==="+checkStatus);
                holder.cbSwitchSelect.setChecked(true);
                mMap.put(SELECTED_SWITCH,"1");
                arrayList.set(position,mMap);
            }
            else
            {
                Log.d("checking_adapter","==="+checkStatus);
                holder.cbSwitchSelect.setChecked(false);
                mMap.put(SELECTED_SWITCH,"0");
                arrayList.set(position,mMap);
            }


        }
        else {
            if(switchId1 == null)
            {
                holder.cbSwitchSelect.setChecked(false);
            }else
            {
                for(int i1=0;i1<=(switchId1.length-1);i1++)
                {
                    String val=arrayList.get(position).get(SWITCH_ID);
                    Log.d("mode_data_all","==="+Arrays.asList(map1));
                    Log.d("mode_data_all","switch id==="+val);
                    Log.d("mode_data_all","selcted id==="+selctedId);


                    if(map1.containsValue(val)  )
                    {
                        holder.cbSwitchSelect.setChecked(true);
                        mMap.put(SELECTED_SWITCH,"1");
                        arrayList.set(position,mMap);


                    }
                    else
                    {
                        holder.cbSwitchSelect.setChecked(false);
                        mMap.put(SELECTED_SWITCH,"0");
                        arrayList.set(position,mMap);


                    }


                }
            }
        }


        holder.cbSwitchSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
               // HashMap<String,String> mMap=arrayList.get(position);

                if(isChecked) {
                    mMap.put(SELECTED_SWITCH,"1");
                }
                else {
                    mMap.put(SELECTED_SWITCH,"0");
                    LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("unSelectChcekbox"));
                }

                arrayList.set(position,mMap);
            }
        });
    }

    public Object getItem() {
        // TODO Auto-generated method stub
        return arrayList;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ImageView imgItemSwitch;
        TextView txtItemSwitch,txtItemRoom;
        CheckBox cbSwitchSelect;
        public MyViewHolder(View itemView) {
            super(itemView);
            imgItemSwitch= (ImageView) itemView.findViewById(R.id.imgItemSwitch);
            txtItemSwitch= (TextView) itemView.findViewById(R.id.txtItemSwitch);
            txtItemRoom= (TextView) itemView.findViewById(R.id.txtItemRoom);
            cbSwitchSelect= (CheckBox) itemView.findViewById(R.id.cbSwitchSelect);
        }
    }
}
